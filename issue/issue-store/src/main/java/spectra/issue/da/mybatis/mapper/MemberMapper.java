package spectra.issue.da.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import spectra.issue.domain.entity.Member;

import java.util.List;

@Mapper
public interface MemberMapper
{
    void insert(Member member);
    boolean exists(String id);
    Member findOne(String id);
    Member findByMemberId(@Param("memberId") String memberId);
    List<Member> findByName(@Param("name") String name);
    List<Member> findByEmail(@Param("email") String email);
    List<Member> findByRoleId(@Param("roleId") String roleId);
    Member findByMemberIdAndPassword(@Param("memberId") String memberId, @Param("password") String password);
    int update(Member member);
    int delete(String id);
}
