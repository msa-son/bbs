package spectra.issue.adapter.rest;

import nara.share.domain.NameValueList;
import nara.share.restclient.NaraRestClient;
import nara.share.restclient.RequestBuilder;
import spectra.issue.domain.entity.Issue;
import spectra.issue.domain.entity.Project;
import spectra.issue.domain.spec.IssueProvider;
import spectra.issue.domain.spec.ProjectProvider;
import spectra.issue.domain.spec.sdo.IssueCdo;
import spectra.issue.domain.spec.sdo.ProjectCdo;

import java.util.List;

public class ProjectProviderRestAdapter implements ProjectProvider
{
    private NaraRestClient naraRestClient;

    public ProjectProviderRestAdapter(NaraRestClient naraRestClient)
    {
        this.naraRestClient = naraRestClient;
    }

    @Override
    public List<Project> listProject()
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.PROJECT_P_LIST)
                            .setResponseType(List.class)
        );
    }

    @Override
    public String registerProject(ProjectCdo projectCdo)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.PROJECT_P_BUILD)
                            .setRequestBody(projectCdo)
                            .setResponseType(String.class)
        );
    }

    @Override
    public Project findProject(String projectId)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.PROJECT_P_FIND)
                            .addPathParam("projectId", projectId)
                            .setResponseType(Project.class)
        );
    }

    @Override
    public void modifyProject(String projectId, NameValueList nameValueList)
    {
        naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.PROJECT_P_MODIFY)
                            .addPathParam("projectId", projectId)
                            .setRequestBody(nameValueList)
        );
    }
}
