package spectra.audit.da.jpa;

import nara.share.exception.store.AlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import spectra.audit.da.jpa.jpo.HistoryJpo;
import spectra.audit.da.jpa.springdata.HistoryRepository;
import spectra.audit.domain.entity.History;
import spectra.audit.domain.store.HistoryStore;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Repository
public class HistoryJpaStore implements HistoryStore
{
    @Autowired
    private HistoryRepository historyRepository;

    @Override
    public String create(History history)
    {
        String id = history.getId();
        if(historyRepository.exists(id))
        {
            throw new AlreadyExistsException(String.format("Comment jpo[ID:%s already exit", id));
        }
        HistoryJpo historyJpo = new HistoryJpo(history);
        historyRepository.save(historyJpo);
        return id;
    }

    @Override
    public History retrieve(String id) throws NoSuchElementException
    {
        HistoryJpo historyJpo = historyRepository.findOne(id);
        if(historyJpo == null)
        {
            throw new NoSuchElementException(String.format("No History jpo[ID:%s] to retrieve", id));
        }

        return historyJpo.toDomain();
    }

    @Override
    public List<History> retrieveList()
    {
        Iterable<HistoryJpo> it = historyRepository.findAll(new Sort(Sort.Direction.DESC, "time"));
        List<HistoryJpo> historyJpos = StreamSupport.stream(it.spliterator(), false).collect(Collectors.toList());
        return HistoryJpo.toDomains(historyJpos);
    }

    @Override
    public void delete(History history)
    {
        historyRepository.delete(history.getId());
    }
}