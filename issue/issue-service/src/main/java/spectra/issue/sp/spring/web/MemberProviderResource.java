package spectra.issue.sp.spring.web;

import nara.share.crypt.CryptoUtil;
import nara.share.domain.NameValueList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.logic.MemberLogic;
import spectra.issue.domain.spec.MemberProvider;
import spectra.issue.domain.spec.sdo.MemberCdo;
import spectra.issue.sp.spring.web.util.ServletUtil;

import java.util.List;

@RestController
@RequestMapping("api/p/members")
public class MemberProviderResource implements MemberProvider
{
    @Value("${jwt.key}")
    private String jwtKey;

    @Value("${jwt.key.validate}")
    private long jwtValidate; // 30일짜리 토큰;

    @Autowired
    private MemberLogic memberLogic;

    @Override
    @GetMapping
    public List<Member> listMember()
    {
        return memberLogic.listMember();
    }

    @Override
    @PostMapping
    public String registerMember(@RequestBody MemberCdo memberCdo)
    {
        return memberLogic.registerMember(memberCdo);
    }

    @Override
    @GetMapping("{memberId}")
    public Member findMember(@PathVariable String memberId)
    {
        return memberLogic.findMember(memberId);
    }

    @Override
    @PutMapping("{memberId}")
    public void modifyMember(@PathVariable String memberId, @RequestBody NameValueList nameValueList)
    {
        memberLogic.modifyMember(memberId, nameValueList);
    }

    @Override
    @GetMapping("exists")
    public boolean existsMemberId(@RequestParam String memberId)
    {
        return memberLogic.existsMemberId(memberId);
    }

    @Override
    @PostMapping("login")
    public Member login(@RequestParam String memberId, @RequestParam String password)
    {
        Member member = memberLogic.login(memberId, password);

        if (member != null)
        {
            String userJson = "{";
            userJson += "\"memberId\" : \"" + member.getMemberId() + "\"";
            userJson += ", \"name\" : \"" + member.getName() + "\"";
            userJson += ", \"email\" : \"" + member.getEmail() + "\"";
            userJson += ", \"roleId\" : \"" + member.getRoleId() + "\"";
            userJson += "}";

            String jwt = CryptoUtil.createJWT(member.getMemberId(), userJson, jwtKey, jwtValidate);
            System.err.println("jwt : " + jwt);
            member.setJwt(jwt);

            ServletUtil.setResponseHeader("jwt", jwt);
        }

        return member;
    }

    @Override
    @GetMapping("logout")
    public void logout(String memberId)
    {

    }
}
