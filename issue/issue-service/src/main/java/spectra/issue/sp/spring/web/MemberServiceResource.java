package spectra.issue.sp.spring.web;

import nara.share.domain.NameValueList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.logic.MemberLogic;
import spectra.issue.domain.spec.MemberService;
import spectra.issue.domain.spec.sdo.MemberCdo;

@RestController
@RequestMapping("api/s/members")
public class MemberServiceResource implements MemberService
{
    @Autowired
    private MemberLogic memberLogic;

    @Override
    @PostMapping
    public String registerMember(@RequestBody MemberCdo memberCdo)
    {
        return memberLogic.registerMember(memberCdo);
    }

    @Override
    @GetMapping("{memberId}")
    public Member findMember(@PathVariable String memberId)
    {
        return memberLogic.findMember(memberId);
    }

    @Override
    @PutMapping("{memberId}")
    public void modifyMember(@PathVariable String memberId, @RequestBody NameValueList nameValueList)
    {
        memberLogic.modifyMember(memberId, nameValueList);
    }

    @Override
    @DeleteMapping("{memberId}")
    public void removeMember(@PathVariable String memberId)
    {
        memberLogic.removeMember(memberId);
    }
}
