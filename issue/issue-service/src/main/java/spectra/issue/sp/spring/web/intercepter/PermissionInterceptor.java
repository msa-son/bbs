package spectra.issue.sp.spring.web.intercepter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import spectra.issue.domain.entity.Member;
import spectra.issue.sp.spring.web.config.PermissionProperty;
import spectra.issue.sp.spring.web.security.JwtValidator;
import spectra.issue.sp.spring.web.security.PermissionValidator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class PermissionInterceptor extends HandlerInterceptorAdapter
{
    @Resource
    PermissionProperty permissionProperty;

    @Autowired
    private JwtValidator jwtValidator;

    @Autowired
    private PermissionValidator rbacValidator;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
    {
        if(request.getRequestURI().startsWith("/api/s/") && permissionProperty.isEnabled())
        {
            Member member = jwtValidator.validate(request);
            String method = request.getMethod();
            String uri = request.getRequestURI();
            String roleId = member.getRoleId();

            rbacValidator.validate(uri, method, roleId);
            request.setAttribute("memberId", member.getMemberId());
            request.setAttribute("roleId", member.getRoleId());
        }
        return true;
    }
}
