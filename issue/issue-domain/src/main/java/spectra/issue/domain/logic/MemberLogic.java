package spectra.issue.domain.logic;

import nara.share.domain.NameValueList;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.spec.MemberProvider;
import spectra.issue.domain.spec.MemberService;
import spectra.issue.domain.spec.sdo.MemberCdo;
import spectra.issue.domain.store.IssueStoreLycler;
import spectra.issue.domain.store.MemberStore;

import java.util.List;

public class MemberLogic implements MemberService, MemberProvider
{
    private MemberStore memberStore;

    public MemberLogic(IssueStoreLycler storeLycler)
    {
        this.memberStore = storeLycler.requestMemberStore();
    }

    @Override
    public List<Member> listMember()
    {
        return memberStore.retreiveList();
    }

    @Override
    public String registerMember(MemberCdo memberCdo)
    {
        Member member = new Member(memberCdo.getMemberId(), memberCdo.getPassword(), memberCdo.getName(), memberCdo.getEmail(), memberCdo.getRoleId());
        memberStore.create(member);

        return member.getMemberId();
    }

    @Override
    public Member findMember(String memberId)
    {
        return memberStore.retrieveByMemberId(memberId);
    }

    @Override
    public void modifyMember(String memberId, NameValueList nameValueList)
    {
        Member member = memberStore.retrieveByMemberId(memberId);

        member.setValues(nameValueList);

        memberStore.update(member);
    }

    @Override
    public void removeMember(String memberId)
    {
        Member member = memberStore.retrieveByMemberId(memberId);

        memberStore.delete(member);
    }

    @Override
    public boolean existsMemberId(String memberId)
    {
        return memberStore.retrieveByMemberId(memberId) != null;
    }

    @Override
    public Member login(String memberId, String password)
    {
        return memberStore.retrieveByMemberIdPassword(memberId, password);
    }

    @Override
    public void logout(String memberId)
    {

    }
}
