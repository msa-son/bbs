package spectra.issue.domain.spec;

import nara.share.domain.NameValueList;
import spectra.issue.domain.entity.Project;
import spectra.issue.domain.spec.sdo.ProjectCdo;

public interface ProjectService
{
    //
    String registerProject(ProjectCdo projectCdo);
    Project findProject(String projectId);
    void modifyProject(String projectId, NameValueList nameValueList);
    void removeProject(String projectId);
}
