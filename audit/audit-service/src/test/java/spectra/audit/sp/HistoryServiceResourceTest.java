package spectra.audit.sp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import spectra.audit.AbstractAuditApplicationTests;
import spectra.audit.domain.entity.History;
import spectra.audit.domain.spec.sdo.HistoryCdo;

import java.util.List;

public class HistoryServiceResourceTest extends AbstractAuditApplicationTests
{
    private String historyId;

    @Before
    public void setUp()
    {
        historyId = historyProviderRestAdapter().registerHistory(HistoryCdo.getSample());
    }

    @Test
    public void testCreate()
    {
        historyId = historyProviderRestAdapter().registerHistory(HistoryCdo.getSample());
    }

    @Test
    public void testFind() throws Exception
    {
        History history = historyProviderRestAdapter().findHistory(historyId);
        Assert.assertEquals(historyId, history.getId());
    }

    @Test
    public void testFindList() throws Exception
    {
        List<History> histories = historyProviderRestAdapter().listHistory();
        Assert.assertNotEquals(0, histories.size());
        System.out.println("[TEST INFO] size: " + histories.size());
    }
}
