package spectra.issue.domain.logic;

import nara.share.domain.NameValueList;
import spectra.issue.domain.Role;
import spectra.issue.domain.entity.Comment;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.spec.CommentProvider;
import spectra.issue.domain.spec.CommentService;
import spectra.issue.domain.spec.sdo.CommentCdo;
import spectra.issue.domain.store.CommentStore;
import spectra.issue.domain.store.IssueStoreLycler;

public class CommentLogic implements CommentService, CommentProvider
{
    private CommentStore commentStore;

    public CommentLogic(IssueStoreLycler storeLycler)
    {
        this.commentStore = storeLycler.requestCommentStore();
    }

    @Override
    public String registerComment(CommentCdo commentCdo)
    {
        Comment comment = new Comment(commentCdo.getIssueId(), commentCdo.getCreator(), commentCdo.getContent());

        commentStore.create(comment);

        return comment.getId();
    }

    @Override
    public Comment findComment(String commentId)
    {
        return commentStore.retrieve(commentId);
    }

    @Override
    public void modifyComment(String commentId, NameValueList nameValueList)
    {
        Member member = new Member();
        member.setValues(nameValueList);

        Comment comment = commentStore.retrieve(commentId);
        if(Role.User.getCode().equals(member.getRoleId()))
        {
            String creator = comment.getCreator();
            String memberId = member.getMemberId();

            if(!creator.equals(memberId))
            {
                return;
            }
        }

        comment.setValues(nameValueList);
        commentStore.update(comment);
    }


    @Override
    public void removeComment(String commentId)
    {
        Comment comment = commentStore.retrieve(commentId);
        commentStore.delete(comment);
    }
}
