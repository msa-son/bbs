import React from 'react';
import TextField from 'material-ui/TextField';

/**
 * The input is used to create the `dataSource`, so the input always matches three entries.
 */
export default class Input extends React.Component {
  render() {
    return (
      <TextField hintText="Search"/>
    );
  }
}
