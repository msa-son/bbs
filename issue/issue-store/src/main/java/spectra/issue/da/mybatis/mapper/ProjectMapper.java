package spectra.issue.da.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import spectra.issue.domain.entity.Project;

import java.util.List;

@Mapper
public interface ProjectMapper
{
    void insert(Project project);
    boolean exists(String id);
    Project findOne(String id);
    List<Project> findByName(@Param("name") String name);
    List<Project> findByOwner(@Param("owner") String owner);
    int update(Project project);
    int delete(String id);
}
