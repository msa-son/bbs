package spectra.issue.domain.spec;

import nara.share.domain.NameValueList;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.spec.sdo.MemberCdo;

public interface MemberService
{
    //
    String registerMember(MemberCdo memberCdo);
    Member findMember(String memberId);
    void modifyMember(String memberId, NameValueList nameValueList);
    void removeMember(String memberId);
}
