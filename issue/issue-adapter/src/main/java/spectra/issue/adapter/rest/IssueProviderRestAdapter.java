package spectra.issue.adapter.rest;

import nara.share.restclient.NaraRestClient;
import nara.share.restclient.RequestBuilder;
import spectra.issue.domain.entity.Issue;
import spectra.issue.domain.spec.IssueProvider;
import spectra.issue.domain.spec.sdo.IssueCdo;

import java.util.List;

public class IssueProviderRestAdapter implements IssueProvider
{
    private NaraRestClient naraRestClient;

    public IssueProviderRestAdapter(NaraRestClient naraRestClient)
    {
        this.naraRestClient = naraRestClient;
    }

    @Override
    public List<Issue> listIssue(String projectId, String status, String memberId, String keyword, int startNo)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.ISSUE_P_LIST)
                            .addQueryParam("projectId", projectId)
                            .addQueryParam("status", status)
                            .addQueryParam("memberId", memberId)
                            .addQueryParam("keyword", keyword)
                            .addQueryParam("start", startNo)
                            .setResponseType(List.class)
        );
    }

    @Override
    public String registerIssue(IssueCdo issueCdo)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.ISSUE_P_BUILD)
                            .setRequestBody(issueCdo)
                            .setResponseType(String.class)
        );
    }

    @Override
    public Issue findIssue(String issueId)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.ISSUE_P_FIND)
                            .addPathParam("issueId", issueId)
                        .setResponseType(Issue.class)
        );
    }
}
