package spectra.issue.kafkaclient.simple;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerTest
{
    //
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    public void test(){
        Consumer consumer = new Consumer();
        consumer.start("test", (ConsumerRecord<String, String> record) -> logger.info("offset = {}, key = {}, value = {}", record.offset(), record.key(), record.value()));
    }
}
