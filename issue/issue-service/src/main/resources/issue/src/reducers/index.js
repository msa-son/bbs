import { combineReducers } from 'redux';

import issue from './issue';
import status from './status';
import project from './project';
import member from './member';


export default combineReducers({
    issue, project, status, member
});
