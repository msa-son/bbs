package spectra.issue.kafkaclient.simple;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Service;

@Service
public class Producer
{
    //
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private KafkaProducer<String, String> producer;

    public Producer()
    {
        //
        try
        {
            Properties properties = PropertiesLoaderUtils.loadProperties(new ClassPathResource("producer.properties"));
            producer = new KafkaProducer<>(properties, new StringSerializer(), new StringSerializer());
        }
        catch (IOException e)
        {
            logger.error(ExceptionUtils.getStackTrace(e));
        }
    }

    public void send(String topic, String message)
    {
        try
        {
            Callback callback = (metadata, exception) -> {
                if (exception != null)
                {
                    System.out.println("Encountered exception " + exception);
                }
            };
            producer.send(new ProducerRecord<>(topic, Integer.toString(message.hashCode()), message), callback);
        }
        catch (Exception e)
        {
            logger.error(ExceptionUtils.getStackTrace(e));
        }
    }
}
