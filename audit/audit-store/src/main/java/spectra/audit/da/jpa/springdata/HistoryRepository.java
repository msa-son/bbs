package spectra.audit.da.jpa.springdata;

import org.springframework.data.repository.PagingAndSortingRepository;
import spectra.audit.da.jpa.jpo.HistoryJpo;

import java.util.List;

public interface HistoryRepository extends PagingAndSortingRepository<HistoryJpo, String>
{
}
