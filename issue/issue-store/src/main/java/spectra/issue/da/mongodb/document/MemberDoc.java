package spectra.issue.da.mongodb.document;

import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;
import spectra.issue.domain.entity.Member;

import javax.persistence.Id;
import java.util.List;
import java.util.stream.Collectors;

@Document(collection = "t_member")
public class MemberDoc
{
    @Id
    private String id;

    private String memberId;
    private String password;
    private String name;
    private String email;
    private String roleId;
    private long createdTime;
    private long updatedTime;

    public MemberDoc()
    {
    }

    public MemberDoc(Member member)
    {
        BeanUtils.copyProperties(member, this);
    }

    public void update(Member member)
    {
        BeanUtils.copyProperties(member, this);
    }

    public Member toDomain()
    {
        Member member = new Member(id);
        BeanUtils.copyProperties(this, member);
        return member;
    }

    public static List<Member> toDomains(List<MemberDoc> memberJpos)
    {
        return memberJpos.stream()
                .map(jpo -> jpo.toDomain())
                .collect(Collectors.toList());
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getMemberId()
    {
        return memberId;
    }

    public void setMemberId(String memberId)
    {
        this.memberId = memberId;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getRoleId()
    {
        return roleId;
    }

    public void setRoleId(String roleId)
    {
        this.roleId = roleId;
    }

    public long getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(long createdTime)
    {
        this.createdTime = createdTime;
    }

    public long getUpdatedTime()
    {
        return updatedTime;
    }

    public void setUpdatedTime(long updatedTime)
    {
        this.updatedTime = updatedTime;
    }
}
