import { combineReducers } from 'redux';

import authentication from './authentication';
import issue from './issue';
import project from './project';
import member from './member';
import status from './status';


export default combineReducers({
    authentication, issue, project, member, status
});
