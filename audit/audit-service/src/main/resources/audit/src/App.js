import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect, NavLink } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider  } from 'react-redux';
import thunk from 'redux-thunk';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import reducers from './reducers';

import history from './common/history';
import ToolBar from './common/ToolBar';

import Issues from './issue/Issues';
import EditIssue from './issue/Edit';
import Projects from './secure/Projects';
import Members from './secure/Members';
import Login from './secure/Login';
import Logout from './secure/Logout';

const store = createStore(reducers, applyMiddleware(thunk));

const TestHeader = () => (
  <header className="primary-header">
    <nav>
      <NavLink to="/bbs/system/login">Login</NavLink>
      <NavLink to="/bbs/main/issues/1">Issues</NavLink>
      <NavLink to="/bbs/main/issue/0">Edit</NavLink>
      <NavLink to="/bbs/main/projects">Projects</NavLink>
      <NavLink to="/bbs/main/members">Members</NavLink>
      <NavLink to="/bbs/system/logout">Logout</NavLink>
    </nav>
  </header>
)

const MainLayout = ({ match }) => {
  return (
    <div >
      <ToolBar />
      <div>
        <Switch>
          <Route path={`${match.path}/issues/:page`} component={Issues} />
          <Route path={`${match.path}/issue/:id`} component={EditIssue} />
          <Route path={`${match.path}/projects`} component={Projects} />
          <Route path={`${match.path}/members`} component={Members} />
          <Redirect to={`${match.path}/issues`} />
        </Switch>
      </div>
    </div>
  )
};

const LoginLayout = ({ match }) => {
  return (
    <div>
      <Switch>
        <Route path={`${match.path}/login`} component={Login} />
        <Route path={`${match.path}/logout`} component={Logout} />
        <Redirect to={`${match.path}/login`} />
      </Switch>
    </div>
  )
};

const SubLayout = ({ match }) => {
  return (
    <div >
      <Switch>
        <Route path={`${match.path}/main`} component={MainLayout} />
        <Route path={`${match.path}/system`} component={LoginLayout} />
        <Redirect to={`${match.path}/system`} />
      </Switch>
    </div>
  )
};

const App = () => {
  return (
    <MuiThemeProvider>
      <Provider store = {store}>
        <Router history={history}>
          <div>
            <TestHeader/>
            <Route path="/bbs" component={SubLayout} />
          </div>
        </Router>
      </Provider>
    </MuiThemeProvider>
  );
};

export default App;
