import {
    MEMBER_SELECTED,
    MEMBER_LIST_GET,
    MEMBER_LIST_GET_SUCCESS,
    MEMBER_LIST_GET_FAILURE
} from './ActionTypes';
import axios from 'axios';


/* selected */
export function selectedMember(ids) {
    return {
        type: MEMBER_SELECTED,
        ids
    };
}

export function getMemberListRequest() {
    return (dispatch) => {
        dispatch(getMemberList());

        return axios.get('http://localhost:9999/api/p/members')
        .then((response) => {
            dispatch(getMemberListSuccess(response.data));
        }).catch((error) => {
            dispatch(getMemberListFailure());
        });
    };
}

export function getMemberList() {
    return {
        type: MEMBER_LIST_GET
    };
}

export function getMemberListSuccess(data) {
    return {
        type: MEMBER_LIST_GET_SUCCESS,
        data
    };
}

export function getMemberListFailure() {
    console.log('getMemberListFailure');
    return {
        type: MEMBER_LIST_GET_FAILURE
    };
}
