package spectra.audit.cp.spring.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spectra.audit.domain.entity.History;
import spectra.audit.domain.logic.HistoryLogic;
import spectra.audit.domain.spec.sdo.HistoryCdo;
import spectra.audit.domain.store.AuditStoreLycler;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class HistorySpringLogic extends HistoryLogic
{
    @Autowired
    public HistorySpringLogic(AuditStoreLycler storeLycler)
    {
        super(storeLycler);
    }

    @Override
    public List<History> listHistory()
    {
        return super.listHistory();
    }

    @Override
    public String registerHistory(HistoryCdo historyCdo)
    {
        return super.registerHistory(historyCdo);
    }

    @Override
    public History findHistory(String historyId)
    {
        return super.findHistory(historyId);
    }

    @Override
    public void removeHistory(String historyId)
    {
        super.removeHistory(historyId);
    }
}
