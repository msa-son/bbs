import React from 'react';
import PropTypes from 'prop-types';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';


class Select extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedData: props.selectedData,
      dataList: props.dataList,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selectedData: nextProps.selectedData,
      dataList: nextProps.dataList
    });
  }

  handleChange = (event, index, value) => {
    this.setState({
      selectedData: value
    });
    this.props.onItemClick(event, index, value);
  }

  render() {
    let label = this.props.label ? this.props.label : 'unknown';
    return (
      <SelectField
        disabled={this.props.disabled}
        multiple={this.props.multiple}
        floatingLabelText={label}
        value={this.state.selectedData}
        onChange={this.handleChange}
        style={{display : 'inline-block'}}
      >
      {this.state.dataList.map((data) => {
        return (
          <MenuItem
            key={data.id}
            value={data.id}
            checked={this.props.multiple && this.state.selectedData && this.state.selectedData.indexOf(data.id) > -1}
            primaryText={data.name}
          />
        );
      })}
      </SelectField>
    );
  }
}

Select.propTypes = {
    onItemClick: PropTypes.func,
};

Select.defaultProps = {
    onItemClick: (event, index, value) => { console.error("onItemClick not defined"); },
};

export default Select;
