import React from 'react';
import { connect } from 'react-redux';

import Select from '../../components/Select';


class ProjectSelect extends React.Component {
  onSelect = (event, index, value) => {
    this.props.selectedProject(value);
  };

  render() {
    return (
     <Select label={'Project'} onItemClick={this.onSelect} {...this.props} />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    status: state.project.list_get.status,
    dataList: state.project.list_get.data
  };
};

export default connect(mapStateToProps)(ProjectSelect);
