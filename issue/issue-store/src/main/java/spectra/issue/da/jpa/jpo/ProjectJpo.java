package spectra.issue.da.jpa.jpo;

import org.springframework.beans.BeanUtils;
import spectra.issue.domain.entity.Project;
import spectra.issue.domain.granule.MemberList;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.util.List;
import java.util.stream.Collectors;

@Entity(name = "t_project")
public class ProjectJpo
{
    @Id
    private String id;

    private String name;
    private String owner;

    @Lob
    private String membersJson;
    private long createdTime;
    private long updatedTime;

    public ProjectJpo()
    {
    }

    public ProjectJpo(Project project)
    {
        BeanUtils.copyProperties(project, this);
        this.membersJson = project.getMembers().toJson();
    }

    public void update(Project project)
    {
        BeanUtils.copyProperties(project, this);
        this.membersJson = project.getMembers().toJson();
    }


    public Project toDomain()
    {
        Project project = new Project(id);
        BeanUtils.copyProperties(this, project);
        project.setMembers(MemberList.fromJson(membersJson));
        return project;
    }

    public static List<Project> toDomains(List<ProjectJpo> projectJpos)
    {
        return projectJpos.stream()
                .map(jpo -> jpo.toDomain())
                .collect(Collectors.toList());
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getOwner()
    {
        return owner;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getMembersJson()
    {
        return membersJson;
    }

    public void setMembersJson(String membersJson)
    {
        this.membersJson = membersJson;
    }

    public long getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(long createdTime)
    {
        this.createdTime = createdTime;
    }

    public long getUpdatedTime()
    {
        return updatedTime;
    }

    public void setUpdatedTime(long updatedTime)
    {
        this.updatedTime = updatedTime;
    }
}
