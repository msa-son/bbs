import {
  STATUS_SELECTED
} from './ActionTypes';


/* selected */
export function selectedStatus(ids) {
  console.log('selectedStatus : '+ids);
  return {
    type: STATUS_SELECTED,
    ids
  };
}
