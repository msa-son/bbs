package spectra.audit.adapter.rest;

import nara.share.restclient.HttpMethod;
import nara.share.restclient.NaraRestUrl;

public enum HistoryRestUrl implements NaraRestUrl
{
    HISTORY_S_BUILD                   ("api/s/history",                                HttpMethod.POST),
    HISTORY_S_LIST                    ("api/s/history",                                HttpMethod.GET),
    HISTORY_S_FIND                    ("api/s/history/{historyId}",                    HttpMethod.GET),
    HISTORY_S_REMOVE                  ("api/s/history/{historyId}",                    HttpMethod.DELETE),

    HISTORY_P_BUILD                   ("api/p/history",                                HttpMethod.POST),
    HISTORY_P_FIND                    ("api/p/history/{historyId}",                    HttpMethod.GET),
    HISTORY_P_LIST                    ("api/p/history",                                HttpMethod.GET),
    ;

    private String restUrl;
    private HttpMethod httpMethod;

    HistoryRestUrl(String restUrl, HttpMethod httpMethod)
    {
        this.restUrl = restUrl;
        this.httpMethod = httpMethod;
    }

    @Override
    public String getUrl()
    {
        return this.restUrl;
    }

    @Override
    public HttpMethod getMethod()
    {
        return this.httpMethod;
    }
}
