package spectra.issue.domain.spec.sdo;

import nara.share.util.json.JsonUtil;
import spectra.issue.domain.entity.Comment;

import java.io.Serializable;

public class CommentCdo implements Serializable
{
    private String issueId;
    private String creator;
    private String content;

    public CommentCdo() {}

    public CommentCdo(String issueId, String creator, String content)
    {
        this.issueId = issueId;
        this.creator = creator;
        this.content = content;
    }

    public static CommentCdo getSample()
    {
        Comment commentSample = Comment.getSample();

        CommentCdo sample = new CommentCdo(commentSample.getIssueId(), commentSample.getCreator(), commentSample.getContent());

        return sample;
    }

    public String toJson()
    {
        return JsonUtil.toJson(this);
    }

    public static CommentCdo fromJson(String json)
    {
        return JsonUtil.fromJson(json, CommentCdo.class);
    }

    public String getIssueId()
    {
        return issueId;
    }

    public void setIssueId(String issueId)
    {
        this.issueId = issueId;
    }

    public String getCreator()
    {
        return creator;
    }

    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public static void main(String[] args)
    {
        //
        System.out.println(getSample());
    }
}
