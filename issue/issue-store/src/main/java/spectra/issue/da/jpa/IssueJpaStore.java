package spectra.issue.da.jpa;

import nara.share.exception.store.AlreadyExistsException;
import nara.share.exception.store.NonExistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import spectra.issue.da.jpa.jpo.IssueJpo;
import spectra.issue.da.jpa.springdata.IssueRepository;
import spectra.issue.domain.entity.Issue;
import spectra.issue.domain.store.IssueStore;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class IssueJpaStore implements IssueStore
{
    @Autowired
    private IssueRepository issueRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public String create(Issue issue)
    {
        String id = issue.getId();
        if(issueRepository.exists(id))
        {
            throw new AlreadyExistsException(String.format("Issue jpo[ID:%s] already exist", id));
        }
        issueRepository.save(new IssueJpo(issue));
        return id;
    }

    @Override
    public Issue retrieve(String id) throws NoSuchElementException
    {
        IssueJpo issueJpo = issueRepository.findOne(id);
        if(issueJpo == null)
        {
            throw new NoSuchElementException(String.format("No Issue jpo[ID:%s] to retrieve", id));
        }
        return issueJpo.toDomain();
    }

    @Override
    public List<Issue> retrieveByProjectId(String projectId)
    {
        List<IssueJpo> issueJpos = issueRepository.findByProjectId(projectId);
        return IssueJpo.toDomains(issueJpos);
    }

    @Override
    public List<Issue> retrieveByTitle(String title)
    {
        List<IssueJpo> issueJpos = issueRepository.findByTitle(title);
        return IssueJpo.toDomains(issueJpos);
    }

    @Override
    public List<Issue> retrieveByAssignee(String assignee)
    {
        List<IssueJpo> issueJpos = issueRepository.findByAssignee(assignee);
        return IssueJpo.toDomains(issueJpos);
    }

    @Override
    public List<Issue> retrieveByStatus(String status)
    {
        List<IssueJpo> issueJpos = issueRepository.findBystatus(status);
        return IssueJpo.toDomains(issueJpos);
    }

    @Override
    public List<Issue> retrieveList(List<String> projectId, List<String> status, List<String> memberId, String searchKeyword, int startNo)
    {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<IssueJpo> query = builder.createQuery(IssueJpo.class);
        Root<IssueJpo> root = query.from(IssueJpo.class);

        List<Predicate> predicates = new ArrayList<>();

        // condition
        if (projectId != null)
        {
            Expression<String> exp = root.get("projectId");
            predicates.add(exp.in(projectId));
        }

        if (status != null)
        {
            Expression<String> exp = root.get("status");
            predicates.add(exp.in(status));
        }

        if (memberId != null)
        {
            predicates.add(
                            builder.or(
                                            root.get("creator").in(memberId),
                                            root.get("assignee").in(memberId)
                            )

            );
        }

        if (searchKeyword != null)
        {
            predicates.add(
                            builder.or(
                                            builder.like(root.get("title"), "%" + searchKeyword + "%"),
                                            builder.like(root.get("questionContent"), "%" + searchKeyword + "%"),
                                            builder.like(root.get("answerContent"), "%" + searchKeyword + "%")
                            )
            );
        }

        query.where(predicates.toArray(new Predicate[]{}));
        query.orderBy(builder.desc(root.get("createdTime")));
        query.select(root);

        TypedQuery<IssueJpo> typesQuery = entityManager.createQuery(query)
                        .setFirstResult(startNo)
                        .setMaxResults(10);

        List<IssueJpo> issueJpos = typesQuery.getResultList();
        return IssueJpo.toDomains(issueJpos);
    }

    @Override
    public void update(Issue issue)
    {
        String id = issue.getId();
        if(!issueRepository.exists(id))
        {
            throw new NonExistenceException(String.format("No Issue jpo[ID:%s] to update", id));
        }
        IssueJpo issueJpo = issueRepository.findOne(id);
        issueJpo.update(issue);
        issueRepository.save(issueJpo);
    }

    @Override
    public void delete(Issue issue)
    {
        issueRepository.delete(issue.getId());
    }
}