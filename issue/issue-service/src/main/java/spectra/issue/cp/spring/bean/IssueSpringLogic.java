package spectra.issue.cp.spring.bean;

import nara.share.domain.NameValueList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spectra.issue.domain.entity.Issue;
import spectra.issue.domain.logic.IssueLogic;
import spectra.issue.domain.spec.sdo.IssueCdo;
import spectra.issue.domain.store.IssueStoreLycler;

import javax.transaction.Transactional;

@Service
@Transactional
public class IssueSpringLogic extends IssueLogic
{
    @Autowired
    public IssueSpringLogic(IssueStoreLycler storeLycler)
    {
        super(storeLycler);
    }

    @Override
    public String registerIssue(IssueCdo issueCdo)
    {
        return super.registerIssue(issueCdo);
    }

    @Override
    public Issue findIssue(String issueId)
    {
        return super.findIssue(issueId);
    }

    @Override
    public void modifyIssue(String issueId, NameValueList nameValueList)
    {
        super.modifyIssue(issueId, nameValueList);
    }

    @Override
    public void removeIssue(String issueId)
    {
        super.removeIssue(issueId);
    }
}
