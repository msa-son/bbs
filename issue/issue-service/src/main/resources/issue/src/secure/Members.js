import React from 'react';
import { connect } from 'react-redux';


class Members extends React.Component {
  componentWillReceiveProps(nextProps){
    console.log("componentWillReceiveProps: " + JSON.stringify(nextProps));
  }

  render(){
    console.log(this.props);
    return (
      <div>
        Members : {this.props.currentUser}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    currentUser: state.member.status.currentUser
  };
}

export default connect(mapStateToProps)(Members);
