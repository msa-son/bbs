package spectra.issue.kafkaclient.stream;

import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Service;

@Service
public class StreamConsumer
{
    //
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private KafkaConsumer<String, Long> consumer;

    public StreamConsumer()
    {
        //
        try
        {
            Properties properties = PropertiesLoaderUtils.loadProperties(new ClassPathResource("consumer.properties"));
            consumer = new KafkaConsumer<>(properties, new StringDeserializer(), new LongDeserializer());
        }
        catch (IOException e)
        {
            logger.error(ExceptionUtils.getStackTrace(e));
        }
    }

    private void startConsumer(String topic)
    {
        try
        {
            this.consumer.subscribe(Arrays.asList(topic));
            while (true)
            {
                ConsumerRecords<String, Long> records = this.consumer.poll(100);
                for (ConsumerRecord<String, Long> record : records)
                {
                    logger.info("offset = {}, key = {}, value = {}", record.offset(), record.key(), record.value());
                }
            }
        }
        catch (Exception e)
        {
            logger.error(ExceptionUtils.getStackTrace(e));
        }
        finally
        {
            if (this.consumer != null)
            {
                this.consumer.close();
            }
        }
    }

    public void start()
    {
        Thread thread = new Thread(() -> startConsumer("WordsWithCountsTopic"));
        thread.start();
    }
}
