package spectra.issue.da.mybatis;

import nara.share.exception.store.AlreadyExistsException;
import nara.share.exception.store.NonExistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spectra.issue.da.mybatis.mapper.CommentMapper;
import spectra.issue.domain.entity.Comment;
import spectra.issue.domain.store.CommentStore;

import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class CommentMybatisStore implements CommentStore
{
    @Autowired
    private CommentMapper commentMapper;

    @Override
    public String create(Comment comment)
    {
        String id = comment.getId();
        if(commentMapper.exists(id))
        {
            throw new AlreadyExistsException(String.format("Comment [ID:%s already exit", id));
        }
        commentMapper.insert(comment);
        return id;
    }

    @Override
    public Comment retrieve(String id) throws NoSuchElementException
    {
        Comment comment = commentMapper.findOne(id);
        if(comment == null)
        {
            throw new NoSuchElementException(String.format("No Comment [ID:%s] to retrieve", id));
        }
        return comment;
    }

    @Override
    public Comment retrieveByIdAndCreator(String id, String creator) throws NoSuchElementException
    {
        Comment comment = commentMapper.findByIdAndCreator(id, creator);
        if(comment == null)
        {
            throw new NoSuchElementException(String.format("No Comment [ID:%s] to retrieve", id));
        }
        return comment;
    }

    @Override
    public List<Comment> retrieveByIssueId(String issueId)
    {
        return commentMapper.findByIssueId(issueId);
    }

    @Override
    public void update(Comment comment)
    {
        String id = comment.getId();
        if(!commentMapper.exists(id))
        {
            throw new NonExistenceException(String.format("No Comment [ID:%s] to update", id));
        }
        commentMapper.update(comment);
    }

    @Override
    public void delete(Comment comment)
    {
        commentMapper.delete(comment.getId());
    }
}