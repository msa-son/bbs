package spectra.issue.domain.spec;

import nara.share.domain.NameValueList;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.spec.sdo.MemberCdo;

import java.util.List;

public interface MemberProvider
{
    //
    List<Member> listMember();
    String registerMember(MemberCdo memberCdo);
    Member findMember(String memberId);
    void modifyMember(String memberId, NameValueList nameValueList);

    boolean existsMemberId(String memberId);

    Member login(String memberId, String password);
    void logout(String memberId);
}
