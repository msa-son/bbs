import React from 'react';
import {
  connect
} from 'react-redux';
import TextField from 'material-ui/TextField';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import ProjectSelect from './create/ProjectSelect';
import MdEditor from '../components/MdEditor';
import {
  postIssueRequest
} from '../actions/issue';


class Create extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      projectId:'',
      title:'',
      questionContent:''
    }
  }

  close(){
    this.props.onClose();
    this.setState({
      projectId:'',
      title:'',
      questionContent:''
    });
  }

  handleClose = () => {
    this.close();
  };

  handleSubmit = () => {
    const data = {
      ...this.state,
      creator: this.props.currentUser.id
    };
    this.props.postIssueRequest(data);
    this.close();
  };

  handleProject = (value) => {
    this.setState({
        ...this.state.data,
        projectId:value
    })
  }

  handleTitle = (event, newValue) => {
    this.setState({
        ...this.state.data,
        title:newValue
    })
  }

  handleQuestionContent = (editor, data, value) => {
    this.setState({
        ...this.state.data,
        questionContent:value
    })
  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleClose}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onClick={this.handleSubmit}
      />,
    ];

    return (
      <Dialog
        title="Issue Create"
        actions={actions}
        modal={false}
        open={this.props.show}
        onRequestClose={this.handleClose}
        autoScrollBodyContent={true}
      >
        <div>
          <ProjectSelect selectedProject={this.handleProject}/>
        </div>
        <div>
          <TextField hintText="Title" onChange={this.handleTitle}/>
        </div>
        <div>
          Question Content
          <MdEditor content={this.state.questionContent} onChange={this.handleQuestionContent}/>
        </div>
      </Dialog>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentUser: state.member.status.currentUser
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    postIssueRequest: (data) => {
      dispatch(postIssueRequest(data));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Create);
