package spectra.issue.domain.spec;

import nara.share.domain.NameValueList;
import spectra.issue.domain.entity.Comment;
import spectra.issue.domain.spec.sdo.CommentCdo;

public interface CommentService
{
    //
    String registerComment(CommentCdo commentCdo);
    Comment findComment(String commentId);
    void modifyComment(String commentId, NameValueList nameValueList);
    void removeComment(String commentId);
}
