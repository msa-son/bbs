package spectra.issue.da.mybatis.typehandler;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;
import spectra.issue.domain.granule.MemberList;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes(MemberList.class)
@MappedJdbcTypes(JdbcType.VARCHAR)
public class MemberListTypeHandler implements TypeHandler<MemberList>
{
    @Override
    public void setParameter(PreparedStatement preparedStatement, int i, MemberList memberList, JdbcType jdbcType) throws SQLException
    {
        preparedStatement.setString(i, memberList != null ? memberList.toJson() : null);
    }

    @Override
    public MemberList getResult(ResultSet resultSet, String columnName) throws SQLException
    {
        return MemberList.fromJson(resultSet.getString(columnName));
    }

    @Override
    public MemberList getResult(ResultSet resultSet, int columnIndex) throws SQLException
    {
        return MemberList.fromJson(resultSet.getString(columnIndex));
    }

    @Override
    public MemberList getResult(CallableStatement callableStatement, int columnIndex) throws SQLException
    {
        return MemberList.fromJson(callableStatement.getString(columnIndex));
    }
}
