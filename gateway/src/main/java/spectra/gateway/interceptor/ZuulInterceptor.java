package spectra.gateway.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import spectra.gateway.bean.Member;
import spectra.gateway.security.JwtValidator;
import spectra.gateway.security.PermissionValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class ZuulInterceptor implements HandlerInterceptor
{
    @Autowired
    private JwtValidator jwtValidator;

    @Autowired
    private PermissionValidator permissionValidator;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
                    throws Exception
    {
        if (!request.getRequestURI().startsWith("/api/s/"))
        {
            return true;
        }

        // JWT 체크
        Member member = jwtValidator.validate(request);

        String method = request.getMethod(); // GET
        String uri = request.getRequestURI(); // /projects/12345

        // RBAC 체크
        permissionValidator.validate(uri, method, member.getRoleId());

        request.setAttribute("member", member);

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                    ModelAndView modelAndView) throws Exception
    {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
                    throws Exception
    {
    }

}