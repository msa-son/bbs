package spectra.audit.adapter.rest;

import nara.share.restclient.NaraRestClient;
import nara.share.restclient.RequestBuilder;
import spectra.audit.domain.entity.History;
import spectra.audit.domain.spec.HistoryProvider;
import spectra.audit.domain.spec.sdo.HistoryCdo;

import java.util.List;

public class HistoryProviderRestAdapter implements HistoryProvider
{
    private NaraRestClient naraRestClient;

    public HistoryProviderRestAdapter(NaraRestClient naraRestClient)
    {
        this.naraRestClient = naraRestClient;
    }

    @Override
    public String registerHistory(HistoryCdo historyCdo)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(HistoryRestUrl.HISTORY_P_BUILD)
                                        .setRequestBody(historyCdo)
                                        .setResponseType(String.class)
        );
    }

    @Override
    public History findHistory(String historyId)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(HistoryRestUrl.HISTORY_P_FIND)
                                        .addPathParam("historyId", historyId)
                                        .setResponseType(History.class)
        );
    }

    @Override
    public List<History> listHistory()
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(HistoryRestUrl.HISTORY_P_LIST)
                                        .setResponseType(List.class)
        );
    }
}
