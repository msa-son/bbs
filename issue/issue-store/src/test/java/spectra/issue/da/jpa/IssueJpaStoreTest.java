package spectra.issue.da.jpa;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import spectra.issue.da.IssueStoreTestApplication;
import spectra.issue.domain.entity.Issue;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = IssueStoreTestApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class IssueJpaStoreTest
{
    @Autowired
    private IssueJpaStore issueJpaStore;

    private static String PROJECT_ID = "1";
    private static String TITLE = "Test Title 2";
    private static String QUESTION_CONTENT = "Test Question Content 1";
    private static String CREATOR = "tester";
    private static String ASSIGNEE = "tester2";
    private static String STATUS = "100";

    private String issueId;

    @Before
    public void setUp() throws Exception
    {
        issueId = issueJpaStore.create(new Issue(PROJECT_ID, TITLE, QUESTION_CONTENT, CREATOR));
    }

    @Test
    public void testCreate() throws Exception
    {
        issueId = issueJpaStore.create(new Issue(PROJECT_ID, TITLE, QUESTION_CONTENT, CREATOR));
        System.out.println("[TEST INFO] issue create => ID : " + issueId);
    }

    @Test(expected = NoSuchElementException.class)
    public void testRetrieve() throws Exception
    {
        Issue issue = issueJpaStore.retrieve(issueId);
        Assert.assertEquals(issueId, issue.getId());
        Assert.assertEquals(TITLE, issue.getTitle());
        Assert.assertEquals(CREATOR, issue.getCreator());
        System.out.println("[TEST INFO] " + issue.toString());

        issueJpaStore.retrieve("None");
    }

    @Test
    public void testRetrieveByProjectId() throws Exception
    {
        List<Issue> issues = issueJpaStore.retrieveByProjectId(PROJECT_ID);
        Assert.assertNotEquals(0, issues.size());
        Assert.assertEquals(PROJECT_ID, issues.get(0).getProjectId());
        System.out.println("[TEST INFO] Issue Size " + issues.size());
        System.out.println("[TEST INFO] " + issues.get(0).toString());
    }

    @Test
    public void testRetrieveByTitle() throws Exception
    {
        List<Issue> issues = issueJpaStore.retrieveByTitle(TITLE);
        Assert.assertNotEquals(0, issues.size());
        Assert.assertEquals(TITLE, issues.get(0).getTitle());
        System.out.println("[TEST INFO] Issue Size" + issues.size());
        System.out.println("[TEST INFO] " + issues.get(0).toString());
    }

    @Test
    public void testRetrieveByAssignee() throws Exception
    {
        // find
        Issue issue = issueJpaStore.retrieve(issueId);
        issue.setAssignee(ASSIGNEE);

        // update
        issueJpaStore.update(issue);

        // find by assignee
        List<Issue> issues = issueJpaStore.retrieveByAssignee(ASSIGNEE);
        Assert.assertNotEquals(0, issues.size());
        Assert.assertEquals(ASSIGNEE, issues.get(0).getAssignee());
        System.out.println("[TEST INFO] Issue Size" + issues.size());
        System.out.println("[TEST INFO] " + issues.get(0).toString());
    }

    @Test
    public void testRetrieveByStatus() throws Exception
    {
        List<Issue> issues = issueJpaStore.retrieveByStatus(STATUS);
        Assert.assertNotEquals(0, issues.size());
        Assert.assertEquals(STATUS, issues.get(0).getStatus());
        System.out.println("[TEST INFO] Issue Size" + issues.size());
        System.out.println("[TEST INFO] " + issues.get(0).toString());
    }

    @Test
    public void testRetrieveList() throws Exception
    {
        List projectIds = new ArrayList();
        projectIds.add("1");

        List statuses = new ArrayList();
        statuses.add("100");

        List memberIds = null;
        memberIds = new ArrayList();
        memberIds.add("tester");

        String searchKeyword = null;
        searchKeyword = "Test";

        List<Issue> issues = issueJpaStore.retrieveList(projectIds, statuses, memberIds, searchKeyword, 0);

        Assert.assertNotEquals(0, issues.size());
        Assert.assertEquals(STATUS, issues.get(0).getStatus());
        System.out.println("[TEST INFO] Issue Size" + issues.size());
        System.out.println("[TEST INFO] " + issues.get(0).toString());
    }


    @Test
    public void testUpdate() throws Exception
    {
        Issue issue = issueJpaStore.retrieve(issueId);
        System.out.println("[TEST INFO] 0. Before update => " + issue.toString());

        issue.setTitle("Test Title 2");
        issue.setQuestionContent("Test Question Content 2");
        issueJpaStore.update(issue);
        System.out.println("[TEST INFO] 1. After update => " + issue.toString());
    }

    @Test
    public void testDelete() throws Exception
    {
        Issue issue = issueJpaStore.retrieve(issueId);
        issueJpaStore.delete(issue);
    }
}
