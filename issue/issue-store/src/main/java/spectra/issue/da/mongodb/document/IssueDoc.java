package spectra.issue.da.mongodb.document;

import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;
import spectra.issue.domain.entity.Issue;

import javax.persistence.Id;
import javax.persistence.Lob;
import java.util.List;
import java.util.stream.Collectors;

@Document(collection = "t_issue")
public class IssueDoc
{
    @Id
    private String id;

    private String projectId;
    private String title;
    @Lob
    private String questionContent;
    @Lob
    private String answerContent;
    private String creator;
    private String assignee;
    private String status;
    private long resolvedTime;
    private long createdTime;
    private long updatedTime;

    public IssueDoc()
    {

    }

    public IssueDoc(Issue issue)
    {
        BeanUtils.copyProperties(issue, this);
    }

    public void update(Issue issue)
    {
        BeanUtils.copyProperties(issue, this);
    }

    public Issue toDomain()
    {
        Issue issue = new Issue(id);
        BeanUtils.copyProperties(this, issue);
        return issue;
    }

    public static List<Issue> toDomains(List<IssueDoc> issueJpos)
    {
        return issueJpos.stream()
                .map(jpo -> jpo.toDomain())
                .collect(Collectors.toList());
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getQuestionContent()
    {
        return questionContent;
    }

    public void setQuestionContent(String questionContent)
    {
        this.questionContent = questionContent;
    }

    public String getAnswerContent()
    {
        return answerContent;
    }

    public void setAnswerContent(String answerContent)
    {
        this.answerContent = answerContent;
    }

    public String getCreator()
    {
        return creator;
    }

    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    public String getAssignee()
    {
        return assignee;
    }

    public void setAssignee(String assignee)
    {
        this.assignee = assignee;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public long getResolvedTime()
    {
        return resolvedTime;
    }

    public void setResolvedTime(long resolvedTime)
    {
        this.resolvedTime = resolvedTime;
    }

    public long getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(long createdTime)
    {
        this.createdTime = createdTime;
    }

    public long getUpdatedTime()
    {
        return updatedTime;
    }

    public void setUpdatedTime(long updatedTime)
    {
        this.updatedTime = updatedTime;
    }
}
