import {
  COMMENT_LIST_GET,
  COMMENT_LIST_GET_SUCCESS,
  COMMENT_LIST_GET_FAILURE,
  COMMENT_POST,
  COMMENT_POST_SUCCESS,
  COMMENT_POST_FAILURE
} from './ActionTypes';
import axios from '../common/axios'

import objectUtil from '../common/js/objectUtil';


/* postIssue */
export function postIssueRequest(data) {
  console.log(JSON.stringify(data));
  return (dispatch) => {
    dispatch(postIssue());

    return axios.post('http://localhost:9999/api/s/issues/612a661c-59c8-4f1d-a908-9dc58e761f48', objectUtil.toNameValues(data, ['assignee', 'resolvedTime', 'questionContent', 'answerContent']))
      .then((response) => {
        dispatch(postIssueSuccess(response.data));
      }).catch((error) => {
        dispatch(postIssueFailure());
      });
  };
}

export function postIssue() {
  return {
    type: ISSUE_POST
  };
}

export function postIssueSuccess(data) {
  return {
    type: ISSUE_POST_SUCCESS,
    data
  };
}

export function postIssueFailure() {
  console.log('postIssueFailure');
  return {
    type: ISSUE_POST_FAILURE
  };
}

/* getIssueList */
export function getIssueListRequest(request, keyword = '', start = 0) {
  console.log(request);
  return (dispatch) => {
    dispatch(getIssueList());

    let params = 'start=' + (start * 10)
    if (request.projectIds.length > 0) {
      params += '&projectId=' + request.projectIds;
    }
    if (request.memberIds.length > 0) {
      params += '&memberId=' + request.memberIds;
    }
    if (request.status.length > 0) {
      params += '&status=' + request.status;
    }
    if (keyword) {
      params += '&keyword=' + keyword;
    }

    console.log('http://localhost:9999/api/p/issues?' + params);
    return axios.get('http://localhost:9999/api/p/issues?' + params)
      .then((response) => {
        dispatch(getIssueListSuccess(response.data));
      }).catch((error) => {
        dispatch(getIssueListFailure());
      });
  };
}

export function getIssueList() {
  console.log('getIssueList');
  return {
    type: ISSUE_LIST_GET
  };
}

export function getIssueListSuccess(data) {
  console.log('getIssueListSuccess');
  return {
    type: ISSUE_LIST_GET_SUCCESS,
    data
  };
}

export function getIssueListFailure() {
  console.log('getIssueListFailure');
  return {
    type: ISSUE_LIST_GET_FAILURE
  };
}
