package spectra.issue.domain.spec.sdo;

import nara.share.domain.annote.Optional;
import nara.share.util.json.JsonUtil;
import spectra.issue.domain.entity.Issue;
import spectra.issue.domain.entity.Project;

import java.io.Serializable;

public class ProjectCdo implements Serializable
{
    private String name;
    private String owner;

    public ProjectCdo() {}

    public ProjectCdo(String name, String owner)
    {
        this.name = name;
        this.owner = owner;
    }

    public static ProjectCdo getSample()
    {
        String projectId = Project.getSample().getId();
        Project projectSample = Project.getSample();

        ProjectCdo sample = new ProjectCdo(projectSample.getName(), projectSample.getOwner());

        return sample;
    }

    public String toJson()
    {
        return JsonUtil.toJson(this);
    }

    public static ProjectCdo fromJson(String json)
    {
        return JsonUtil.fromJson(json, ProjectCdo.class);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getOwner()
    {
        return owner;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public static void main(String[] args)
    {
        //
        System.out.println(getSample());
    }
}
