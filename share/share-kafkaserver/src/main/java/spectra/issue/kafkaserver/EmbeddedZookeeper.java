package spectra.issue.kafkaserver;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.apache.zookeeper.server.ServerConfig;
import org.apache.zookeeper.server.ZooKeeperServerMain;
import org.apache.zookeeper.server.quorum.QuorumPeerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Service;

@Service
public class EmbeddedZookeeper
{
    //
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Executor executor = Executors.newSingleThreadExecutor();
    private Properties properties;
    private ZooKeeperServerMain zooKeeperServerMain = new ZooKeeperServerMain();
    private ServerConfig serverConfig = new ServerConfig();

    public EmbeddedZookeeper()
    {
        //
        try
        {
            properties = PropertiesLoaderUtils.loadProperties(new ClassPathResource("zookeeper.properties"));
            QuorumPeerConfig quorumPeerConfig = new QuorumPeerConfig();
            quorumPeerConfig.parseProperties(properties);
            serverConfig.readFrom(quorumPeerConfig);
        }
        catch (IOException | QuorumPeerConfig.ConfigException e)
        {
            e.printStackTrace();
        }
    }

    @PostConstruct
    public void start()
    {
        //
        cleanDataDir();

        logger.info("start " + this.getClass().getSimpleName() + " " + serverConfig.getClientPortAddress().toString().replace("/", ""));
        executor.execute(() -> {
            try
            {
                zooKeeperServerMain.runFromConfig(serverConfig);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        });
    }

    private void cleanDataDir()
    {
        //
        String dataDir = properties.getProperty("dataDir");
        logger.debug("deleteDirectory : " + dataDir);
        try
        {
            FileUtils.deleteDirectory(new File(dataDir));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
