import React from 'react';
import { connect } from 'react-redux';

import Select from '../components/Select';
import { selectedProject, getProjectListRequest } from '../actions/project';


class ProjectSelect extends React.Component {
  componentDidMount() {
    this.props.getProjectListRequest();
  }

  onSelect = (event, index, value) => {
    this.props.selectedProject(value);
  };

  render() {
    return (
     <Select label={'Project'} onItemClick={this.onSelect} {...this.props} />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    status: state.project.list_get.status,
    dataList: state.project.list_get.data
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getProjectListRequest: () => {
      dispatch(getProjectListRequest());
    },
    selectedProject: (value) => {
      dispatch(selectedProject(value));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectSelect);
