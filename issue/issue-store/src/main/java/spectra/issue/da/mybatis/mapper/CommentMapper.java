package spectra.issue.da.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import spectra.issue.domain.entity.Comment;

import java.util.List;

@Mapper
public interface CommentMapper
{
    void insert(Comment comment);
    boolean exists(String id);
    Comment findOne(String id);
    Comment findByIdAndCreator(@Param("id")String id, @Param("creator")String creator);
    List<Comment> findByIssueId(@Param("issueId") String issueId);
    int update(Comment comment);
    int delete(String id);
}
