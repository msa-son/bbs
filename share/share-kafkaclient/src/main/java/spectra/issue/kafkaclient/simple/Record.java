package spectra.issue.kafkaclient.simple;

import org.apache.kafka.clients.consumer.ConsumerRecord;

@FunctionalInterface
public interface Record
{
	void process(ConsumerRecord<String, String> record);
}