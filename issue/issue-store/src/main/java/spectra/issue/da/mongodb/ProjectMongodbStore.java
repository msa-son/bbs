package spectra.issue.da.mongodb;

import nara.share.exception.store.AlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import spectra.issue.da.mongodb.document.ProjectDoc;
import spectra.issue.domain.entity.Project;
import spectra.issue.domain.store.ProjectStore;

import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class ProjectMongodbStore implements ProjectStore
{
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public String create(Project project)
    {
        String id = project.getId();
        Query query = new Query(Criteria.where("id").is(id));
        if (mongoTemplate.exists(query, ProjectDoc.class)) {
            throw new AlreadyExistsException(String.format("Project document[ID:%s already exit", id));
        }

        mongoTemplate.save(new ProjectDoc(project));
        return id;
    }

    @Override
    public Project retrieve(String id) throws NoSuchElementException
    {
        ProjectDoc projectDoc = mongoTemplate.findById(id, ProjectDoc.class);
        if(projectDoc == null)
        {
            throw new NoSuchElementException(String.format("No Project document[ID:%s] to retrieve", id));
        }
        return projectDoc.toDomain();
    }

    @Override
    public List<Project> retrieveByName(String name)
    {
        Query query = new Query(Criteria.where("name").is(name));
        List<ProjectDoc> projectDocs = mongoTemplate.find(query, ProjectDoc.class);

        return ProjectDoc.toDomains(projectDocs);
    }

    @Override
    public List<Project> retrieveByOwner(String owner)
    {
        Query query = new Query(Criteria.where("owner").is(owner));
        List<ProjectDoc> projectDocs = mongoTemplate.find(query, ProjectDoc.class);

        return ProjectDoc.toDomains(projectDocs);
    }

    @Override
    public List<Project> retrieveList()
    {
        //sorting
        Query query = new Query();
        query.with(new Sort(Sort.Direction.ASC,"name"));
        List<ProjectDoc> projectDocs =  mongoTemplate.find(query, ProjectDoc.class);
        return ProjectDoc.toDomains(projectDocs);
    }

    @Override
    public void update(Project project)
    {
        String id = project.getId();
        ProjectDoc projectDoc = mongoTemplate.findById(id , ProjectDoc.class);
        if (projectDoc == null) {
            throw new AlreadyExistsException(String.format("Project document[ID:%s  exit", id));
        }

        projectDoc.update(project);
        mongoTemplate.save(projectDoc);
    }

    @Override
    public void delete(Project project)
    {
        mongoTemplate.remove(new ProjectDoc(project));
    }
}