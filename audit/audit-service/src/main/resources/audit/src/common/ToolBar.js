import React from 'react';
import { withRouter } from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {Toolbar, ToolbarGroup, ToolbarSeparator} from 'material-ui/Toolbar';

import ProjectSelect from './ProjectSelect';
import StatusSelect from './StatusSelect';
import MemberSelect from './MemberSelect';
import ToolBarMenu from './ToolBarMenu';
import Create from '../issue/Create'


class ToolBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false
    };
  }

  handleSearch = () => {
    this.props.history.push('/bbs/main/issues/1');
  };

  handleOpen = () => {
    this.setState({
      open: true
    });
  };

  handleClose = () => {
    this.setState({
      open: false
    });
  };

  render() {
    return (
      <div>
        <Toolbar>
          <ToolbarGroup firstChild={true}>
            <ProjectSelect multiple={true}/>
            <StatusSelect multiple={true}/>
            <MemberSelect multiple={true}/>
            <TextField hintText="Search"/>
            <RaisedButton label="Search" onClick={this.handleSearch}/>
            <ToolbarSeparator />
          </ToolbarGroup>
          <ToolbarGroup>
            <RaisedButton label="Create" primary={true} onClick={this.handleOpen}/>
            <ToolbarSeparator />
            <ToolBarMenu/>
          </ToolbarGroup>
        </Toolbar>
        <Create show={this.state.open}
          onClose={this.handleClose}/>
      </div>
    );
  }
}

export default withRouter(ToolBar);
