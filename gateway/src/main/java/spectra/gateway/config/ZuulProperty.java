package spectra.gateway.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = "classpath:/application.yml", factory = YamlPropertySourceFactory.class)
@ConfigurationProperties(prefix = "zuul")
public class ZuulProperty
{
	private Routes routes;

	public static class Routes
	{

	}

}
