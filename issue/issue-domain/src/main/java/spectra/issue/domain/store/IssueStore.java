package spectra.issue.domain.store;

import spectra.issue.domain.entity.Issue;

import java.util.List;
import java.util.NoSuchElementException;

public interface IssueStore
{
    String create(Issue issue);
    Issue retrieve(String id) throws NoSuchElementException;
    List<Issue> retrieveByProjectId(String projectId);
    List<Issue> retrieveByTitle(String title);
    List<Issue> retrieveByAssignee(String assignee);
    List<Issue> retrieveByStatus(String status);
    List<Issue> retrieveList(List<String> projectId, List<String> status, List<String> memberId, String searchKeyword, int startNo);

    void update(Issue issue);
    void delete(Issue issue);
}
