package spectra.audit.da.jpa;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import spectra.audit.da.AuditStoreTestApplication;
import spectra.audit.domain.entity.History;

import java.util.List;
import java.util.NoSuchElementException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AuditStoreTestApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class HistoryJpaStoreTest
{
    @Autowired
    private HistoryJpaStore historyJpaStore;

    private static final String METHOD = "GET";
    private static final String ACTION_TYPE = "";
    private static final String URL = "/api/p/members";
    private static final String IP = "127.0.0.1";
    private static final String ENTITY_ID = "541c2d42-db1b-4533-8142-61a3951b2ff7";
    private static final String JWT = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJtYW5hZ2VyIiwiaWF0IjoxNTEyMTEzMTQzLCJzdWIiOiJ7XCJtZW1iZXJJZFwiIDogXCJtYW5hZ2VyXCIsIFwibmFtZVwiIDogXCLrp6Tri4jsoIBcIiwgXCJlbWFpbFwiIDogXCJrbWhhbkBzcGVjdHJhLmNvLmtyXCIsIFwicm9sZUlkXCIgOiBcIm1hbmFnZXJcIn0iLCJleHAiOjE1MTQ3MDUxNDN9.2rzxwBjXM9ahWw6hHrfD-KbaMZTj83PnSMZrGI14-oc";

    private String historyId;

    @Before
    public void setUp()
    {
        historyId = historyJpaStore.create(History.getSample());
    }

    @Test
    public void testCreate()
    {
        historyId = historyJpaStore.create(new History(METHOD, ACTION_TYPE, URL, IP, ENTITY_ID, JWT));
        System.out.println("[TEST INFO] comment create => ID : " + historyId);
    }

    @Test(expected = NoSuchElementException.class)
    public void testRetrieve() throws Exception
    {
        History history = historyJpaStore.retrieve(historyId);
        Assert.assertEquals(historyId, history.getId());
        System.out.println("[TEST INFO] " + history.toString());

        historyJpaStore.retrieve("None");
    }

    @Test
    public void testRetrieveList() throws Exception
    {
        List<History> histories = historyJpaStore.retrieveList();
        Assert.assertNotEquals(0, histories.size());
        System.out.println("[TEST INFO] size: " + histories.size());
    }

    @Test
    public void testDelete() throws Exception
    {
        History history = historyJpaStore.retrieve(historyId);
        historyJpaStore.delete(history);
    }
}
