import React from 'react';

const NotFound = ({history}) => {
  return (
    <div>
      <h3>찾는 페이지가 없습니다. 메인페이지로 돌아가시려면 아래 버튼을 눌러주세요.</h3>
      <button onClick={()=>{history.push('/posts')}}>
        버튼
      </button>
    </div>
  );
};
