package spectra.issue.adapter.rest;

import nara.share.domain.NameValueList;
import nara.share.restclient.NaraRestClient;
import nara.share.restclient.RequestBuilder;
import spectra.issue.domain.entity.Project;
import spectra.issue.domain.spec.ProjectService;
import spectra.issue.domain.spec.sdo.ProjectCdo;

public class ProjectServiceRestAdapter implements ProjectService
{
    private NaraRestClient naraRestClient;

    public ProjectServiceRestAdapter(NaraRestClient naraRestClient)
    {
        this.naraRestClient = naraRestClient;
    }

    @Override
    public String registerProject(ProjectCdo projectCdo)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.PROJECT_S_BUILD)
                            .setRequestBody(projectCdo)
                            .setResponseType(String.class)
        );
    }

    @Override
    public Project findProject(String projectId)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.PROJECT_S_FIND)
                            .addPathParam("projectId", projectId)
                            .setResponseType(Project.class)
        );
    }

    @Override
    public void modifyProject(String projectId, NameValueList nameValueList)
    {
        naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.PROJECT_S_MODIFY)
                            .addPathParam("projectId", projectId)
                            .setRequestBody(nameValueList)
        );
    }

    @Override
    public void removeProject(String projectId)
    {
        naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.PROJECT_S_REMOVE)
                            .addPathParam("projectId", projectId)
        );
    }
}
