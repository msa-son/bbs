package spectra.issue.kafkaclient.simple;

import org.junit.Test;

public class ProducerTest
{

    @Test
    public void test()
    {
        Producer producer = new Producer();
        producer.send("test", "hello my first message.");
        try
        {
            Thread.sleep(1000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
