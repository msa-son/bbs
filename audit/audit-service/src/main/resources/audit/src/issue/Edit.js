import React from 'react';
import { connect } from 'react-redux';
import AppBar from 'material-ui/AppBar';
import DatePicker from 'material-ui/DatePicker';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';

import MdEditor from '../components/MdEditor';
import StatusSelect from '../common/StatusSelect';
import MemberSelect from '../common/MemberSelect';
import Activity from './Activity';
import { getIssueRequest } from '../actions/issue';

class Edit extends React.Component {
  componentDidMount() {
      this.props.getIssueRequest(this.props.match.params.id);
  }

  render = () => {
    let title = this.props.data.project+' > '+this.props.data.id+' : '+this.props.data.title;

    return (
      <div style={{ padding: '20px' }}>
        <AppBar
          iconElementLeft={<IconButton/>}
          iconElementRight={<StatusSelect/>}
          title={title}
        />
        <div>
          <MemberSelect label={'Creator'} selectedData={this.props.data.creator}/>
        </div>
        <div>
          <MemberSelect label={'Assignee'} selectedData={this.props.data.assignee}/>
        </div>
        <div>
          <DatePicker floatingLabelText="Create Date" autoOk={true} value={this.props.data.createdTime ? new Date(this.props.data.createdTime) : null} />
        </div>
        <div>
          <DatePicker floatingLabelText="Resolve Date" autoOk={true} value={this.props.data.resolvedTime ? new Date(this.props.data.resolvedTime) : null}/>
        </div>
        <div>
          <Subheader style={{ padding: '0px' }}>Question Content</Subheader>
          <MdEditor content={this.props.data.questionContent}/>
        </div>
        <div>
          <Subheader style={{ padding: '0px' }}>Answer Content</Subheader>
          <MdEditor content={this.props.data.answerContent}/>
        </div>
        <div>
          <Activity/>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        status: state.issue.get.status,
        data: state.issue.get.data
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getIssueRequest: (issueId) => {
            dispatch(getIssueRequest(issueId));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Edit);
