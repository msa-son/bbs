package spectra.audit.da.jpa.jpo;

import org.springframework.beans.BeanUtils;
import spectra.audit.domain.entity.History;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;
import java.util.stream.Collectors;

@Entity(name = "t_history")
public class HistoryJpo
{
    @Id
    private String id;

    private String method;
    private String actionType;
    private String url;
    private String ip;
    private String entityId;
    private String jwt;
    private long time;

    public HistoryJpo()
    {

    }

    public HistoryJpo(History history)
    {
        BeanUtils.copyProperties(history, this);
    }

    public void update(History history)
    {
        BeanUtils.copyProperties(history, this);
    }

    public History toDomain()
    {
        History history = new History(id);
        BeanUtils.copyProperties(this, history);
        return history;
    }

    public static List<History> toDomains(List<HistoryJpo> historyJpos)
    {
        return historyJpos.stream()
                .map(jpo -> jpo.toDomain())
                .collect(Collectors.toList());
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getMethod()
    {
        return method;
    }

    public void setMethod(String method)
    {
        this.method = method;
    }

    public String getActionType()
    {
        return actionType;
    }

    public void setActionType(String actionType)
    {
        this.actionType = actionType;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public String getEntityId()
    {
        return entityId;
    }

    public void setEntityId(String entityId)
    {
        this.entityId = entityId;
    }

    public String getJwt()
    {
        return jwt;
    }

    public void setJwt(String jwt)
    {
        this.jwt = jwt;
    }

    public long getTime()
    {
        return time;
    }

    public void setTime(long time)
    {
        this.time = time;
    }
}
