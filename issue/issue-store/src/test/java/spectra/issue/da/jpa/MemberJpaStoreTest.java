package spectra.issue.da.jpa;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import spectra.issue.da.IssueStoreTestApplication;
import spectra.issue.domain.Role;
import spectra.issue.domain.entity.Member;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = IssueStoreTestApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MemberJpaStoreTest
{
    @Autowired
    private MemberJpaStore memberJpaStore;

    private static String PASSWORD = "1234";
    private static String NAME = "테스트";
    private static String EMAIL = "test@spectra.co.kr";
    private static String ROLE_ID = Role.User.name();

    private String memberId;
    private String randomMemberId;

    @Before
    public void setUp() throws Exception
    {
        randomMemberId = randomMemberId();
        memberId = memberJpaStore.create(new Member(randomMemberId, PASSWORD, NAME, EMAIL, ROLE_ID));
    }

    @Test
    public void testCreate() throws Exception
    {
        memberId = memberJpaStore.create(new Member(randomMemberId(), PASSWORD, NAME, EMAIL, ROLE_ID));
        System.out.println("[TEST INFO] meber create => ID : " + memberId);
    }

    @Test(expected = NoSuchElementException.class)
    public void testRetrieve() throws Exception
    {
        Member member = memberJpaStore.retrieve(memberId);
        Assert.assertEquals(NAME, member.getName());
        Assert.assertEquals(EMAIL, member.getEmail());
        System.out.println("[TEST INFO] " + member.toString());

        memberJpaStore.retrieve("None");
    }

    @Test
    public void testRetrieveByMemberId() throws Exception
    {
        Member member = memberJpaStore.retrieveByMemberId(randomMemberId);
        Assert.assertEquals(randomMemberId, member.getMemberId());
        System.out.println("[TEST INFO] " + member.toString());
    }

    @Test
    public void testRetrieveByName() throws Exception
    {
        List<Member> members = memberJpaStore.retrieveByName(NAME);
        Assert.assertNotEquals(0, members.size());
        Assert.assertEquals(NAME, members.get(0).getName());
        System.out.println("[TEST INFO] Issue Size" + members.size());
        System.out.println("[TEST INFO] " + members.get(0).toString());
    }

    @Test
    public void testRetrieveByEmail() throws Exception
    {
        List<Member> members = memberJpaStore.retrieveByEmail(EMAIL);
        Assert.assertNotEquals(0, members.size());
        Assert.assertEquals(EMAIL, members.get(0).getEmail());
        System.out.println("[TEST INFO] Issue Size" + members.size());
        System.out.println("[TEST INFO] " + members.get(0).toString());
    }

    @Test
    public void testRetrieveByRoleId() throws Exception
    {
        List<Member> members = memberJpaStore.retrieveByRoleId(ROLE_ID);
        Assert.assertNotEquals(0, members.size());
        Assert.assertEquals(ROLE_ID, members.get(0).getRoleId());
        System.out.println("[TEST INFO] Issue Size" + members.size());
        System.out.println("[TEST INFO] " + members.get(0).toString());
    }

    @Test
    public void testRetrieveByMemberIdPassword() throws Exception
    {
        Member member = memberJpaStore.retrieveByMemberIdPassword(memberId, PASSWORD);
        Assert.assertEquals(randomMemberId, member.getMemberId());
        System.out.println("[TEST INFO] " + member.toString());
    }

    @Test
    public void testUpdate() throws Exception
    {
        Member member = memberJpaStore.retrieveByMemberId(memberId);
        System.out.println("[TEST INFO] 0. Before update => " + member.toString());

        member.setEmail("abcd@spectra.co.kr");
        memberJpaStore.update(member);
        System.out.println("[TEST INFO] 1. After update => " + member.toString());
    }

    @Test
    public void testDelete() throws Exception
    {
        Member member = memberJpaStore.retrieveByMemberId(memberId);
        memberJpaStore.delete(member);
    }

    private String randomMemberId()
    {
        return "Test-" + UUID.randomUUID().toString().replace("-", "");
    }
}
