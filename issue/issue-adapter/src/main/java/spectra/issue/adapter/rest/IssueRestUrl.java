package spectra.issue.adapter.rest;

import nara.share.restclient.HttpMethod;
import nara.share.restclient.NaraRestUrl;

public enum IssueRestUrl implements NaraRestUrl
{
    ISSUE_S_BUILD                   ("api/s/issues",                                HttpMethod.POST),
    ISSUE_S_FIND                    ("api/s/issues/{issueId}",                      HttpMethod.GET),
    ISSUE_S_MODIFY                  ("api/s/issues/{issueId}",                      HttpMethod.PUT),
    ISSUE_S_REMOVE                  ("api/s/issues/{issueId}",                      HttpMethod.DELETE),

    ISSUE_P_BUILD                   ("api/p/issues",                                HttpMethod.POST),
    ISSUE_P_FIND                    ("api/p/issues/{issueId}",                      HttpMethod.GET),
    ISSUE_P_LIST                    ("api/p/issues",                                HttpMethod.GET),

    PROJECT_S_BUILD                 ("api/s/projects",                              HttpMethod.POST),
    PROJECT_S_FIND                  ("api/s/projects/{projectId}",                  HttpMethod.GET),
    PROJECT_S_MODIFY                ("api/s/projects/{projectId}",                  HttpMethod.PUT),
    PROJECT_S_REMOVE                ("api/s/projects/{projectId}",                  HttpMethod.DELETE),

    PROJECT_P_BUILD                 ("api/p/projects",                              HttpMethod.POST),
    PROJECT_P_FIND                  ("api/p/projects/{projectId}",                  HttpMethod.GET),
    PROJECT_P_LIST                  ("api/p/projects",                              HttpMethod.GET),
    PROJECT_P_MODIFY                ("api/p/projects/{projectId}",                  HttpMethod.PUT),

    COMMENT_S_BUILD                 ("api/s/comments",                              HttpMethod.POST),
    COMMENT_S_FIND                  ("api/s/comments/{commentId}",                  HttpMethod.GET),
    COMMENT_S_MODIFY                ("api/s/comments/{commentId}",                  HttpMethod.PUT),
    COMMENT_S_REMOVE                ("api/s/comments/{commentId}",                  HttpMethod.DELETE),

    COMMENT_P_BUILD                 ("api/p/comments",                              HttpMethod.POST),
    COMMENT_P_FIND                  ("api/p/comments/{commentId}",                  HttpMethod.GET),
    COMMENT_P_MODIFY                ("api/p/comments/{commentId}",                  HttpMethod.PUT),

    MEMBER_S_BUILD                  ("api/s/members",                               HttpMethod.POST),
    MEMBER_S_FIND                   ("api/s/members/{memberId}",                    HttpMethod.GET),
    MEMBER_S_MODIFY                 ("api/s/members/{memberId}",                    HttpMethod.PUT),
    MEMBER_S_REMOVE                 ("api/s/members/{memberId}",                    HttpMethod.DELETE),

    MEMBER_P_BUILD                  ("api/p/members",                               HttpMethod.POST),
    MEMBER_P_FIND                   ("api/p/members/{memberId}",                    HttpMethod.GET),
    MEMBER_P_LIST                   ("api/p/members",                               HttpMethod.GET),
    MEMBER_P_MODIFY                 ("api/p/members/{memberId}",                    HttpMethod.PUT),
    MEMBER_P_EXISTS                 ("api/p/members/{memberId}/exists",             HttpMethod.GET),
    MEMBER_P_LOGIN                  ("api/p/members/login",                         HttpMethod.POST),
    MEMBER_P_LOGOUT                 ("api/p/members/logout",                        HttpMethod.GET)

    ;

    private String restUrl;
    private HttpMethod httpMethod;

    IssueRestUrl(String restUrl, HttpMethod httpMethod)
    {
        this.restUrl = restUrl;
        this.httpMethod = httpMethod;
    }

    @Override
    public String getUrl()
    {
        return this.restUrl;
    }

    @Override
    public HttpMethod getMethod()
    {
        return this.httpMethod;
    }
}
