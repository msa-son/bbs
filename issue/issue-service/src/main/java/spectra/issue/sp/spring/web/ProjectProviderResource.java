package spectra.issue.sp.spring.web;

import nara.share.domain.NameValueList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spectra.issue.domain.entity.Project;
import spectra.issue.domain.logic.ProjectLogic;
import spectra.issue.domain.spec.ProjectProvider;
import spectra.issue.domain.spec.sdo.ProjectCdo;

import java.util.List;

@RestController
@RequestMapping("api/p/projects")
public class ProjectProviderResource implements ProjectProvider
{
    @Autowired
    private ProjectLogic projectLogic;

    @Override
    @GetMapping
    public List<Project> listProject()
    {
        return projectLogic.listProject();
    }

    @Override
    @PostMapping
    public String registerProject(@RequestBody ProjectCdo projectCdo)
    {
        return projectLogic.registerProject(projectCdo);
    }

    @Override
    @GetMapping("{projectId}")
    public Project findProject(@PathVariable String projectId)
    {
        return projectLogic.findProject(projectId);
    }

    @Override
    @PutMapping("{projectId}")
    public void modifyProject(@PathVariable String projectId, @RequestBody NameValueList nameValueList)
    {
        projectLogic.modifyProject(projectId, nameValueList);
    }
}
