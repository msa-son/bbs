package spectra.issue.da.jpa;

import nara.share.exception.store.AlreadyExistsException;
import nara.share.exception.store.NonExistenceException;
import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import spectra.issue.da.jpa.jpo.ProjectJpo;
import spectra.issue.da.jpa.springdata.ProjectRepository;
import spectra.issue.domain.entity.Project;
import spectra.issue.domain.store.ProjectStore;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Repository
public class ProjectJpaStore implements ProjectStore
{
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public String create(Project project)
    {
        String id = project.getId();
        if(projectRepository.exists(id))
        {
            throw new AlreadyExistsException(String.format("Project jpo[ID:%s] already exist", id));
        }
        projectRepository.save(new ProjectJpo(project));
        return id;
    }

    @Override
    public Project retrieve(String id) throws NoSuchElementException
    {
        ProjectJpo projectJpo = projectRepository.findOne(id);
        if(projectJpo == null)
        {
            throw new NoSuchElementException(String.format("No project jpo[ID:%s] to retrieve", id));
        }
        return projectJpo.toDomain();
    }

    @Override
    public List<Project> retrieveByName(String name)
    {
        List<ProjectJpo> projectJpos = projectRepository.findByName(name);
        return ProjectJpo.toDomains(projectJpos);
    }

    @Override
    public List<Project> retrieveByOwner(String owner)
    {
        List<ProjectJpo> projectJpos = projectRepository.findByOwner(owner);
        return ProjectJpo.toDomains(projectJpos);
    }

    @Override
    public List<Project> retrieveList()
    {
        Iterable<ProjectJpo> it = projectRepository.findAll(new Sort(Sort.Direction.DESC, "name"));
        List<ProjectJpo> projectJpos =
                        StreamSupport.stream(it.spliterator(), false).collect(Collectors.toList());
        return ProjectJpo.toDomains(projectJpos);
    }

    @Override
    public void update(Project project)
    {
        String id = project.getId();
        if(!projectRepository.exists(id))
        {
            throw new NonExistenceException(String.format("No project jpo[ID:%s] to update", id));
        }
        ProjectJpo projectJpo = projectRepository.findOne(id);
        projectJpo.update(project);
        projectRepository.save(projectJpo);
    }

    @Override
    public void delete(Project project)
    {
        projectRepository.delete(project.getId());
    }
}