import {
  ISSUE_GET,
  ISSUE_GET_SUCCESS,
  ISSUE_GET_FAILURE,
  ISSUE_LIST_GET,
  ISSUE_LIST_GET_SUCCESS,
  ISSUE_LIST_GET_FAILURE,
  ISSUE_POST,
  ISSUE_POST_SUCCESS,
  ISSUE_POST_FAILURE,
  ISSUE_PUT,
  ISSUE_PUT_SUCCESS,
  ISSUE_PUT_FAILURE
} from './ActionTypes';
import axios from '../common/axios'

import objectUtil from '../common/js/objectUtil';


/* getIssue */
export function getIssueRequest(issueId) {
  return (dispatch) => {
    dispatch(getIssue());

    return axios.get('http://localhost:9999/api/p/issues/' + issueId)
      .then((response) => {
        dispatch(getIssueSuccess(response.data));
      }).catch((error) => {
        dispatch(getIssueFailure());
      });
  };
}

export function getIssue() {
  return {
    type: ISSUE_GET
  };
}

export function getIssueSuccess(data) {
  return {
    type: ISSUE_GET_SUCCESS,
    data
  };
}

export function getIssueFailure() {
  console.log('getIssueFailure');
  return {
    type: ISSUE_GET_FAILURE
  };
}

/* postIssue */
export function postIssueRequest(data) {
  console.log(JSON.stringify(data));
  return (dispatch) => {
    dispatch(postIssue());

    return axios.post('http://localhost:9999/api/s/issues', objectUtil.toNameValues(data, ['projectId', 'title', 'questionContent', 'creator']))
      .then((response) => {
        dispatch(postIssueSuccess(response.data));
      }).catch((error) => {
        dispatch(postIssueFailure());
      });
  };
}

export function postIssue() {
  return {
    type: ISSUE_POST
  };
}

export function postIssueSuccess(data) {
  return {
    type: ISSUE_POST_SUCCESS,
    data
  };
}

export function postIssueFailure() {
  console.log('postIssueFailure');
  return {
    type: ISSUE_POST_FAILURE
  };
}

/* putIssue */
export function putIssueRequest(data) {
  console.log(JSON.stringify(data));
  return (dispatch) => {
    dispatch(putIssue());

    return axios.put('http://localhost:9999/api/s/issues/' + data.id, objectUtil.toNameValues(data, ['assignee', 'resolvedTime', 'questionContent', 'answerContent', 'status']))
      .then((response) => {
        dispatch(putIssueSuccess(response.data));
      }).catch((error) => {
        dispatch(putIssueFailure());
      });
  };
}

export function putIssue() {
  return {
    type: ISSUE_PUT
  };
}

export function putIssueSuccess(data) {
  return {
    type: ISSUE_PUT_SUCCESS,
    data
  };
}

export function putIssueFailure() {
  console.log('putIssueFailure');
  return {
    type: ISSUE_PUT_FAILURE
  };
}

/* getIssueList */
export function getIssueListRequest(request, keyword = '', start = 0) {
  console.log(request);
  return (dispatch) => {
    dispatch(getIssueList());

    let params = 'start=' + (start * 10)
    if (request.projectIds.length > 0) {
      params += '&projectId=' + request.projectIds;
    }
    if (request.memberIds.length > 0) {
      params += '&memberId=' + request.memberIds;
    }
    if (request.status.length > 0) {
      params += '&status=' + request.status;
    }
    if (keyword) {
      params += '&keyword=' + keyword;
    }

    console.log('http://localhost:9999/api/p/issues?' + params);
    return axios.get('http://localhost:9999/api/p/issues?' + params)
      .then((response) => {
        dispatch(getIssueListSuccess(response.data));
      }).catch((error) => {
        dispatch(getIssueListFailure());
      });
  };
}

export function getIssueList() {
  console.log('getIssueList');
  return {
    type: ISSUE_LIST_GET
  };
}

export function getIssueListSuccess(data) {
  console.log('getIssueListSuccess');
  return {
    type: ISSUE_LIST_GET_SUCCESS,
    data
  };
}

export function getIssueListFailure() {
  console.log('getIssueListFailure');
  return {
    type: ISSUE_LIST_GET_FAILURE
  };
}
