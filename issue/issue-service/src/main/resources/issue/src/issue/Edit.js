import React from 'react';
import {
  connect
} from 'react-redux';
import AppBar from 'material-ui/AppBar';
import DatePicker from 'material-ui/DatePicker';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';

import MdEditor from '../components/MdEditor';
import StatusSelect from './edit/StatusSelect';
import MemberSelect from './edit/MemberSelect';
import Activity from './Activity';
import {
  getIssueRequest,
  putIssueRequest
} from '../actions/issue';

class Edit extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: {}
    }
  }

  componentDidMount() {
    this.props.getIssueRequest(this.props.match.params.id);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return JSON.stringify(nextState.data) !== JSON.stringify(this.state.data);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      data: nextProps.data
    });
  }

  componentWillUpdate(nextProps, nextState){
    if(Object.keys(this.state.data).length > 0 && Object.keys(nextState.data).length > 0){
        this.props.putIssueRequest(nextState.data);
    }
  }

  getProjectId(projectId) {
    return this.props.projectList.filter(project => project.id === projectId).map(project => project.name);
  }

  selectedAssignee = (value) => {
    this.setState({
      data: {
        ...this.state.data,
        assignee: value
      }
    });
  }

  selectedStatus = (value) => {
    this.setState({
      data: {
        ...this.state.data,
        status: value
      }
    });
  }

  handleResolvedDate = (event, date) => {
    this.setState({
      data: {
        ...this.state.data,
        resolvedTime: date.getTime()
      }
    });
  }

  handleQuestionContent = (editor, data, value) => {
    this.setState({
      data: {
        ...this.state.data,
        questionContent: value
      }
    });
  }

  handleAnswerContent = (editor, data, value) => {
    this.setState({
      data: {
        ...this.state.data,
        answerContent: value
      }
    });
  }

  render = () => {
    let title = this.getProjectId(this.state.data.projectId) + ' > ' + this.state.data.id + ' : ' + this.state.data.title;

    return (
      <div style={{ padding: '20px' }}>
        <AppBar
          iconElementLeft={<IconButton/>}
          iconElementRight={<StatusSelect selectedData={this.state.data.status} selectedStatus={this.selectedStatus}/>}
          title={title}
        />
        <div>
          <MemberSelect
            disabled={true}
            label={'Creator'}
            selectedData={this.state.data.creator}
          />
        </div>
        <div>
          <MemberSelect
            label={'Assignee'}
            selectedData={this.state.data.assignee}
            selectedMember={this.selectedAssignee}
          />
        </div>
        <div>
          <DatePicker
            disabled={true}
            floatingLabelText="Create Date"
            value={this.state.data.createdTime ? new Date(this.state.data.createdTime) : null}
          />
        </div>
        <div>
          <DatePicker
          onChange={this.handleResolvedDate}
          floatingLabelText="Resolved Date"
          autoOk={true}
          value={this.state.data.resolvedTime ? new Date(this.state.data.resolvedTime) : null}
          />
        </div>
        <div>
          <Subheader style={{ padding: '0px' }}>Question Content</Subheader>
          <MdEditor content={this.state.data.questionContent} onChange={this.handleQuestionContent}/>
        </div>
        <div>
          <Subheader style={{ padding: '0px' }}>Answer Content</Subheader>
          <MdEditor content={this.state.data.answerContent} onChange={this.handleAnswerContent}/>
        </div>
        <div>
          <Activity/>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentUser: state.member.status.currentUser,
    data: state.issue.get.data,
    projectList: state.project.list_get.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getIssueRequest: (issueId) => {
      dispatch(getIssueRequest(issueId));
    },
    putIssueRequest: (data) => {
      dispatch(putIssueRequest(data));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Edit);
