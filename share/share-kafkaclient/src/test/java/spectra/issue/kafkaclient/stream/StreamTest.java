package spectra.issue.kafkaclient.stream;

import org.junit.Before;
import org.junit.Test;

import spectra.issue.kafkaclient.simple.Producer;

public class StreamTest
{
    @Before
    public void setup()
    {
        CountStream stream = new CountStream();
        stream.start();
        StreamConsumer consumer = new StreamConsumer();
        consumer.start();
        try
        {
            Thread.sleep(10000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void test()
    {
        Producer producer = new Producer();
        producer.send("TextLinesTopic", "all streams lead to kafka");
        try
        {
            Thread.sleep(60000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
