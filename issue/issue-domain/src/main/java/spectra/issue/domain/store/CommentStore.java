package spectra.issue.domain.store;

import spectra.issue.domain.entity.Comment;

import java.util.List;
import java.util.NoSuchElementException;

public interface CommentStore
{
    String create(Comment comment);
    Comment retrieve(String id) throws NoSuchElementException;
    Comment retrieveByIdAndCreator(String id, String creator) throws NoSuchElementException;
    List<Comment> retrieveByIssueId(String issueId);
    void update(Comment comment);
    void delete(Comment comment);
}
