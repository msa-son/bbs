import React from 'react';
import { connect } from 'react-redux';

import { Authentication } from '../components';
import { loginRequest } from '../actions/authentication';
import Notify from '../common/Notify';

class Login extends React.Component {

    constructor(props) {
        super(props);

        console.log(props);
        this.handleLogin = this.handleLogin.bind(this);
        this.state = {
          open: false,
          message: 'Default'
        }
    }

    handleLogin(id, pw) {
        return this.props.loginRequest(id, pw).then(
            () => {
                if(this.props.status === "SUCCESS") {
                    let loginData = {
                        isLoggedIn: true,
                        username: id
                    };

                    document.cookie = 'key=' + btoa(JSON.stringify(loginData));

                    this.setState({
                      open: true,
                      message: 'Welcome ' + id + '!'
                    })
                    this.props.history.push('/bbs/main/issues');
                    return true;
                } else {
                    this.setState({
                      open: true,
                      message: 'Incorrect username or password!'
                    })
                    return false;
                }
            }
        );
    }

    handleNotifyClose = () => {
      this.setState({
        open: false,
      });
    };

    render() {
        return (
            <div>
              <Notify {...this.state} onClose={this.handleNotifyClose}/>
              <Authentication mode={true} onLogin={this.handleLogin}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        status: state.authentication.login.status
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loginRequest: (id, pw) => {
            return dispatch(loginRequest(id,pw));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
