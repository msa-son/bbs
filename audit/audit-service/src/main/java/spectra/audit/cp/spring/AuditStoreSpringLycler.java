package spectra.audit.cp.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import spectra.audit.domain.store.AuditStoreLycler;
import spectra.audit.domain.store.HistoryStore;

@Component
public class AuditStoreSpringLycler implements AuditStoreLycler
{
    @Autowired
    @Qualifier("historyJpaStore")
    private HistoryStore historyStore;

    @Override
    public HistoryStore requestHistoryStore()
    {
        return historyStore;
    }
}
