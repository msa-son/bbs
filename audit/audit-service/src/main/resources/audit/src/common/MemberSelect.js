import React from 'react';
import { connect } from 'react-redux';

import Select from '../components/Select';
import { selectedMember, getMemberListRequest } from '../actions/member';


class MemberSelect extends React.Component {
  componentDidMount() {
    this.props.getMemberListRequest();
  }

  onSelect = (event, index, value) => {
    this.props.selectedMember(value);
  };

  render() {
    return (
      <Select label={'Member'} onItemClick={this.onSelect} {...this.props} />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    status: state.member.list_get.status,
    dataList: state.member.list_get.data
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getMemberListRequest: () => {
      dispatch(getMemberListRequest());
    },
    selectedMember: (value) => {
      dispatch(selectedMember(value));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MemberSelect);
