# msa-son bbs

전체 빌드
- bbs 최상위 디렉토리로 이동
- mvn install 실행

서버 실행
- issue/issue-boot 디렉토리로 이동
- mvn spring-boot:run 실행

접속
- http://localhost:9999/bbs
- http://localhost:9999/swagger
