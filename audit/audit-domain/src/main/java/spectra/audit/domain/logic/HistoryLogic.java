package spectra.audit.domain.logic;

import spectra.audit.domain.entity.History;
import spectra.audit.domain.spec.HistoryProvider;
import spectra.audit.domain.spec.HistoryService;
import spectra.audit.domain.spec.sdo.HistoryCdo;
import spectra.audit.domain.store.AuditStoreLycler;
import spectra.audit.domain.store.HistoryStore;

import java.util.List;

public class HistoryLogic implements HistoryService, HistoryProvider
{
    private HistoryStore historyStore;

    public HistoryLogic(AuditStoreLycler storeLycler)
    {
        this.historyStore = storeLycler.requestHistoryStore();
    }

    @Override
    public List<History> listHistory()
    {
        List<History> historyList = historyStore.retrieveList();
        return historyList;
    }

    @Override
    public String registerHistory(HistoryCdo historyCdo)
    {
        History history = new History(historyCdo.getMethod(), historyCdo.getActionType(), historyCdo.getUrl(), historyCdo.getIp(), historyCdo.getEntityId(), historyCdo.getJwt());
        historyStore.create(history);

        return history.getId();
    }

    @Override
    public History findHistory(String historyId)
    {
        return historyStore.retrieve(historyId);
    }

    @Override
    public void removeHistory(String historyId)
    {
        History history = historyStore.retrieve(historyId);
        historyStore.delete(history);
    }
}
