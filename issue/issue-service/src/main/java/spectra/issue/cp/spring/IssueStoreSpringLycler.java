package spectra.issue.cp.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import spectra.issue.domain.store.*;

@Component
public class IssueStoreSpringLycler implements IssueStoreLycler
{
    @Autowired
    @Qualifier("issueJpaStore")
    private IssueStore issueStore;

    @Autowired
    @Qualifier("projectJpaStore")
    private ProjectStore projectStore;

    @Autowired
    @Qualifier("commentJpaStore")
    private CommentStore commentStore;

    @Autowired
    @Qualifier("memberJpaStore")
    private MemberStore memberStore;

    @Override
    public IssueStore requestIssueStore()
    {
        return issueStore;
    }

    @Override
    public ProjectStore requestProjectStore()
    {
        return projectStore;
    }

    @Override
    public MemberStore requestMemberStore()
    {
        return memberStore;
    }

    @Override
    public CommentStore requestCommentStore()
    {
        return commentStore;
    }
}
