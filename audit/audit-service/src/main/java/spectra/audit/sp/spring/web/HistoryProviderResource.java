package spectra.audit.sp.spring.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spectra.audit.domain.entity.History;
import spectra.audit.domain.logic.HistoryLogic;
import spectra.audit.domain.spec.HistoryProvider;
import spectra.audit.domain.spec.sdo.HistoryCdo;

import java.util.List;

@RestController
@RequestMapping("api/p/history")
public class HistoryProviderResource implements HistoryProvider
{
    @Autowired
    private HistoryLogic historyLogic;

    @Override
    @PostMapping
    public String registerHistory(@RequestBody HistoryCdo historyCdo)
    {
        return historyLogic.registerHistory(historyCdo);
    }

    @Override
    @GetMapping("{historyId}")
    public History findHistory(@PathVariable String historyId)
    {
        return historyLogic.findHistory(historyId);
    }

    @Override
    @GetMapping
    public List<History> listHistory()
    {
        return historyLogic.listHistory();
    }
}
