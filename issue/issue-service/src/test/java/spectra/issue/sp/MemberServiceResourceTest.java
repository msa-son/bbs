package spectra.issue.sp;

import nara.share.domain.NameValueList;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spectra.issue.AbstractIssueApplicationTests;
import spectra.issue.domain.IssueStatus;
import spectra.issue.domain.Role;
import spectra.issue.domain.entity.Issue;
import spectra.issue.domain.entity.Member;

public class MemberServiceResourceTest extends AbstractIssueApplicationTests
{
    Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void test()
    {
        // 조회
        Member member = memberServiceRestAdapter().findMember(getSampleMemberId());
        Assert.assertNotNull(member);

        logger.debug("{}", member);

        NameValueList nameValues = new NameValueList();
        nameValues.add("name", "한경만2");
        nameValues.add("roleId", Role.User.name());
        nameValues.add("email", "rudaks94@gmail.com");

        // 수정
        memberServiceRestAdapter().modifyMember(getSampleMemberId(), nameValues);

        // 삭제
        memberServiceRestAdapter().removeMember(getSampleMemberId());
    }
}
