package spectra.audit;

import nara.share.restclient.NaraRestClient;
import nara.share.restclient.springweb.SpringWebRestClient;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import spectra.audit.adapter.rest.HistoryProviderRestAdapter;
import spectra.audit.adapter.rest.HistoryServiceRestAdapter;
import spectra.audit.domain.spec.sdo.HistoryCdo;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AuditTestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public abstract class AbstractAuditApplicationTests
{
    private HistoryCdo historyCdo;

    private NaraRestClient restClient;

    @LocalServerPort
    private int port;

    @Bean
    public NaraRestClient createRestClient()
    {
        if(restClient == null)
        {
            restClient = new SpringWebRestClient("http://localhost:" + port);
        }
        return restClient;
    }

    @Bean
    public HistoryServiceRestAdapter hisotryServiceRestAdapter()
    {
        return new HistoryServiceRestAdapter(createRestClient());
    }

    @Bean
    public HistoryProviderRestAdapter historyProviderRestAdapter()
    {
        return new HistoryProviderRestAdapter(createRestClient());
    }
}
