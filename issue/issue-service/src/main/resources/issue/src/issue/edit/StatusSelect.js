import React from 'react';

import Select from '../../components/Select';


class StatusSelect extends React.Component {
  onSelect = (event, index, value) => {
    this.props.selectedStatus(value);
  };

  render() {
    return (
      <Select label={'Status'} onItemClick={this.onSelect} {...this.props} />
    );
  }
}

StatusSelect.defaultProps = {
  dataList: [{
    id: '100',
    name: "Open"
  }, {
    id: '200',
    name: "In Progress"
  }, {
    id: '300',
    name: "Closed"
  }]
};

export default StatusSelect;
