package spectra.issue.sp;

import nara.share.domain.NameValueList;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spectra.issue.AbstractIssueApplicationTests;
import spectra.issue.domain.Role;
import spectra.issue.domain.entity.Comment;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.spec.sdo.CommentCdo;
import spectra.issue.domain.spec.sdo.MemberCdo;

public class CommentServiceResourceTest extends AbstractIssueApplicationTests
{
    Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void test()
    {
        // 조회
        Comment comment = commentServiceRestAdapter().findComment(getSampleCommentId());

        logger.debug("{}", comment);

        NameValueList nameValues = new NameValueList();
        nameValues.add("content", "내용수정");

        // 수정
        commentServiceRestAdapter().modifyComment(getSampleCommentId(), nameValues);

        // 삭제
        commentServiceRestAdapter().removeComment(getSampleCommentId());
    }

    @Test
    public void testByMember()
    {
        String user1 = "test01";
        String user2 = "test02";
        String password1 = "1111";
        String password2 = "1111";

        memberServiceRestAdapter().removeMember(user1);
        memberServiceRestAdapter().removeMember(user2);

        MemberCdo sampleMemberCdo1 = new MemberCdo(user1, password1, user1, user1 +"@spectra.co.kr", Role.User.getCode());
        user1 = memberServiceRestAdapter().registerMember(sampleMemberCdo1);

        MemberCdo sampleMemberCdo2 = new MemberCdo(user2, password2, user2, user2 + "@spectra.co.kr", Role.User.getCode());
        user2 = memberServiceRestAdapter().registerMember(sampleMemberCdo2);

        // user1 로그인
        Member member1 = memberProviderRestAdapter().login(user1, password1);


        // 등록
        CommentCdo commentCdo = new CommentCdo("test-issue-id", member1.getMemberId(), "############# 새 답글입니다. 1 #############");
        String commentId = commentServiceRestAdapter().registerComment(commentCdo);

        // 조회
        Comment comment = commentServiceRestAdapter().findComment(commentId);

        // user2 로그인
        Member member2 = memberProviderRestAdapter().login(user2, password2);

        // 수정
        NameValueList nameValues = new NameValueList();
        nameValues.add("content", "######################### 내용수정 2 #########################");
        commentServiceRestAdapter().modifyComment(commentId, nameValues);
    }
}
