package spectra.issue.da.mongodb.springdata;

import org.springframework.data.mongodb.repository.MongoRepository;
import spectra.issue.da.mongodb.document.CommentDoc;

import java.util.List;

public interface CommentMongodbRepository extends MongoRepository<CommentDoc, String>
{
    CommentDoc findByIdAndCreator(String id, String creator);
    List<CommentDoc> findByIssueId(String issueid);
}