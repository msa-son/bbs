package spectra.issue.da.mongodb;

import nara.share.exception.store.AlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import spectra.issue.da.mongodb.document.MemberDoc;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.store.MemberStore;

import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class MemberMongodbStore implements MemberStore
{
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public String create(Member member)
    {
        String id = member.getMemberId();
        Query query = new Query(Criteria.where("memberId").is(id));
        if (mongoTemplate.exists(query, MemberDoc.class)) {
            throw new AlreadyExistsException(String.format("Comment document[ID:%s already exit", id));
        }

        mongoTemplate.save(new MemberDoc(member));
        return id;
    }

    @Override
    public Member retrieve(String id) throws NoSuchElementException
    {
        MemberDoc memberDoc = mongoTemplate.findById(id, MemberDoc.class);
        if(memberDoc == null)
        {
            throw new NoSuchElementException(String.format("No Member document[ID:%s] to retrieve", id));
        }
        return memberDoc.toDomain();
    }

    @Override
    public Member retrieveByMemberId(String memberId)
    {
        Query query = new Query(Criteria.where("memberId").is(memberId));
        MemberDoc memberDoc = mongoTemplate.findOne(query, MemberDoc.class);
        if(memberDoc == null)
        {
            throw new NoSuchElementException(String.format("No Member document[ID:%s] to retrieve", memberId));
        }
        return memberDoc.toDomain();
    }

    @Override
    public Member retrieveByMemberIdPassword(String memberId, String password)
    {
        //여기
        Query query = new Query(Criteria.where("memberId").is(memberId));
        MemberDoc memberDoc = mongoTemplate.findOne(query, MemberDoc.class);
        if(memberDoc == null)
        {
            throw new NoSuchElementException(String.format("No Member document[ID:%s] to retrieve", memberId));
        }
        return memberDoc.toDomain();
    }

    public List<Member> retrieveByName(String name)

    {
        Query query = new Query(Criteria.where("name").is(name));
        List<MemberDoc> memberDocs = mongoTemplate.find(query, MemberDoc.class);

        return MemberDoc.toDomains(memberDocs);
    }

    @Override
    public List<Member> retrieveByEmail(String email)
    {
        Query query = new Query(Criteria.where("email").is(email));
        List<MemberDoc> memberDocs = mongoTemplate.find(query, MemberDoc.class);

        return MemberDoc.toDomains(memberDocs);
    }

    @Override
    public List<Member> retrieveByRoleId(String roleId)
    {
        Query query = new Query(Criteria.where("roleId").is(roleId));
        List<MemberDoc> memberDocs = mongoTemplate.find(query, MemberDoc.class);

        return MemberDoc.toDomains(memberDocs);
    }

    @Override
    public List<Member> retreiveList()
    {
        //sorting
        Query query = new Query();
        query.with(new Sort(Sort.Direction.DESC,"createdTime"));
        List<MemberDoc> memberDocs =  mongoTemplate.find(query, MemberDoc.class);
        return MemberDoc.toDomains(memberDocs);
    }

    @Override
    public void update(Member member)
    {
        String id = member.getId();
        MemberDoc memberDoc = mongoTemplate.findById(id , MemberDoc.class);
        if (memberDoc == null) {
            throw new AlreadyExistsException(String.format("Comment documenr[ID:%s  exit", id));
        }

        memberDoc.update(member);
        mongoTemplate.save(memberDoc);
    }

    @Override
    public void delete(Member member)
    {
        mongoTemplate.remove(new MemberDoc(member));
    }

    @Override
    public boolean exists(String memberId)
    {
        Query query = new Query(Criteria.where("memberId").is(memberId));
        return mongoTemplate.exists(query, MemberDoc.class);
    }
}