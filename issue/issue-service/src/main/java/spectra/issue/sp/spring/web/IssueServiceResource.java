package spectra.issue.sp.spring.web;

import nara.share.domain.NameValueList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spectra.issue.domain.entity.Issue;
import spectra.issue.domain.logic.IssueLogic;
import spectra.issue.domain.spec.IssueService;
import spectra.issue.domain.spec.sdo.IssueCdo;
import spectra.issue.sp.spring.web.util.ServletUtil;

@RestController
@RequestMapping("api/s/issues")
public class IssueServiceResource implements IssueService
{
    @Autowired
    private IssueLogic issueLogic;

    @Override
    @PostMapping
    public String registerIssue(@RequestBody IssueCdo issueCdo)
    {
        return issueLogic.registerIssue(issueCdo);
    }

    @Override
    @GetMapping("{issueId}")
    public Issue findIssue(@PathVariable String issueId)
    {
        return issueLogic.findIssue(issueId);
    }

    @Override
    @PutMapping("{issueId}")
    public void modifyIssue(@PathVariable String issueId, @RequestBody NameValueList nameValueList)
    {
        String memberId = (String) ServletUtil.getRequestAttributes("memberId");
        String roleId = (String) ServletUtil.getRequestAttributes("roleId");

        nameValueList.add("memberId", memberId);
        nameValueList.add("roleId", roleId);

        issueLogic.modifyIssue(issueId, nameValueList);
    }

    @Override
    @DeleteMapping("{issueId}")
    public void removeIssue(@PathVariable String issueId)
    {
        issueLogic.removeIssue(issueId);
    }
}
