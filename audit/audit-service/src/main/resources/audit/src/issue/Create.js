import React from 'react';
import TextField from 'material-ui/TextField';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import ProjectSelect from '../common/ProjectSelect';
import MdEditor from '../components/MdEditor';


class Create extends React.Component {
  handleClose = () => {
    this.props.onClose();
  };

  handleSubmit = () => {
    console.log('submit');
    this.props.onClose();
  };

  render() {
    let actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleClose}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onClick={this.handleSubmit}
      />,
    ];

    return (
      <Dialog
        title="Issue Create"
        actions={actions}
        modal={false}
        open={this.props.show}
        onRequestClose={this.handleClose}
        autoScrollBodyContent={true}
      >
        <div>
          <ProjectSelect/>
        </div>
        <div>
          <TextField hintText="Title"/>
        </div>
        <div>
          Question Content
          <MdEditor/>
        </div>
      </Dialog>
    );
  }
}

export default Create;
