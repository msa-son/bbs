import React from 'react';
import { connect } from 'react-redux';

import Select from '../../components/Select';


class MemberSelect extends React.Component {
  onSelect = (event, index, value) => {
    this.props.selectedMember(value);
  };

  render() {
    return (
      <Select label={'Member'} onItemClick={this.onSelect} {...this.props} />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    dataList: state.member.list_get.data
  };
};

export default connect(mapStateToProps)(MemberSelect);
