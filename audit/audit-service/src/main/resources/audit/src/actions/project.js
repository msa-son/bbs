import {
    PROJECT_SELECTED,
    PROJECT_LIST_GET,
    PROJECT_LIST_GET_SUCCESS,
    PROJECT_LIST_GET_FAILURE
} from './ActionTypes';
import axios from 'axios';


/* selected */
export function selectedProject(ids) {
  console.log('selectedProject : '+ids);
    return {
        type: PROJECT_SELECTED,
        ids
    };
}

/* getProjectList */
export function getProjectListRequest() {
    return (dispatch) => {
        dispatch(getProjectList());

        return axios.get('http://localhost:9999/api/p/projects')
        .then((response) => {
            dispatch(getProjectListSuccess(response.data));
        }).catch((error) => {
            dispatch(getProjectListFailure());
        });
    };
}

export function getProjectList() {
    return {
        type: PROJECT_LIST_GET
    };
}

export function getProjectListSuccess(data) {
    return {
        type: PROJECT_LIST_GET_SUCCESS,
        data
    };
}

export function getProjectListFailure() {
    console.log('getProjectListFailure');
    return {
        type: PROJECT_LIST_GET_FAILURE
    };
}
