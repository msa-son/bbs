DROP TABLE IF EXISTS t_history;

CREATE TABLE t_history
(
  id                VARCHAR(36) PRIMARY KEY,
  method            VARCHAR(10) NOT NULL,
  action_type       VARCHAR(10),
  url               VARCHAR(1000) NOT NULL,
  ip                VARCHAR(100) NOT NULL,
  entity_id         VARCHAR(36),
  jwt               VARCHAR(1000),
  time             DECIMAL(13, 0)
);
