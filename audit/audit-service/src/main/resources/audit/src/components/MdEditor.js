import React from 'react'
import { Controlled as CodeMirror } from 'react-codemirror2';

import 'codemirror/lib/codemirror.css';
import 'codemirror/mode/markdown/markdown';

export default class MdEditor extends React.Component {
  constructor(props)
  {
    super(props);

    this.state = {
      content: props.content ? props.content : "# Heading\n\nSome **bold** and _italic_ text",
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({content: nextProps.content});
  }

  render() {
		let options = {
			// lineNumbers: true,
			mode: 'markdown',
      readOnly: this.props.readOnly,
      viewportMargin: Infinity
		};
    return (
      <div style={{ border: '1px solid lightgrey'}}>
        <CodeMirror
          value={this.state.content}
          options={options}
          onBeforeChange={(editor, data, value) => {
            this.setState({content:value});
          }}
          onChange={(editor, data, value) => {
          }}
        />
      </div>
    );
  }
}
