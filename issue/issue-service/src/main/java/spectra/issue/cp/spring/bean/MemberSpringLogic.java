package spectra.issue.cp.spring.bean;

import nara.share.domain.NameValueList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.logic.MemberLogic;
import spectra.issue.domain.spec.sdo.MemberCdo;
import spectra.issue.domain.store.IssueStoreLycler;

import javax.transaction.Transactional;

@Service
@Transactional
public class MemberSpringLogic extends MemberLogic
{
    @Autowired
    public MemberSpringLogic(IssueStoreLycler storeLycler)
    {
        super(storeLycler);
    }

    @Override
    public String registerMember(MemberCdo memberCdo)
    {
        return super.registerMember(memberCdo);
    }

    @Override
    public Member findMember(String memberId)
    {
        return super.findMember(memberId);
    }

    @Override
    public void modifyMember(String memberId, NameValueList nameValueList)
    {
        super.modifyMember(memberId, nameValueList);
    }

    @Override
    public void removeMember(String memberId)
    {
        super.removeMember(memberId);
    }

    @Override
    public boolean existsMemberId(String memberId)
    {
        return super.existsMemberId(memberId);
    }

    @Override
    public Member login(String memberId, String password)
    {
        return super.login(memberId, password);
    }

    @Override
    public void logout(String memberId)
    {
        super.logout(memberId);
    }
}
