package spectra.issue.kafkaserver;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import javax.annotation.PostConstruct;

import kafka.server.KafkaConfig;
import kafka.server.KafkaServerStartable;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Service;

@Service
@DependsOn("embeddedZookeeper")
public class EmbeddedKafka
{
    //
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Properties properties;

    public EmbeddedKafka()
    {
        //
        try
        {
            properties = PropertiesLoaderUtils.loadProperties(new ClassPathResource("server.properties"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @PostConstruct
    public void start()
    {
        //
        cleanDataDir();

        KafkaConfig kafkaConfig = new KafkaConfig(properties);
        logger.info("start " + this.getClass().getSimpleName() + " " + kafkaConfig.brokerId() + ":" + kafkaConfig.hostName() + ":" + kafkaConfig.port() + ", "
                + kafkaConfig.logDirs());
        KafkaServerStartable kafkaServerStartable = new KafkaServerStartable(kafkaConfig);
        kafkaServerStartable.startup();
    }

    private void cleanDataDir()
    {
        //
        String logDirs = properties.getProperty("log.dirs");
        try
        {
            FileUtils.deleteDirectory(new File(logDirs));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
