import React from 'react';
import {
  withRouter
} from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {
  Toolbar,
  ToolbarGroup,
  ToolbarSeparator
} from 'material-ui/Toolbar';

import ProjectSelect from './toolbar/ProjectSelect';
import StatusSelect from './toolbar/StatusSelect';
import MemberSelect from './toolbar/MemberSelect';
import ToolBarMenu from './toolbar/ToolBarMenu';
import Create from '../issue/Create'


class ToolBar extends React.Component {
  constructor(props) {
    super(props);

    this.props.history.listen((location, action) => {
      const params = location.pathname.split("/");
      const page = params[4];
      const keyword = params[5];
      this.setState({
        keyword: keyword,
        page: isNaN(page) ? 1:Number(page),
      })
    });

    this.state = {
      open: false,
      keyword: props.match.params.keyword,
      page: isNaN(props.match.params.page) ? 1:Number(props.match.params.page),
    };
  }

  handleOnChange = (event, newValue) => {
    this.setState({
      keyword: newValue
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return JSON.stringify(nextState) !== JSON.stringify(this.state);
  }

  componentWillUpdate(nextProps, nextState){
    this.movePage(nextState);
  }

  handleSearch = () => {
    this.movePage(this.state);
  };

  movePage(state){
    this.props.history.push('/bbs/main/issues/' + state.page + (state.keyword ? ('/' + state.keyword) : ''));
  }

  handleOpen = () => {
    this.setState({
      open: true
    });
  };

  handleClose = () => {
    this.setState({
      open: false
    });
  };

  render() {
    return (
      <div>
        <Toolbar>
          <ToolbarGroup firstChild={true}>
            <ProjectSelect multiple={true}/>
            <StatusSelect multiple={true}/>
            <MemberSelect multiple={true}/>
            <TextField hintText="Search" defaultValue={this.state.keyword} onChange={this.handleOnChange}/>
            <RaisedButton label="Search" onClick={this.handleSearch}/>
            <ToolbarSeparator />
          </ToolbarGroup>
          <ToolbarGroup>
            <RaisedButton label="Create" primary={true} onClick={this.handleOpen}/>
            <ToolbarSeparator />
            <ToolBarMenu/>
          </ToolbarGroup>
        </Toolbar>
        <Create show={this.state.open}
          onClose={this.handleClose}/>
      </div>
    );
  }
}

export default withRouter(ToolBar);
