import * as types from '../actions/ActionTypes';
import update from 'react-addons-update';

const initialState = {
    selected: [],
};

export default function status(state, action) {
    if(typeof state === "undefined") {
        state = initialState;
    }

    switch(action.type) {
        /* selected */
        case types.STATUS_SELECTED:
            return update(state, {
                selected: {
                    $set: action.ids
                }
            });

        default:
            return state;
    }
}
