package spectra.gateway.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessorAdapter;
import org.springframework.cloud.netflix.zuul.web.ZuulHandlerMapping;
import org.springframework.context.annotation.Configuration;
import spectra.gateway.interceptor.ZuulInterceptor;

@Configuration
//@RequiredArgsConstructor
public class ZuulHandlerBeanPostProcessor extends InstantiationAwareBeanPostProcessorAdapter
{
    @Autowired
    private ZuulInterceptor zuulInterceptor;

    @Override
    public boolean postProcessAfterInstantiation(final Object bean, final String beanName) throws BeansException
    {
        if (bean instanceof ZuulHandlerMapping)
        {
            ZuulHandlerMapping zuulHandlerMapping = (ZuulHandlerMapping) bean;
            zuulHandlerMapping.setInterceptors(zuulInterceptor);
        }

        return super.postProcessAfterInstantiation(bean, beanName);
    }

}