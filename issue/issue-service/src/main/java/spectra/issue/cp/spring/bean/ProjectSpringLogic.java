package spectra.issue.cp.spring.bean;

import nara.share.domain.NameValueList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spectra.issue.domain.entity.Project;
import spectra.issue.domain.logic.ProjectLogic;
import spectra.issue.domain.spec.sdo.ProjectCdo;
import spectra.issue.domain.store.IssueStoreLycler;

import javax.transaction.Transactional;

@Service
@Transactional
public class ProjectSpringLogic extends ProjectLogic
{
    @Autowired
    public ProjectSpringLogic(IssueStoreLycler storeLycler)
    {
        super(storeLycler);
    }

    @Override
    public String registerProject(ProjectCdo projectCdo)
    {
        return super.registerProject(projectCdo);
    }

    @Override
    public Project findProject(String projectId)
    {
        return super.findProject(projectId);
    }

    @Override
    public void modifyProject(String projectId, NameValueList nameValueList)
    {
        super.modifyProject(projectId, nameValueList);
    }

    @Override
    public void removeProject(String projectId)
    {
        super.removeProject(projectId);
    }
}
