package spectra.issue.da.mongodb;

import nara.share.exception.store.AlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import spectra.issue.da.mongodb.document.CommentDoc;
import spectra.issue.domain.entity.Comment;
import spectra.issue.domain.store.CommentStore;

import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class CommentMongodbStore implements CommentStore
{
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public String create(Comment comment)
    {
        String id = comment.getId();

        Query query = new Query(Criteria.where("id").is(id));
        if (mongoTemplate.exists(query, CommentDoc.class)) {
            throw new AlreadyExistsException(String.format("Comment document[ID:%s already exit", id));
        }

        mongoTemplate.save(new CommentDoc(comment));
        return id;
    }

    @Override
    public Comment retrieve(String id) throws NoSuchElementException
    {
        CommentDoc commentDoc = mongoTemplate.findById(id, CommentDoc.class);

        return commentDoc.toDomain();
    }

    @Override
    public Comment retrieveByIdAndCreator(String id, String creator) throws NoSuchElementException
    {
        Query query = new Query(Criteria.where("id").is(id)
                        .and("creator").is(creator));

        CommentDoc commentDoc = mongoTemplate.findOne(query, CommentDoc.class);
        if (commentDoc != null) {
            throw new AlreadyExistsException(String.format("Comment jpo[ID:%s already exit", id));
        }

        return commentDoc.toDomain();
    }

    @Override
    public List<Comment> retrieveByIssueId(String issueId)
    {
        Query query = new Query(Criteria.where("issueId").is(issueId));
        List<CommentDoc> commentDocs = mongoTemplate.find(query, CommentDoc.class);

        return CommentDoc.toDomains(commentDocs);
    }

    @Override
    public void update(Comment comment)
    {
        String id = comment.getId();
        Query query = new Query(Criteria.where("id").is(id));
        CommentDoc commentDoc = mongoTemplate.findOne(query, CommentDoc.class);
        if (commentDoc == null) {
            throw new AlreadyExistsException(String.format("Comment documenr[ID:%s  exit", id));
        }

        commentDoc.update(comment);
        mongoTemplate.save(commentDoc);
    }

    @Override
    public void delete(Comment comment)
    {
        mongoTemplate.remove(new CommentDoc(comment));
    }
}