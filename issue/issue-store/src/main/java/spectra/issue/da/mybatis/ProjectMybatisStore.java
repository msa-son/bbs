package spectra.issue.da.mybatis;

import nara.share.exception.store.AlreadyExistsException;
import nara.share.exception.store.NonExistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spectra.issue.da.mybatis.mapper.ProjectMapper;
import spectra.issue.domain.entity.Project;
import spectra.issue.domain.store.ProjectStore;

import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class ProjectMybatisStore implements ProjectStore
{
    @Autowired
    private ProjectMapper projectMapper;

    @Override
    public String create(Project project)
    {
        String id = project.getId();
        if(projectMapper.exists(id))
        {
            throw new AlreadyExistsException(String.format("Project [ID:%s] already exist", id));
        }
        projectMapper.insert(project);
        return id;
    }

    @Override
    public Project retrieve(String id) throws NoSuchElementException
    {
        Project project = projectMapper.findOne(id);
        if(project == null)
        {
            throw new NoSuchElementException(String.format("No project [ID:%s] to retrieve", id));
        }
        return project;
    }

    @Override
    public List<Project> retrieveByName(String name)
    {
        return projectMapper.findByName(name);
    }

    @Override
    public List<Project> retrieveByOwner(String owner)
    {
        return projectMapper.findByOwner(owner);
    }

    @Override
    public List<Project> retrieveList()
    {
        return null;
    }

    @Override
    public void update(Project project)
    {
        String id = project.getId();
        if(!projectMapper.exists(id))
        {
            throw new NonExistenceException(String.format("No project [ID:%s] to update", id));
        }
        projectMapper.update(project);
    }

    @Override
    public void delete(Project project)
    {
        projectMapper.delete(project.getId());
    }

}