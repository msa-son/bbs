package spectra.issue.da.mongodb.document;


import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;
import spectra.issue.domain.entity.Comment;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.util.List;
import java.util.stream.Collectors;

@Document(collection = "t_comment")
public class CommentDoc
{
    @Id
    private String id;

    private String issueId;
    private String creator;
    @Lob
    private String content;
    private long createdTime;
    private long updatedTime;

    public CommentDoc()
    {

    }

    public CommentDoc(Comment comment)
    {
        BeanUtils.copyProperties(comment, this);
    }

    public void update(Comment comment)
    {
        BeanUtils.copyProperties(comment, this);
    }

    public Comment toDomain()
    {
        Comment comment = new Comment(id);
        BeanUtils.copyProperties(this, comment);
        return comment;
    }

    public static List<Comment> toDomains(List<CommentDoc> commentDocs)
    {
        return commentDocs.stream()
                .map(doc -> doc.toDomain())
                .collect(Collectors.toList());
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getIssueId()
    {
        return issueId;
    }

    public void setIssueId(String issueId)
    {
        this.issueId = issueId;
    }

    public String getCreator()
    {
        return creator;
    }

    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    public long getCreatedTime()
    {
        return createdTime;
    }

    public void setCreatedTime(long createdTime)
    {
        this.createdTime = createdTime;
    }

    public long getUpdatedTime()
    {
        return updatedTime;
    }

    public void setUpdatedTime(long updatedTime)
    {
        this.updatedTime = updatedTime;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }
}
