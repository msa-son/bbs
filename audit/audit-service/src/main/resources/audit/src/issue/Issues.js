import React from 'react';
import {
  connect
} from 'react-redux';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import Pagination from 'material-ui-pagination';

import {
  getIssueListRequest
} from '../actions/issue';
import {
  selectedProject
} from '../actions/project';


class Issues extends React.Component {

  constructor(props) {
    super(props);

    this.initialize(this.props.projectIds, this.props.match.params.page);
    this.state = {
      issuesData: [{
          id: "875bd2d8-dd2b-4e41-b57a-40d8da812187",
          status: "RESOLVED",
          title: "카테고리별 현황 통계 배치 CPU 과점유 현상1",
          creator: "엄윤식",
          created: "2017.11.27",
          assignee: "이경환",
          resolved: "2017.11.30"
        },
        {
          id: "876c19ce-d852-4414-ad33-ec6a1dd88250",
          status: "RESOLVED",
          title: "카테고리별 현황 통계 배치 CPU 과점유 현상2",
          creator: "엄윤식",
          created: "2017.11.27",
          comments: "2"
        },
        {
          id: "fcf3de6a-0e21-4d9b-b1b8-4ac3f886299c",
          status: "RESOLVED",
          title: "카테고리별 현황 통계 배치 CPU 과점유 현상3",
          creator: "엄윤식",
          created: "2017.11.27",
          assignee: "이경환",
          resolved: "2017.11.30",
          comments: "3"
        }
      ],
      total: 20,
      display: 7,
      number: Number(this.props.match.params.page),
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      number: Number(nextProps.match.params.page)
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return JSON.stringify(nextProps) !== JSON.stringify(this.props) || JSON.stringify(nextState) !== JSON.stringify(this.state);
  }

  componentWillUpdate(nextProps, nextState){
    this.initialize(nextProps.projectIds, nextState.number);
  }

  initialize(projectIds, page) {
    if(page <= 0)
    {
      this.props.history.push('/bbs/main/issues/1');
    }
    this.props.getIssueListRequest(projectIds, page-1);
  }


  handleRowSelection = (selectedRow) => {
    this.props.history.push('../issue/' + this.state.issuesData[selectedRow].id);
  };

  handlePageSelection(number) {
    this.props.history.push('/bbs/main/issues/' + number);
  }

  drawTableItem() {
    return (
      this.props.dataList.map((issueInfo, key) => {
        return (
          <TableRow key={issueInfo.id}>
            <TableRowColumn style={{textAlign: 'center'}}>{issueInfo.id}</TableRowColumn>
            <TableRowColumn style={{textAlign: 'center'}}>{issueInfo.status}</TableRowColumn>
            <TableRowColumn>{issueInfo.title}</TableRowColumn>
            <TableRowColumn style={{textAlign: 'center'}}>{issueInfo.creator}</TableRowColumn>
            <TableRowColumn style={{textAlign: 'center'}}>{(new Date(issueInfo.createdTime)).toISOString().slice(0,10)}</TableRowColumn>
            <TableRowColumn style={{textAlign: 'center'}}>{issueInfo.assignee}</TableRowColumn>
            <TableRowColumn style={{textAlign: 'center'}}>{issueInfo.resolvedTime ? new Date(issueInfo.resolvedTime).toISOString().slice(0,10) : null}</TableRowColumn>
            <TableRowColumn style={{textAlign: 'center'}}>{issueInfo.comments}</TableRowColumn>
          </TableRow>
        );
      })
    );
  }

  render() {
    return (
      <div>
        <div style={{ padding: '20px' }}>
          <Table onRowSelection={this.handleRowSelection}>
            <TableHeader
              displaySelectAll={false}
              adjustForCheckbox={false}
            >
              <TableRow>
                <TableHeaderColumn style={{textAlign: 'center'}}>ID</TableHeaderColumn>
                <TableHeaderColumn style={{textAlign: 'center'}}>Status</TableHeaderColumn>
                <TableHeaderColumn style={{textAlign: 'center'}}>Title</TableHeaderColumn>
                <TableHeaderColumn style={{textAlign: 'center'}}>Creator</TableHeaderColumn>
                <TableHeaderColumn style={{textAlign: 'center'}}>Created</TableHeaderColumn>
                <TableHeaderColumn style={{textAlign: 'center'}}>Assignee</TableHeaderColumn>
                <TableHeaderColumn style={{textAlign: 'center'}}>Resolved</TableHeaderColumn>
                <TableHeaderColumn style={{textAlign: 'center'}}>Comments</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {this.drawTableItem()}
            </TableBody>
          </Table>
        </div>

        <div style = {{ margin: 'auto', width: '327px', padding: '10px' }}>
          <Pagination
            total = { this.state.total }
            current = { this.state.number }
            display = { this.state.display }
            onChange = { number => this.handlePageSelection(number) }
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    dataList: state.issue.list_get.data,
    projectIds: state.project.selected,
    status: state.status.selected,
    memberIds: state.member.selected,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getIssueListRequest: (projectIds, start) => {
      dispatch(getIssueListRequest(projectIds, start));
    },
    selectedProject: (ids) => {
      dispatch(selectedProject(ids));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Issues);
