package spectra.issue.domain.logic;

import nara.share.domain.NameValueList;
import org.apache.commons.lang3.StringUtils;
import spectra.issue.domain.Role;
import spectra.issue.domain.entity.Issue;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.spec.IssueProvider;
import spectra.issue.domain.spec.IssueService;
import spectra.issue.domain.spec.sdo.IssueCdo;
import spectra.issue.domain.store.IssueStore;
import spectra.issue.domain.store.IssueStoreLycler;

import java.util.Arrays;
import java.util.List;

public class IssueLogic implements IssueService, IssueProvider
{
    private IssueStore issueStore;

    public IssueLogic(IssueStoreLycler storeLycler)
    {
        this.issueStore = storeLycler.requestIssueStore();
    }

    @Override
    public String registerIssue(IssueCdo issueCdo)
    {
        Issue issue = new Issue(issueCdo.getProjectId(), issueCdo.getTitle(), issueCdo.getQuestionContent(), issueCdo.getCreator());

        issueStore.create(issue);

        return issue.getId();
    }

    @Override
    public Issue findIssue(String issueId)
    {
        return issueStore.retrieve(issueId);
    }

    @Override
    public void modifyIssue(String issueId, NameValueList nameValueList)
    {
        Member member = new Member();
        member.setValues(nameValueList);

        Issue issue = issueStore.retrieve(issueId);

        if(Role.User.getCode().equals(member.getRoleId()))
        {
            String creator = issue.getCreator();
            String memberId =member.getMemberId();

            if(!creator.equals(memberId))
            {
                return;
            }
        }

        issue.setValues(nameValueList);
        issueStore.update(issue);
    }

    @Override
    public void removeIssue(String issueId)
    {
        Issue issue = issueStore.retrieve(issueId);

        issueStore.delete(issue);
    }

    @Override
    public List<Issue> listIssue(String projectId, String status, String memberId, String keyword, int startNo)
    {
        /*String [] projectIds = StringUtils.split(projectId, ",");
        return issueStore.retrieveList(projectIds, startNo);*/
        List<String> projectIds = null;
        if (projectId != null)
            projectIds = Arrays.asList(StringUtils.split(projectId, ","));

        List<String> statuses = null;
        if (status != null)
            statuses = Arrays.asList(StringUtils.split(status, ","));

        List<String> memberIds = null;
        if (memberId != null)
            memberIds = Arrays.asList(StringUtils.split(memberId, ","));

        return issueStore.retrieveList(projectIds, statuses, memberIds, keyword, startNo);
    }
}
