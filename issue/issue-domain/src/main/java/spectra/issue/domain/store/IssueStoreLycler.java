package spectra.issue.domain.store;

public interface IssueStoreLycler
{
    IssueStore requestIssueStore();
    ProjectStore requestProjectStore();
    MemberStore requestMemberStore();
    CommentStore requestCommentStore();
}