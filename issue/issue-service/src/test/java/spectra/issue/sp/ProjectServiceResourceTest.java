package spectra.issue.sp;

import nara.share.domain.NameValueList;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spectra.issue.AbstractIssueApplicationTests;
import spectra.issue.domain.Role;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.entity.Project;
import spectra.issue.domain.granule.MemberList;

public class ProjectServiceResourceTest extends AbstractIssueApplicationTests
{
    Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void test()
    {
        // 조회
        Project project = projectServiceRestAdapter().findProject(getSampleProjectId());

        logger.debug("{}", project);

        NameValueList nameValues = new NameValueList();
        nameValues.add("name", "프로젝트2");
        nameValues.add("owner", "rudaks");


        MemberList memberList = new MemberList();
        memberList.add(new Member("kmhan", "1234", "한경만", "kmhan@spectra.co.kr", Role.Manager.name()));
        nameValues.add("members", memberList.toJson());


        // 수정
        projectServiceRestAdapter().modifyProject(getSampleProjectId(), nameValues);

        // 삭제
        projectServiceRestAdapter().removeProject(getSampleProjectId());
    }
}
