import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { logoutRequest } from '../actions/authentication';


class Logout extends React.Component {
  componentWillReceiveProps(nextProps){
    console.log("componentWillReceiveProps: " + JSON.stringify(nextProps));
  }

  render(){
    logoutRequest(this.props.currentUser);
    return (
      <div>
          <Redirect to="bbs/system/logout"/>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    currentUser: state.authentication.status.currentUser
  };
}

export default connect(mapStateToProps)(Logout);
