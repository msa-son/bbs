package spectra.issue.da.mybatis;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import spectra.issue.da.IssueStoreTestApplication;
import spectra.issue.domain.Role;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.entity.Project;
import spectra.issue.domain.granule.MemberList;

import java.util.List;
import java.util.NoSuchElementException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = IssueStoreTestApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ProjectMybatisStoreTest
{
    @Autowired
    private ProjectMybatisStore projectMybatisStore;

    private static final String PROJECT_NAME = "testProject1";
    private static final String PROJECT_OWNER = "tester";

    private String projectId;

    @Before
    public void setUp() throws Exception
    {
        projectId = projectMybatisStore.create(new Project(PROJECT_NAME, PROJECT_OWNER));
    }

    @Test
    public void testCreate() throws Exception
    {
        MemberList memberList = new MemberList();
        memberList.add(new Member("test", "1234", "테스터", "test@spectra.co.kr", Role.Manager.name()));
        Project project = new Project(PROJECT_NAME, PROJECT_OWNER);
        project.setMembers(memberList);
        projectId = projectMybatisStore.create(project);
        System.out.println("[TEST INFO] project create => ID : " + projectId);
    }

    @Test(expected = NoSuchElementException.class)
    public void testRetrieve() throws Exception
    {
        Project project = projectMybatisStore.retrieve(projectId);
        Assert.assertEquals(projectId, project.getId());
        System.out.println("[TEST INFO] " + project.toString());

        projectMybatisStore.retrieve("None");
    }

    @Test
    public void testRetrieveByName() throws Exception
    {
        List<Project> projects = projectMybatisStore.retrieveByName(PROJECT_NAME);
        Assert.assertNotEquals(0, projects.size());
        Assert.assertEquals(PROJECT_NAME, projects.get(0).getName());
        System.out.println("[TEST INFO] Projects Size" + projects.size());
        System.out.println("[TEST INFO] " + projects.get(0).toString());
    }

    @Test
    public void testRetrieveByOwner() throws Exception
    {
        List<Project> projects = projectMybatisStore.retrieveByOwner(PROJECT_OWNER);
        Assert.assertNotEquals(0, projects.size());
        Assert.assertEquals(PROJECT_OWNER, projects.get(0).getOwner());
        System.out.println("[TEST INFO] Projects Size" + projects.size());
        System.out.println("[TEST INFO] " + projects.get(0).toString());
    }

    @Test
    public void testUpdate() throws Exception
    {
        Project project = projectMybatisStore.retrieve(projectId);
        System.out.println("[TEST INFO] 0. Before update => " + project.toString());

        project.setName("testProject2");
        projectMybatisStore.update(project);
        System.out.println("[TEST INFO] 1. After update => " + project.toString());
    }

    @Test
    public void testDelete() throws Exception
    {
        Project project = projectMybatisStore.retrieve(projectId);
        projectMybatisStore.delete(project);
    }
}
