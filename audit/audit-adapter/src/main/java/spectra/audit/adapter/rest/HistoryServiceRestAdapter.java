package spectra.audit.adapter.rest;

import nara.share.restclient.NaraRestClient;
import nara.share.restclient.RequestBuilder;
import spectra.audit.domain.entity.History;
import spectra.audit.domain.spec.HistoryService;
import spectra.audit.domain.spec.sdo.HistoryCdo;

import java.util.List;

public class HistoryServiceRestAdapter implements HistoryService
{
    private NaraRestClient naraRestClient;

    public HistoryServiceRestAdapter(NaraRestClient naraRestClient)
    {
        this.naraRestClient = naraRestClient;
    }

    @Override
    public String registerHistory(HistoryCdo historyCdo)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(HistoryRestUrl.HISTORY_S_BUILD)
                                        .setRequestBody(historyCdo)
                                        .setResponseType(String.class)
        );
    }

    @Override
    public History findHistory(String historyId)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(HistoryRestUrl.HISTORY_S_FIND)
                                        .addPathParam("historyId", historyId)
                                        .setResponseType(History.class)
        );
    }

    @Override
    public List<History> listHistory()
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(HistoryRestUrl.HISTORY_S_LIST)
                                        .setResponseType(List.class)
        );
    }

    @Override
    public void removeHistory(String historyId)
    {
        naraRestClient.sendAndRecieve(
                        RequestBuilder.create(HistoryRestUrl.HISTORY_S_REMOVE)
                                        .addPathParam("historyId", historyId)
        );
    }
}
