package spectra.audit.domain.spec.sdo;

import nara.share.domain.Entity;
import nara.share.util.json.JsonUtil;
import spectra.audit.domain.entity.History;

import java.io.Serializable;

public class HistoryCdo implements Serializable
{
    private String method;
    private String actionType;
    private String url;
    private String ip;
    private String entityId;
    private String jwt;
    private long time;

    public HistoryCdo()
    {
    }

    public HistoryCdo(String method, String actionType, String url, String ip, String entityId, String jwt)
    {
        this.method = method;
        this.actionType = actionType;
        this.url = url;
        this.ip = ip;
        this.entityId = entityId;
        this.jwt = jwt;
    }

    public static HistoryCdo getSample()
    {
        History history = History.getSample();

        HistoryCdo sample = new HistoryCdo(history.getMethod(), history.getActionType(), history.getUrl(), history.getIp(), history.getEntityId(), history.getJwt());

        return sample;
    }

    public String getMethod()
    {
        return method;
    }

    public void setMethod(String method)
    {
        this.method = method;
    }

    public String getActionType()
    {
        return actionType;
    }

    public void setActionType(String actionType)
    {
        this.actionType = actionType;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public String getEntityId()
    {
        return entityId;
    }

    public void setEntityId(String entityId)
    {
        this.entityId = entityId;
    }

    public String getJwt()
    {
        return jwt;
    }

    public void setJwt(String jwt)
    {
        this.jwt = jwt;
    }

    public long getTime()
    {
        return time;
    }

    public void setTime(long time)
    {
        this.time = time;
    }

    public static void main(String[] args)
    {
        System.out.println(getSample());
    }
}
