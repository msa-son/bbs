package spectra.issue.sp.spring.web.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import nara.share.crypt.CryptoUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import spectra.issue.domain.entity.Member;
import spectra.issue.sp.spring.web.exception.InvalidTokenException;
import spectra.issue.sp.spring.web.exception.TokenExpiredException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
public class JwtValidator
{
    @Value("${jwt.headername}")
    private String jwtHeaderName;

    @Value("${jwt.key}")
    private String jwtKey;

    public Member validate(HttpServletRequest request) throws InvalidTokenException, TokenExpiredException
    {
        String token = request.getHeader(jwtHeaderName);
        tokenNullCheck(token);

        String decryptedToken = decryptToken(token);
        tokenNullCheck(decryptedToken);

        return getMemberByDecryptedToken(decryptedToken);
    }

    private String decryptToken(String token) throws TokenExpiredException, InvalidTokenException
    {
        String decryptedToken = null;
        try
        {
            decryptedToken = CryptoUtil.parseJWT(token, jwtKey);
        }
        catch (ExpiredJwtException eje)
        {
            throw new TokenExpiredException("JWT key is expired.");
        }
        catch (MalformedJwtException me)
        {
            throw new InvalidTokenException("JWT key is invalid.");
        }

        return decryptedToken;
    }

    private void tokenNullCheck(String token) throws InvalidTokenException
    {
        if(token == null)
        {
            throw new InvalidTokenException("JWT key is required.");
        }
    }

    private Member getMemberByDecryptedToken(String decryptedToken)
    {
        Member member = null;

        try
        {
            ObjectMapper obj = new ObjectMapper();
            member = obj.readValue(decryptedToken, Member.class);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return member;
    }
}
