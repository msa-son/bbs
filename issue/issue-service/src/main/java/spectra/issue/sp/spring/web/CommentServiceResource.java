package spectra.issue.sp.spring.web;

import nara.share.domain.NameValueList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spectra.issue.domain.entity.Comment;
import spectra.issue.domain.logic.CommentLogic;
import spectra.issue.domain.spec.CommentService;
import spectra.issue.domain.spec.sdo.CommentCdo;
import spectra.issue.sp.spring.web.util.ServletUtil;

@RestController
@RequestMapping("api/s/comments")
public class CommentServiceResource implements CommentService
{
    @Autowired
    private CommentLogic commentLogic;

    @Override
    @PostMapping
    public String registerComment(@RequestBody CommentCdo commentCdo)
    {
        return commentLogic.registerComment(commentCdo);
    }

    @Override
    @GetMapping("{commentId}")
    public Comment findComment(@PathVariable String commentId)
    {
        return commentLogic.findComment(commentId);
    }

    @Override
    @PutMapping("{commentId}")
    public void modifyComment(@PathVariable String commentId, @RequestBody NameValueList nameValueList)
    {
        String memberId = (String) ServletUtil.getRequestAttributes("memberId");
        String roleId = (String) ServletUtil.getRequestAttributes("roleId");

        nameValueList.add("memberId", memberId);
        nameValueList.add("roleId", roleId);

        commentLogic.modifyComment(commentId, nameValueList);
    }

    @Override
    @DeleteMapping("{commentId}")
    public void removeComment(@PathVariable String commentId)
    {
        commentLogic.removeComment(commentId);
    }
}
