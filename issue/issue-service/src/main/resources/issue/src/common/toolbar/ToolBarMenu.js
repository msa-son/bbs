import React from 'react';
import { withRouter } from 'react-router-dom';
import Divider from 'material-ui/Divider';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';


class ToolBarMenu extends React.Component {

  onSelect = (event, child) => {
    this.props.history.push(child.props.path);
  };

  render() {
    return (
      <IconMenu
        iconButtonElement={
          <IconButton><MoreVertIcon /></IconButton>
        }
        targetOrigin={{horizontal: 'right', vertical: 'top'}}
        anchorOrigin={{horizontal: 'right', vertical: 'top'}}
        onItemClick={this.onSelect}
      >
        <MenuItem primaryText="Project" path="/bbs/main/projects" />
        <MenuItem primaryText="Member" path="/bbs/main/members" />
        <Divider />
        <MenuItem primaryText="Sign out" path="/bbs/system/logout" />
      </IconMenu>
    );
  }
}

export default withRouter(ToolBarMenu);
