import React from 'react';
import {
  connect
} from 'react-redux';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import Pagination from 'material-ui-pagination';

import {
  getIssueListRequest
} from '../actions/issue';
import {
  selectedProject
} from '../actions/project';


class Issues extends React.Component {

  constructor(props) {
    super(props);

    this.initialize(this.props, this.props.match.params.keyword, Number(this.props.match.params.page));
    this.state = {
      total: 99,
      display: 10,
      number: Number(this.props.match.params.page),
      keyword: this.props.match.params.keyword,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      number: Number(nextProps.match.params.page),
      keyword: nextProps.match.params.keyword,
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return JSON.stringify(nextProps) !== JSON.stringify(this.props) || JSON.stringify(nextState) !== JSON.stringify(this.state);
  }

  componentWillUpdate(nextProps, nextState){
    this.initialize(nextProps, nextProps.match.params.keyword, Number(nextProps.match.params.page));
  }

  initialize(props, keyword, page) {
    if(page <= 0)
    {
      this.movePage(1);
    }
    this.props.getIssueListRequest(props, keyword, page - 1);
  }


  handleRowSelection = (selectedRow) => {
    this.props.history.push('../issue/' + this.props.dataList[selectedRow].id);
  };

  handlePageSelection(number) {
    this.movePage(number);
  }

  movePage(number) {
    this.props.history.push('/bbs/main/issues/' + number + (this.state.keyword ? ('/'+this.state.keyword) : ''));
  }

  statusString(status){
      switch(status) {
        case '100':
          return 'Open';
        case '200':
          return 'In Progress';
        case '300':
          return 'Closed';
        default:
          return 'unknown';
      }
  }

  drawTableItem() {
    return (
      this.props.dataList.map((issueInfo, key) => {
        return (
          <TableRow key={issueInfo.id}>
            <TableRowColumn style={{textAlign: 'center', width: '5rem'}}>{issueInfo.id}</TableRowColumn>
            <TableRowColumn style={{textAlign: 'center'}}>{this.statusString(issueInfo.status)}</TableRowColumn>
            <TableRowColumn style={{width: '30rem'}}>{issueInfo.title}</TableRowColumn>
            <TableRowColumn style={{textAlign: 'center'}}>{issueInfo.creator}</TableRowColumn>
            <TableRowColumn style={{textAlign: 'center'}}>{(new Date(issueInfo.createdTime)).toISOString().slice(0,10)}</TableRowColumn>
            <TableRowColumn style={{textAlign: 'center'}}>{issueInfo.assignee}</TableRowColumn>
            <TableRowColumn style={{textAlign: 'center'}}>{issueInfo.resolvedTime ? new Date(issueInfo.resolvedTime).toISOString().slice(0,10) : null}</TableRowColumn>
            <TableRowColumn style={{textAlign: 'center'}}>{issueInfo.comments}</TableRowColumn>
          </TableRow>
        );
      })
    );
  }

  render() {
    return (
      <div>
        <div style={{ padding: '20px' }}>
          <Table onRowSelection={this.handleRowSelection}>
            <TableHeader
              displaySelectAll={false}
              adjustForCheckbox={false}
            >
              <TableRow>
                <TableHeaderColumn style={{textAlign: 'center', width: '5rem'}}>ID</TableHeaderColumn>
                <TableHeaderColumn style={{textAlign: 'center'}}>Status</TableHeaderColumn>
                <TableHeaderColumn style={{textAlign: 'center', width: '30rem'}}>Title</TableHeaderColumn>
                <TableHeaderColumn style={{textAlign: 'center'}}>Creator</TableHeaderColumn>
                <TableHeaderColumn style={{textAlign: 'center'}}>Created</TableHeaderColumn>
                <TableHeaderColumn style={{textAlign: 'center'}}>Assignee</TableHeaderColumn>
                <TableHeaderColumn style={{textAlign: 'center'}}>Resolved</TableHeaderColumn>
                <TableHeaderColumn style={{textAlign: 'center'}}>Comments</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false} showRowHover={true}>
              {this.drawTableItem()}
            </TableBody>
          </Table>
        </div>

        <div>
          <Pagination
            styleRoot = {{margin: 'auto', width: '550px', padding: '10px'}}
            total = { this.state.total }
            current = { this.state.number }
            display = { this.state.display }
            onChange = { number => this.handlePageSelection(number) }
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    dataList: state.issue.list_get.data,
    projectIds: state.project.selected,
    status: state.status.selected,
    memberIds: state.member.selected,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getIssueListRequest: (request, keyword, start) => {
      dispatch(getIssueListRequest(request, keyword, start));
    },
    selectedProject: (ids) => {
      dispatch(selectedProject(ids));
    }
  }
}

Issues.defaultProps = {
  projectIds: '',
  status: '',
  memberIds: '',
  status: {}
};

export default connect(mapStateToProps, mapDispatchToProps)(Issues);
