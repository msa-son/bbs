package spectra.issue.sp.spring.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import spectra.issue.domain.entity.Issue;
import spectra.issue.domain.logic.IssueLogic;
import spectra.issue.domain.spec.IssueProvider;
import spectra.issue.domain.spec.sdo.IssueCdo;

import java.util.List;

@RestController
@RequestMapping("api/p/issues")
public class IssueProviderResource implements IssueProvider
{
    @Autowired
    private IssueLogic issueLogic;

    @Override
    @GetMapping
    public List<Issue> listIssue(@RequestParam(value = "projectId", required = false) String projectId,
                    @RequestParam(value = "status", required = false) String status,
                    @RequestParam(value = "memberId", required = false) String memberId,
                    @RequestParam(value = "keyword", required = false) String searchKeyword,
                    @RequestParam(value = "start", required = true) int startNo)
    {
        return issueLogic.listIssue(projectId, status, memberId, searchKeyword, startNo);
    }

    @Override
    @PostMapping
    public String registerIssue(@RequestBody IssueCdo issueCdo)
    {
        return issueLogic.registerIssue(issueCdo);
    }

    @Override
    @GetMapping("{issueId}")
    public Issue findIssue(@PathVariable String issueId)
    {
        return issueLogic.findIssue(issueId);
    }
}
