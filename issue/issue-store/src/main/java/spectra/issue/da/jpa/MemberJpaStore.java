package spectra.issue.da.jpa;

import nara.share.exception.store.AlreadyExistsException;
import nara.share.exception.store.NonExistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import spectra.issue.da.jpa.jpo.MemberJpo;
import spectra.issue.da.jpa.springdata.MemberRepository;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.store.MemberStore;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Repository
public class MemberJpaStore implements MemberStore
{
    @Autowired
    private MemberRepository memberRepository;

    @Override
    public String create(Member member)
    {
        String memberId = member.getMemberId();
        if (exists(memberId))
        {
            throw new AlreadyExistsException(String.format("member jpo[memberId:%s] already exist", memberId));
        }

        memberRepository.save(new MemberJpo(member));
        return memberId;
    }

    @Override
    public Member retrieve(String id) throws NoSuchElementException
    {
        MemberJpo memberJpo = memberRepository.findOne(id);
        if(memberJpo == null)
        {
            throw new NoSuchElementException(String.format("No member jpo[ID:%s] to retrieve", id));
        }
        return memberJpo.toDomain();
    }

    @Override
    public Member retrieveByMemberId(String memberId)
    {
        MemberJpo memberJpo = memberRepository.findByMemberId(memberId);
        if(memberJpo == null)
        {
            throw new NoSuchElementException(String.format("No member jpo[MEMBERID:%s] to retrieve", memberId));
        }
        return memberJpo.toDomain();
    }

    @Override
    public Member retrieveByMemberIdPassword(String memberId, String password)
    {
        MemberJpo memberJpo = memberRepository.findByMemberIdAndPassword(memberId, password);
        if(memberJpo == null)
        {
            throw new NoSuchElementException(String.format("No member jpo[MEMBERID:%s] to retrieveByMemberIdPassword", memberId, password));
        }
        return memberJpo.toDomain();
    }

    public List<Member> retrieveByName(String name)

    {
        List<MemberJpo> memberJpos = memberRepository.findByName(name);
        return MemberJpo.toDomains(memberJpos);
    }

    @Override
    public List<Member> retrieveByEmail(String email)
    {
        List<MemberJpo> memberJpos = memberRepository.findByEmail(email);
        return MemberJpo.toDomains(memberJpos);
    }

    @Override
    public List<Member> retrieveByRoleId(String roleId)
    {
        List<MemberJpo> memberJpos = memberRepository.findByRoleId(roleId);
        return MemberJpo.toDomains(memberJpos);
    }

    @Override
    public List<Member> retreiveList()
    {
        Iterable<MemberJpo> it = memberRepository.findAll(new Sort(Sort.Direction.DESC, "createdTime"));
        List<MemberJpo> memberJpos =
                        StreamSupport.stream(it.spliterator(), false).collect(Collectors.toList());
        return MemberJpo.toDomains(memberJpos);
    }

    @Override
    public void update(Member member)
    {
        String id = member.getId();
        if(!memberRepository.exists(id))
        {
            throw new NonExistenceException(String.format("No member jpo[ID:%s] to update", id));
        }
        MemberJpo memberJpo = memberRepository.findOne(id);
        memberJpo.update(member);
        memberRepository.save(memberJpo);
    }

    @Override
    public void delete(Member member)
    {
        memberRepository.delete(member.getId());
    }

    @Override
    public boolean exists(String memberId)
    {
        return memberRepository.findByMemberId(memberId) != null;
    }
}