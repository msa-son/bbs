package spectra.issue.sp;

import nara.share.domain.NameValueList;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spectra.issue.AbstractIssueApplicationTests;
import spectra.issue.domain.IssueStatus;
import spectra.issue.domain.entity.Issue;
import spectra.issue.domain.spec.sdo.IssueCdo;

public class IssueServiceResourceTest extends AbstractIssueApplicationTests
{
    Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void test()
    {
        // 조회
        Issue issue = issueServiceRestAdapter().findIssue(getSampleIssueId());
        //Assert.assertNotNull(issue);

        logger.debug("{}", issue);

        NameValueList nameValues = new NameValueList();
        nameValues.add("assignee", "ekpark");
        nameValues.add("status", IssueStatus.Closed.name());

        // 수정
        issueServiceRestAdapter().modifyIssue(getSampleIssueId(), nameValues);

        // 삭제
        issueServiceRestAdapter().removeIssue(getSampleIssueId());
    }
}
