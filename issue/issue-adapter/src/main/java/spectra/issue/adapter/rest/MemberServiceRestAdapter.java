package spectra.issue.adapter.rest;

import nara.share.domain.NameValueList;
import nara.share.restclient.NaraRestClient;
import nara.share.restclient.RequestBuilder;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.spec.MemberProvider;
import spectra.issue.domain.spec.MemberService;
import spectra.issue.domain.spec.sdo.MemberCdo;

public class MemberServiceRestAdapter implements MemberService
{
    private NaraRestClient naraRestClient;

    public MemberServiceRestAdapter(NaraRestClient naraRestClient)
    {
        this.naraRestClient = naraRestClient;
    }

    @Override
    public String registerMember(MemberCdo memberCdo)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.MEMBER_S_BUILD)
                                        .setRequestBody(memberCdo)
                                        .setResponseType(String.class)
        );
    }

    @Override
    public Member findMember(String memberId)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.MEMBER_S_FIND)
                            .addPathParam("memberId", memberId)
                            .setResponseType(Member.class)
        );
    }

    @Override
    public void modifyMember(String memberId, NameValueList nameValueList)
    {
        naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.MEMBER_S_MODIFY)
                            .addPathParam("memberId", memberId)
                            .setRequestBody(nameValueList)
        );
    }

    @Override
    public void removeMember(String memberId)
    {
        naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.MEMBER_S_REMOVE)
                            .addPathParam("memberId", memberId)
        );
    }
}
