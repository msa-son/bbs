package spectra.issue.adapter.rest;

import nara.share.domain.NameValueList;
import nara.share.restclient.NaraRestClient;
import nara.share.restclient.RequestBuilder;
import spectra.issue.domain.entity.Comment;
import spectra.issue.domain.spec.CommentProvider;
import spectra.issue.domain.spec.CommentService;
import spectra.issue.domain.spec.sdo.CommentCdo;

public class CommentServiceRestAdapter implements CommentService
{
    private NaraRestClient naraRestClient;

    public CommentServiceRestAdapter(NaraRestClient naraRestClient)
    {
        this.naraRestClient = naraRestClient;
    }

    @Override
    public String registerComment(CommentCdo commentCdo)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.COMMENT_S_BUILD)
                                        .setRequestBody(commentCdo)
                                        .setResponseType(String.class)
        );
    }

    @Override
    public Comment findComment(String commentId)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.COMMENT_S_FIND)
                                        .addPathParam("commentId", commentId)
                                        .setResponseType(Comment.class)
        );
    }

    @Override
    public void modifyComment(String commentId, NameValueList nameValueList)
    {
        naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.COMMENT_S_MODIFY)
                            .addPathParam("commentId", commentId)
                            .setRequestBody(nameValueList)
        );
    }

    @Override
    public void removeComment(String commentId)
    {
        naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.COMMENT_S_REMOVE)
                            .addPathParam("commentId", commentId)
        );
    }
}
