package spectra.issue.cp.spring.bean;

import nara.share.domain.NameValueList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spectra.issue.domain.entity.Comment;
import spectra.issue.domain.logic.CommentLogic;
import spectra.issue.domain.spec.sdo.CommentCdo;
import spectra.issue.domain.store.IssueStoreLycler;

import javax.transaction.Transactional;

@Service
@Transactional
public class CommentSpringLogic extends CommentLogic
{
    @Autowired
    public CommentSpringLogic(IssueStoreLycler storeLycler)
    {
        super(storeLycler);
    }

    @Override
    public String registerComment(CommentCdo commentCdo)
    {
        return super.registerComment(commentCdo);
    }

    @Override
    public Comment findComment(String commentId)
    {
        return super.findComment(commentId);
    }

    @Override
    public void modifyComment(String commentId, NameValueList nameValueList)
    {
        super.modifyComment(commentId, nameValueList);
    }

    @Override
    public void removeComment(String commentId)
    {
        super.removeComment(commentId);
    }
}
