import React from 'react';
import { connect } from 'react-redux';

import Select from '../../components/Select';
import { selectedStatus } from '../../actions/status';


class StatusSelect extends React.Component {
    onSelect = (event, index, value) => {
      this.props.selectedStatus(value);
    };

    render(){
        return(
          <Select label={'Status'} onItemClick={this.onSelect} {...this.props} />
        );
    }
}

StatusSelect.defaultProps = {
    dataList: [{id:'100', name:"Open"}, {id:'200', name:"In Progress"}, {id:'300', name:"Closed"}]
};


const mapDispatchToProps = (dispatch) => {
  return {
    selectedStatus: (value) => {
      dispatch(selectedStatus(value));
    }
  }
}

export default connect(undefined, mapDispatchToProps)(StatusSelect);
