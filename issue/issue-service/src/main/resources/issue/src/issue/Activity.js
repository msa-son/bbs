
import React from 'react';
import Tabs, { Tab } from 'material-ui/Tabs';

import Comment from './activity/Comment';
import History from './activity/History';


class Activity extends React.Component
{
  constructor(props) {
    super(props);

    this.state = {
      value: 'comment',
    };
  }

  handleChange = (value) => {
    this.setState({ value: value });
  };

  render () {
    return(
      <div>
        <Tabs
          value={this.state.value}
          onChange={this.handleChange}
        >
          <Tab label="Comment" value="comment">
            <Comment/>
          </Tab>
          <Tab label="History" value="history">
            <History/>
          </Tab>
        </Tabs>
      </div>
    );
  }
}

export default Activity;
