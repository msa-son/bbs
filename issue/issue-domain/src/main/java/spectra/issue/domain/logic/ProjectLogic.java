package spectra.issue.domain.logic;

import nara.share.domain.NameValueList;
import spectra.issue.domain.entity.Project;
import spectra.issue.domain.spec.ProjectProvider;
import spectra.issue.domain.spec.ProjectService;
import spectra.issue.domain.spec.sdo.ProjectCdo;
import spectra.issue.domain.store.IssueStoreLycler;
import spectra.issue.domain.store.ProjectStore;

import java.util.List;

public class ProjectLogic implements ProjectService, ProjectProvider
{
    private ProjectStore projectStore;

    public ProjectLogic(IssueStoreLycler storeLycler)
    {
        this.projectStore = storeLycler.requestProjectStore();
    }

    @Override
    public List<Project> listProject()
    {
        return projectStore.retrieveList();
    }

    @Override
    public String registerProject(ProjectCdo projectCdo)
    {
        Project project = new Project(projectCdo.getName(), projectCdo.getOwner());
        projectStore.create(project);

        return project.getId();
    }

    @Override
    public Project findProject(String projectId)
    {
        return projectStore.retrieve(projectId);
    }

    @Override
    public void modifyProject(String projectId, NameValueList nameValueList)
    {
        Project project = projectStore.retrieve(projectId);
        project.setValues(nameValueList);

        projectStore.update(project);
    }

    @Override
    public void removeProject(String projectId)
    {
        Project project = projectStore.retrieve(projectId);

        projectStore.delete(project);
    }
}
