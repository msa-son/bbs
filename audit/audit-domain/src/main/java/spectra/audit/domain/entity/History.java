package spectra.audit.domain.entity;

import nara.share.domain.Entity;
import nara.share.util.json.JsonUtil;

public class History extends Entity
{
    private String method;
    private String actionType;
    private String url;
    private String ip;
    private String entityId;
    private String jwt;
    private long time;

    public History()
    {
    }

    public History(String id)
    {
        super(id);
    }

    public History(String method, String actionType, String url, String ip, String entityId, String jwt)
    {
        this.method = method;
        this.actionType = actionType;
        this.url = url;
        this.ip = ip;
        this.entityId = entityId;
        this.jwt = jwt;

        long currTime = System.currentTimeMillis();
        this.time = currTime;
    }

    public static History getSample()
    {
        History history = new History("GET", "", "/api/p/members", "127.0.0.1", "541c2d42-db1b-4533-8142-61a3951b2ff7", "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJtYW5hZ2VyIiwiaWF0IjoxNTEyMTEzMTQzLCJzdWIiOiJ7XCJtZW1iZXJJZFwiIDogXCJtYW5hZ2VyXCIsIFwibmFtZVwiIDogXCLrp6Tri4jsoIBcIiwgXCJlbWFpbFwiIDogXCJrbWhhbkBzcGVjdHJhLmNvLmtyXCIsIFwicm9sZUlkXCIgOiBcIm1hbmFnZXJcIn0iLCJleHAiOjE1MTQ3MDUxNDN9.2rzxwBjXM9ahWw6hHrfD-KbaMZTj83PnSMZrGI14-oc");
        return history;
    }

    public String toJson()
    {
        return JsonUtil.toJson(this);
    }

    public static History fromJson(String json)
    {
        return JsonUtil.fromJson(json, History.class);
    }

    public String getMethod()
    {
        return method;
    }

    public void setMethod(String method)
    {
        this.method = method;
    }

    public String getActionType()
    {
        return actionType;
    }

    public void setActionType(String actionType)
    {
        this.actionType = actionType;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public String getEntityId()
    {
        return entityId;
    }

    public void setEntityId(String entityId)
    {
        this.entityId = entityId;
    }

    public String getJwt()
    {
        return jwt;
    }

    public void setJwt(String jwt)
    {
        this.jwt = jwt;
    }

    public long getTime()
    {
        return time;
    }

    public void setTime(long time)
    {
        this.time = time;
    }

    public static void main(String[] args)
    {
        System.out.println(getSample());
    }
}
