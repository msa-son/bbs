package spectra.audit.domain.store;

import spectra.audit.domain.entity.History;

import java.util.List;
import java.util.NoSuchElementException;

public interface HistoryStore
{
    String create(History history);
    History retrieve(String id) throws NoSuchElementException;
    List<History> retrieveList();
    void delete(History history);
}
