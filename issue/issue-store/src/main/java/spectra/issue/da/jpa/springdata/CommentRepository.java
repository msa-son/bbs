package spectra.issue.da.jpa.springdata;

import org.springframework.data.repository.PagingAndSortingRepository;
import spectra.issue.da.jpa.jpo.CommentJpo;

import java.util.List;

public interface CommentRepository extends PagingAndSortingRepository<CommentJpo, String>
{
    CommentJpo findByIdAndCreator(String id, String creator);
    List<CommentJpo> findByIssueId(String issueid);
}
