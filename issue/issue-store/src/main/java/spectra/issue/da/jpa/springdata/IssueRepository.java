package spectra.issue.da.jpa.springdata;

import org.springframework.data.repository.PagingAndSortingRepository;
import spectra.issue.da.jpa.jpo.IssueJpo;

import java.util.List;

public interface IssueRepository extends PagingAndSortingRepository<IssueJpo, String>
{
    List<IssueJpo> findByProjectId(String projectId);
    List<IssueJpo> findByTitle(String title);
    List<IssueJpo> findByAssignee(String assignee);
    List<IssueJpo> findBystatus(String status);
}
