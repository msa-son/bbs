package spectra.issue.da.jpa;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import spectra.issue.da.IssueStoreTestApplication;
import spectra.issue.domain.entity.Project;

import java.util.List;
import java.util.NoSuchElementException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = IssueStoreTestApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ProjectJpaStoreTest
{
    @Autowired
    private ProjectJpaStore projectJpaStore;

    private static final String PROJECT_NAME = "testProject1";
    private static final String PROJECT_OWNER = "tester";

    private String projectId;

    @Before
    public void setUp() throws Exception
    {
        projectId = projectJpaStore.create(new Project(PROJECT_NAME, PROJECT_OWNER));
    }

    @Test
    public void testCreate() throws Exception
    {
        projectId = projectJpaStore.create(new Project(PROJECT_NAME, PROJECT_OWNER));
        System.out.println("[TEST INFO] project create => ID : " + projectId);
    }

    @Test(expected = NoSuchElementException.class)
    public void testRetrieve() throws Exception
    {
        Project project = projectJpaStore.retrieve(projectId);
        Assert.assertEquals(projectId, project.getId());
        System.out.println("[TEST INFO] " + project.toString());

        projectJpaStore.retrieve("None");
    }

    @Test
    public void testRetrieveByName() throws Exception
    {
        List<Project> projects = projectJpaStore.retrieveByName(PROJECT_NAME);
        Assert.assertNotEquals(0, projects.size());
        Assert.assertEquals(PROJECT_NAME, projects.get(0).getName());
        System.out.println("[TEST INFO] Projects Size" + projects.size());
        System.out.println("[TEST INFO] " + projects.get(0).toString());
    }

    @Test
    public void testRetrieveByOwner() throws Exception
    {
        List<Project> projects = projectJpaStore.retrieveByOwner(PROJECT_OWNER);
        Assert.assertNotEquals(0, projects.size());
        Assert.assertEquals(PROJECT_OWNER, projects.get(0).getOwner());
        System.out.println("[TEST INFO] Projects Size" + projects.size());
        System.out.println("[TEST INFO] " + projects.get(0).toString());
    }

    @Test
    public void testUpdate() throws Exception
    {
        Project project = projectJpaStore.retrieve(projectId);
        System.out.println("[TEST INFO] 0. Before update => " + project.toString());

        project.setName("testProject2");
        projectJpaStore.update(project);
        System.out.println("[TEST INFO] 1. After update => " + project.toString());
    }

    @Test
    public void testDelete() throws Exception
    {
        Project project = projectJpaStore.retrieve(projectId);
        projectJpaStore.delete(project);
    }
}
