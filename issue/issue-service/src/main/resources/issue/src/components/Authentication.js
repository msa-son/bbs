import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import '../secure/Secure.css';

class Authentication extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: ""
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.handleRegister = this.handleRegister.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    handleChange(e) {
        let nextState = {};
        nextState[e.target.name] = e.target.value;
        this.setState(nextState);
    }

    handleLogin() {
        let id = this.state.username;
        let pw = this.state.password;

        this.props.onLogin(id, pw).then(
            (success) => {
                if(!success) {
                    this.setState({
                        password: ''
                    });
                }
            }
        );
    }

    handleRegister() {
        let id = this.state.username;
        let pw = this.state.password;

        this.props.onRegister(id, pw).then(
            (success) => {
                if(!success) {
                    this.setState({
                        password: ''
                    });
                }
            }
        );
    }

    handleKeyPress(e) {
        if(e.charCode ===13 ){
            if(this.props.mode) {
                this.handleLogin();
            } else {
                this.handleRegister();
            }
        }
    }

    render() {
        const inputBoxes = (
            <div>
                <div className="input-field col s12 username">
                    <input
                    name="username"
                    placeholder="ID"
                    type="text"
                    className="validate"
                    value={this.state.username}
                    onChange={this.handleChange}
                    />
                </div>
                <div className="input-field col s12">
                    <input
                    name="password"
                    placeholder="Password"
                    type="password"
                    className="validate"
                    value={this.state.password}
                    onChange={this.handleChange}
                    onKeyPress={this.handleKeyPress}/>
                </div>
            </div>
        );

        const loginView = (
            <div>
                <div className="card-content">
                    <div className="row">
                        { inputBoxes }
                        <button onClick={this.handleLogin}>Sign in</button>
                    </div>
                </div>
            </div>
        );

        const registerView = (
           <div className="card-content">
               <div className="row">
                   { inputBoxes }
                   <a onClick={this.handleRegister} className="waves-effect waves-light btn">CREATE</a>
               </div>
           </div>
       );

        return(
            <div className="container auth">
                <Link className="logo" to="/">SON BBS</Link>
                <div className="card">
                    {this.props.mode ? loginView : registerView }
                </div>
            </div>
        );
    }

}

Authentication.propTypes = {
    mode: PropTypes.bool,
    onLogin: PropTypes.func,
    onRegister: PropTypes.func
};

Authentication.defaultProps = {
    mode: true,
    onLogin: (id, pw) => { console.error("onLogin not defined"); },
    onRegister: (id, pw) => { console.error("onRegister not defined"); }
};

export default Authentication;
