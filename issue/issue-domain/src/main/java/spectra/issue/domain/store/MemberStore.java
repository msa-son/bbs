package spectra.issue.domain.store;

import spectra.issue.domain.entity.Member;

import java.util.List;
import java.util.NoSuchElementException;

public interface MemberStore
{
    String create(Member member);
    Member retrieve(String memberId) throws NoSuchElementException;

    Member retrieveByMemberIdPassword(String memberId, String password);

    Member retrieveByMemberId(String memberId);
    List<Member> retrieveByName(String name);
    List<Member> retrieveByEmail(String email);
    List<Member> retrieveByRoleId(String roleId);

    List<Member> retreiveList();

    boolean exists(String memberId);

    void update(Member member);
    void delete(Member member);
}
