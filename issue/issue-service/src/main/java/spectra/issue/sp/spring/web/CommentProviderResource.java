package spectra.issue.sp.spring.web;

import nara.share.domain.NameValueList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import spectra.issue.domain.entity.Comment;
import spectra.issue.domain.entity.Project;
import spectra.issue.domain.logic.CommentLogic;
import spectra.issue.domain.logic.IssueLogic;
import spectra.issue.domain.spec.CommentProvider;
import spectra.issue.domain.spec.ProjectProvider;
import spectra.issue.domain.spec.sdo.CommentCdo;
import spectra.issue.domain.spec.sdo.ProjectCdo;

@RestController
@RequestMapping("api/p/comments")
public class CommentProviderResource implements CommentProvider
{
    @Autowired
    private CommentLogic commentLogic;

    @Override
    @PostMapping
    public String registerComment(@RequestBody CommentCdo commentCdo)
    {
        return commentLogic.registerComment(commentCdo);
    }

    @Override
    @GetMapping("{commentId}")
    public Comment findComment(@PathVariable String commentId)
    {
        return commentLogic.findComment(commentId);
    }

    @Override
    @PutMapping("{commentId}")
    public void modifyComment(@PathVariable String commentId, @RequestBody NameValueList nameValueList)
    {
        commentLogic.modifyComment(commentId, nameValueList);
    }
}
