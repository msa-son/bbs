DROP TABLE IF EXISTS t_issue;

CREATE TABLE t_issue
(
  id                        VARCHAR(36) PRIMARY KEY,
  project_id                VARCHAR(36) NOT NULL,
  title                     VARCHAR(1000) NOT NULL,
  question_content          TEXT,
  answer_content            TEXT,
  creator                   VARCHAR(100) NOT NULL,
  assignee                  VARCHAR(100),
  status                    VARCHAR(10) NOT NULL,
  created_time              DECIMAL(13, 0) NOT NULL,
  updated_time              DECIMAL(13, 0) NOT NULL,
  resolved_time             DECIMAL(13, 0)
);

DROP TABLE IF EXISTS t_project;

CREATE TABLE t_project
(
    id                      VARCHAR(36) PRIMARY KEY,
    name                    VARCHAR(100) NOT NULL,
    owner                   VARCHAR(100) NOT NULL,
    members_json            VARCHAR(10000),
    created_time            DECIMAL(13, 0) NOT NULL,
    updated_time            DECIMAL(13, 0) NOT NULL
);

DROP TABLE IF EXISTS t_member;

CREATE TABLE t_member
(
    id                      VARCHAR(36) PRIMARY KEY,
    member_id               VARCHAR(100) NOT NULL,
    password                VARCHAR(100) NOT NULL,
    name                    VARCHAR(100) NOT NULL,
    email                   VARCHAR(100),
    role_id                 VARCHAR(10) NOT NULL,
    created_time            DECIMAL(13, 0) NOT NULL,
    updated_time            DECIMAL(13, 0) NOT NULL
);

DROP TABLE IF EXISTS t_comment;

CREATE TABLE t_comment
(
    id                      VARCHAR(36) PRIMARY KEY,
    issue_id                VARCHAR(36) NOT NULL,
    creator                 VARCHAR(100) NOT NULL,
    content                 TEXT,
    created_time            DECIMAL(13, 0) NOT NULL,
    updated_time            DECIMAL(13, 0) NOT NULL
);


--DROP TABLE IF EXISTS t_permission;
--
--CREATE TABLE t_permission
--(
--    role_id                 VARCHAR(10) NOT NULL,
--    category                VARCHAR(10) NOT NULL,
--    action_type             VARCHAR(10) NOT NULL,
--    value                   VARCHAR(1) NOT NULL,
--)
