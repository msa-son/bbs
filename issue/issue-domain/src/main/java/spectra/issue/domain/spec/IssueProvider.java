package spectra.issue.domain.spec;

import spectra.issue.domain.entity.Issue;
import spectra.issue.domain.spec.sdo.IssueCdo;

import java.util.List;

public interface IssueProvider
{
    //
    List<Issue> listIssue(String projectId, String status, String memberId, String keyword, int startNo);
    String registerIssue(IssueCdo issueCdo);
    Issue findIssue(String issueId);
}
