package spectra.issue.domain.spec;

import nara.share.domain.NameValueList;
import spectra.issue.domain.entity.Project;
import spectra.issue.domain.spec.sdo.ProjectCdo;

import java.util.List;

public interface ProjectProvider
{
    //
    List<Project> listProject();
    String registerProject(ProjectCdo projectCdo);
    Project findProject(String projectId);
    void modifyProject(String projectId, NameValueList nameValueList);
}
