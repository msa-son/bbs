package spectra.issue.da.mybatis;

import nara.share.exception.store.AlreadyExistsException;
import nara.share.exception.store.NonExistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spectra.issue.da.mybatis.mapper.IssueMapper;
import spectra.issue.domain.entity.Issue;
import spectra.issue.domain.store.IssueStore;

import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class IssueMybatisStore implements IssueStore
{
    @Autowired
    private IssueMapper issueMapper;

    @Override
    public String create(Issue issue)
    {
        String id = issue.getId();
        if(issueMapper.exists(id))
        {
            throw new AlreadyExistsException(String.format("Issue[%s] already exist.", id));
        }
        issueMapper.insert(issue);
        return id;
    }

    @Override
    public Issue retrieve(String id) throws NoSuchElementException
    {
        Issue issue = issueMapper.findOne(id);
        if(issue == null)
        {
            throw new NoSuchElementException(String.format("No Issue [ID:%s] to retrieve", id));
        }
        return issue;
    }

    @Override
    public List<Issue> retrieveByProjectId(String projectId)
    {
        return issueMapper.findByProjectId(projectId);
    }

    @Override
    public List<Issue> retrieveByTitle(String title)
    {
        return issueMapper.findByTitle(title);
    }

    @Override
    public List<Issue> retrieveByAssignee(String assignee)
    {
        return issueMapper.findByAssignee(assignee);
    }

    @Override
    public List<Issue> retrieveByStatus(String status)
    {
        return issueMapper.findBystatus(status);
    }

    @Override
    public List<Issue> retrieveList(List<String> projectId, List<String> status, List<String> memberId, String searchKeyword, int startNo)
    {
        return null;
    }

    @Override
    public void update(Issue issue)
    {
        String id = issue.getId();
        if(!issueMapper.exists(id))
        {
            throw new NonExistenceException(String.format("No Issue [ID:%s] to update", id));
        }
        issueMapper.update(issue);
    }

    @Override
    public void delete(Issue issue)
    {
        issueMapper.delete(issue.getId());
    }
}
