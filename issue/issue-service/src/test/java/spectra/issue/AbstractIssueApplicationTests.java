package spectra.issue;

import nara.share.restclient.NaraRestClient;
import nara.share.restclient.springweb.SpringWebRestClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import spectra.issue.adapter.rest.*;
import spectra.issue.domain.Role;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.spec.sdo.CommentCdo;
import spectra.issue.domain.spec.sdo.IssueCdo;
import spectra.issue.domain.spec.sdo.MemberCdo;
import spectra.issue.domain.spec.sdo.ProjectCdo;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = IssueTestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public abstract class AbstractIssueApplicationTests
{
    private IssueCdo sampleIssueCdo;
    private String sampleIssueId;

    private ProjectCdo sampleProjectCdo;
    private String sampleProjectId;

    private MemberCdo sampleMemberCdo;
    private String sampleMemberId;

    private CommentCdo sampleCommentCdo;
    private String sampleCommentId;

    private NaraRestClient restClient;

    @LocalServerPort
    private int port;

    @Bean
    public NaraRestClient createRestClient()
    {
        if(restClient == null)
        {
            restClient = new SpringWebRestClient("http://localhost:" + port);
        }
        return restClient;
    }

    @Bean
    public ProjectServiceRestAdapter projectServiceRestAdapter()
    {
        return new ProjectServiceRestAdapter(createRestClient());
    }

    @Bean
    public ProjectProviderRestAdapter projectProviderRestAdapter()
    {
        return new ProjectProviderRestAdapter(createRestClient());
    }

    @Bean
    public IssueServiceRestAdapter issueServiceRestAdapter()
    {
        return new IssueServiceRestAdapter(createRestClient());
    }

    @Bean
    public IssueProviderRestAdapter issueProviderRestAdapter()
    {
        return new IssueProviderRestAdapter(createRestClient());
    }

    @Bean
    public MemberServiceRestAdapter memberServiceRestAdapter()
    {
        return new MemberServiceRestAdapter(createRestClient());
    }

    @Bean
    public MemberProviderRestAdapter memberProviderRestAdapter()
    {
        return new MemberProviderRestAdapter(createRestClient());
    }

    @Bean
    public CommentServiceRestAdapter commentServiceRestAdapter()
    {
        return new CommentServiceRestAdapter(createRestClient());
    }

    @Bean
    public CommentProviderRestAdapter commentProviderRestAdapter()
    {
        return new CommentProviderRestAdapter(createRestClient());
    }

    @Before
    public void setup()
    {
        memberProviderRestAdapter().login("manager", "1111");
        memberServiceRestAdapter().removeMember("hong");

        sampleProjectCdo = new ProjectCdo("새 프로젝트", "kmhan");
        sampleProjectId = projectServiceRestAdapter().registerProject(sampleProjectCdo);

        sampleIssueCdo = new IssueCdo(sampleProjectId, "이슈 제목입니다.", "이슈내용입니다.", "kmhan");
        sampleIssueId = issueServiceRestAdapter().registerIssue(sampleIssueCdo);

        sampleMemberCdo = new MemberCdo(Member.getSample().getMemberId(), "1111", "한경만", "kmhan@spectra.co.kr", Role.Manager.getCode());
        sampleMemberId = memberServiceRestAdapter().registerMember(sampleMemberCdo);

        sampleCommentCdo = new CommentCdo(sampleIssueId, "kmhan", "새 답글입니다.");
        sampleCommentId = commentServiceRestAdapter().registerComment(sampleCommentCdo);
    }

    public IssueCdo getSampleIssueCdo()
    {
        return sampleIssueCdo;
    }

    public String getSampleIssueId()
    {
        return sampleIssueId;
    }

    public ProjectCdo getSampleProjectCdo()
    {
        return sampleProjectCdo;
    }

    public String getSampleProjectId()
    {
        return sampleProjectId;
    }

    public MemberCdo getSampleMemberCdo()
    {
        return sampleMemberCdo;
    }

    public String getSampleMemberId()
    {
        return sampleMemberId;
    }

    public CommentCdo getSampleCommentCdo()
    {
        return sampleCommentCdo;
    }

    public String getSampleCommentId()
    {
        return sampleCommentId;
    }
}
