package spectra.issue.domain.spec.sdo;

import nara.share.util.json.JsonUtil;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.entity.Project;

import java.io.Serializable;

public class MemberCdo implements Serializable
{
    private String memberId;
    private String password;
    private String name;
    private String email;
    private String roleId;

    public MemberCdo() {}

    public MemberCdo(String memberId, String password, String name, String email, String roleId)
    {
        this.memberId = memberId;
        this.password = password;
        this.name = name;
        this.email = email;
        this.roleId = roleId;
    }

    public static MemberCdo getSample()
    {
        Member memberSample = Member.getSample();

        MemberCdo sample = new MemberCdo(memberSample.getMemberId(), memberSample.getPassword(), memberSample.getName(), memberSample.getEmail(), memberSample.getRoleId());

        return sample;
    }

    public String toJson()
    {
        return JsonUtil.toJson(this);
    }

    public static MemberCdo fromJson(String json)
    {
        return JsonUtil.fromJson(json, MemberCdo.class);
    }

    public String getMemberId()
    {
        return memberId;
    }

    public void setMemberId(String memberId)
    {
        this.memberId = memberId;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getRoleId()
    {
        return roleId;
    }

    public void setRoleId(String roleId)
    {
        this.roleId = roleId;
    }

    public static void main(String[] args)
    {
        //
        System.out.println(getSample());
    }
}
