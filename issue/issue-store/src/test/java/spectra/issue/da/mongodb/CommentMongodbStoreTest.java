package spectra.issue.da.mongodb;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import spectra.issue.da.IssueStoreTestApplication;
import spectra.issue.domain.entity.Comment;

import java.util.List;
import java.util.NoSuchElementException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = IssueStoreTestApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class CommentMongodbStoreTest
{
    @Autowired
    private CommentMongodbStore commentMongodbStore;

    private static final String ISSUE_ID = "testComment";
    private static final String CREATOR = "tester";
    private static final String CONTENT = "testContent1";

    private String commentId;

    @Before
    public void setUp()
    {
        commentId = commentMongodbStore.create(new Comment(ISSUE_ID, CREATOR, CONTENT));
    }

    @Test
    public void testCreate()
    {
        commentId = commentMongodbStore.create(new Comment(ISSUE_ID, CREATOR, CONTENT));
        System.out.println("[TEST INFO] comment create => ID : " + commentId);
    }

    @Test
    public void testRetrieve() throws Exception
    {
        Comment comment = commentMongodbStore.retrieve(commentId);
        Assert.assertEquals(commentId, comment.getId());
        Assert.assertEquals(CREATOR, comment.getCreator());
        System.out.println("[TEST INFO] " + comment.toString());
    }

    @Test
    public void testRetrieveByIssueId() throws Exception
    {
        List<Comment> comment = commentMongodbStore.retrieveByIssueId(ISSUE_ID);
        Assert.assertNotEquals(0, comment.size());
        Assert.assertEquals(ISSUE_ID, comment.get(0).getIssueId());
        System.out.println("[TEST INFO] Issue Size" + comment.size());
        System.out.println("[TEST INFO] " + comment.get(0).toString());
    }

    @Test
    public void testUpdate() throws Exception
    {
        Comment comment = commentMongodbStore.retrieve(commentId);
        System.out.println("[TEST INFO] 0. Before update => " + comment.toString());

        comment.setContent("testContent2");
        commentMongodbStore.update(comment);
        System.out.println("[TEST INFO] 1. After update => " + comment.toString());
    }

    @Test
    public void testDelete() throws Exception
    {
        System.out.println("[TEST INFO] 0. Before delete id => " + commentId);
        Comment comment = commentMongodbStore.retrieve(commentId);
        //commentMongodbStore.delete(comment);
    }
}
