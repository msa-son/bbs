package spectra.issue.da.jpa;

import nara.share.exception.store.AlreadyExistsException;
import nara.share.exception.store.NonExistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spectra.issue.da.jpa.jpo.CommentJpo;
import spectra.issue.da.jpa.springdata.CommentRepository;
import spectra.issue.domain.entity.Comment;
import spectra.issue.domain.store.CommentStore;

import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class CommentJpaStore implements CommentStore
{
    @Autowired
    private CommentRepository commentRepository;

    @Override
    public String create(Comment comment)
    {
        String id = comment.getId();
        if(commentRepository.exists(id))
        {
            throw new AlreadyExistsException(String.format("Comment jpo[ID:%s already exit", id));
        }
        CommentJpo commentJpo = new CommentJpo(comment);
        commentRepository.save(commentJpo);
        return id;
    }

    @Override
    public Comment retrieve(String id) throws NoSuchElementException
    {
        CommentJpo commentJpo = commentRepository.findOne(id);
        if(commentJpo == null)
        {
            throw new NoSuchElementException(String.format("No Comment jpo[ID:%s] to retrieve", id));
        }
        return commentJpo.toDomain();
    }

    @Override
    public Comment retrieveByIdAndCreator(String id, String creator) throws NoSuchElementException
    {
        CommentJpo commentJpo = commentRepository.findByIdAndCreator(id, creator);
        if(commentJpo == null)
        {
            throw new NoSuchElementException(String.format("No Comment jpo[ID:%s, creator:%s] to retrieve", id, creator));
        }
        return commentJpo.toDomain();
    }

    @Override
    public List<Comment> retrieveByIssueId(String issueId)
    {
        List<CommentJpo> commentJpos = commentRepository.findByIssueId(issueId);
        return CommentJpo.toDomains(commentJpos);
    }

    @Override
    public void update(Comment comment)
    {
        String id = comment.getId();
        if(!commentRepository.exists(id))
        {
            throw new NonExistenceException(String.format("No Comment jpo[ID:%s] to update", id));
        }
        CommentJpo commentJpo = commentRepository.findOne(id);
        commentJpo.update(comment);
        commentRepository.save(commentJpo);
    }

    @Override
    public void delete(Comment comment)
    {
        commentRepository.delete(comment.getId());
    }
}