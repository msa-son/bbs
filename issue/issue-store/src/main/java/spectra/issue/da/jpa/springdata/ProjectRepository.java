package spectra.issue.da.jpa.springdata;

import org.springframework.data.repository.PagingAndSortingRepository;
import spectra.issue.da.jpa.jpo.ProjectJpo;

import java.util.List;

public interface ProjectRepository extends PagingAndSortingRepository<ProjectJpo, String>
{
    List<ProjectJpo> findByName(String name);
    List<ProjectJpo> findByOwner(String owner);
}
