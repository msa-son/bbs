import * as types from '../actions/ActionTypes';
import update from 'react-addons-update';

const initialState = {
    list_get: {
        status: 'INIT',
        data: [],
        error: -1
    },
    selected: [],
};

export default function member(state, action) {
    if(typeof state === "undefined") {
        state = initialState;
    }

    switch(action.type) {
        /* selected */
        case types.MEMBER_SELECTED:
            return update(state, {
                selected: {
                    $set: action.ids
                }
            });

        /* getList */
        case types.MEMBER_LIST_GET:
            return update(state, {
                list_get: {
                    status: { $set: 'WAITING' },
                    error: { $set: -1 }
                }
            });
        case types.MEMBER_LIST_GET_SUCCESS:
            return update(state, {
                list_get: {
                    status: { $set: 'SUCCESS' },
                    data: { $set: action.data }
                }
            });
        case types.MEMBER_LIST_GET_FAILURE:
            return update(state, {
                list_get: {
                    status: { $set: 'FAILURE' },
                    error: { $set: action.error }
                }
            });
        default:
            return state;
    }
}
