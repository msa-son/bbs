package spectra.issue.adapter.rest;

import nara.share.restclient.NaraRestClient;
import nara.share.restclient.springweb.SpringWebRestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spectra.issue.domain.entity.Issue;
import spectra.issue.domain.spec.sdo.IssueCdo;

public class IssueProviderRestAdapterTest
{
    private static Logger logger = LoggerFactory.getLogger(IssueProviderRestAdapterTest.class);

    private static NaraRestClient naraRestClient = new SpringWebRestClient("http://localhost:19020");

    private static IssueProviderRestAdapter restAdapter = new IssueProviderRestAdapter(naraRestClient);

    public static void main(String[] args)
    {
        String issueId = restAdapter.registerIssue(new IssueCdo("1", "Issue 제목", "Issue 내용", "kmhan"));
        logger.debug("{}", issueId);

        Issue issue = restAdapter.findIssue(issueId);
        logger.debug("{}", issue);
    }
}
