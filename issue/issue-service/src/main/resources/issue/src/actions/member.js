import {
    MEMBER_SELECTED,
    MEMBER_LIST_GET,
    MEMBER_LIST_GET_SUCCESS,
    MEMBER_LIST_GET_FAILURE,
    AUTH_LOGIN,
    AUTH_LOGIN_SUCCESS,
    AUTH_LOGIN_FAILURE,
    AUTH_LOGOUT
} from './ActionTypes';
import axios from '../common/axios'


/* selected */
export function selectedMember(ids) {
    return {
        type: MEMBER_SELECTED,
        ids
    };
}

export function getMemberListRequest() {
    return (dispatch) => {
        dispatch(getMemberList());

        return axios.get('http://localhost:9999/api/p/members')
        .then((response) => {
            dispatch(getMemberListSuccess(response.data));
        }).catch((error) => {
            dispatch(getMemberListFailure());
        });
    };
}

export function getMemberList() {
    return {
        type: MEMBER_LIST_GET
    };
}

export function getMemberListSuccess(data) {
    return {
        type: MEMBER_LIST_GET_SUCCESS,
        data
    };
}

export function getMemberListFailure() {
    console.log('getMemberListFailure');
    return {
        type: MEMBER_LIST_GET_FAILURE
    };
}

/* LOGIN */
export function loginRequest(username, password) {
  console.log('loginRequest : '+username);
    return (dispatch) => {
            dispatch(login());

            return axios.post('http://localhost:9999/api/p/members/login?memberId='+username+'&password='+password)
            .then((response) => {
                dispatch(loginSuccess(response));
            }).catch((error) => {
                dispatch(loginFailure());
            });
    };
}

export function login() {
    return {
        type: AUTH_LOGIN
    };
}

export function loginSuccess(response) {
    axios.defaults.headers.common.jwt = response.data.jwt
    return {
        type: AUTH_LOGIN_SUCCESS,
        data: response.data
    };
}

export function loginFailure() {
  console.log('loginFailure');
    return {
        type: AUTH_LOGIN_FAILURE
    };
}


/* LOGOUT */
export function logoutRequest(username) {
    console.log('logoutRequest : '+username);
    return (dispatch) => {
        return axios.post('http://localhost:9999/api/p/members/logout?memberId='+username)
        .then((response) => {
            dispatch(logout());
        });
    };
}

export function logout() {
    return {
        type: AUTH_LOGOUT
    };
}
