package spectra.issue.da.mongodb;

import nara.share.exception.store.AlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import spectra.issue.da.mongodb.document.IssueDoc;
import spectra.issue.domain.entity.Issue;
import spectra.issue.domain.store.IssueStore;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class IssueMongodbStore implements IssueStore
{
    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private EntityManager entityManager;

    @Override
    public String create(Issue issue)
    {
        String id = issue.getId();
        Query query = new Query(Criteria.where("id").is(id));
        if (mongoTemplate.exists(query, IssueDoc.class)) {
            throw new AlreadyExistsException(String.format("Issue document[ID:%s already exit", id));
        }

        mongoTemplate.save(new IssueDoc(issue));
        return id;
    }

    @Override
    public Issue retrieve(String id) throws NoSuchElementException
    {
        IssueDoc issueDoc = mongoTemplate.findById(id, IssueDoc.class);
        if(issueDoc == null)
        {
            throw new NoSuchElementException(String.format("No Issue document[ID:%s] to retrieve", id));
        }
        return issueDoc.toDomain();
    }

    @Override
    public List<Issue> retrieveByProjectId(String projectId)
    {
        Query query = new Query(Criteria.where("projectId").is(projectId));
        List<IssueDoc> IssueDocs = mongoTemplate.find(query, IssueDoc.class);

        return IssueDoc.toDomains(IssueDocs);
    }

    @Override
    public List<Issue> retrieveByTitle(String title)
    {
        Query query = new Query(Criteria.where("title").is(title));
        List<IssueDoc> IssueDocs = mongoTemplate.find(query, IssueDoc.class);

        return IssueDoc.toDomains(IssueDocs);
    }

    @Override
    public List<Issue> retrieveByAssignee(String assignee)
    {
        Query query = new Query(Criteria.where("assignee").is(assignee));
        List<IssueDoc> IssueDocs = mongoTemplate.find(query, IssueDoc.class);

        return IssueDoc.toDomains(IssueDocs);
    }

    @Override
    public List<Issue> retrieveByStatus(String status)
    {
        Query query = new Query(Criteria.where("status").is(status));
        List<IssueDoc> IssueDocs = mongoTemplate.find(query, IssueDoc.class);

        return IssueDoc.toDomains(IssueDocs);
    }

    @Override
    public List<Issue> retrieveList(List<String> projectId, List<String> status, List<String> memberId, String searchKeyword, int startNo)
    {
        //복잡한 쿼리 구현 필요 paging : limit, skip, indexing
        /*
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<IssueDoc> query = builder.createQuery(IssueDoc.class);
        Root<IssueDoc> root = query.from(IssueDoc.class);

        List<Predicate> predicates = new ArrayList<>();

        // condition
        if (projectId != null)
        {
            Expression<String> exp = root.get("projectId");
            predicates.add(exp.in(projectId));
        }

        if (status != null)
        {
            Expression<String> exp = root.get("status");
            predicates.add(exp.in(status));
        }

        if (memberId != null)
        {
            predicates.add(
                            builder.or(
                                            root.get("creator").in(memberId),
                                            root.get("assignee").in(memberId)
                            )

            );
        }

        if (searchKeyword != null)
        {
            predicates.add(
                            builder.or(
                                            builder.like(root.get("title"), "%" + searchKeyword + "%"),
                                            builder.like(root.get("questionContent"), "%" + searchKeyword + "%"),
                                            builder.like(root.get("answerContent"), "%" + searchKeyword + "%")
                            )
            );
        }

        query.where(predicates.toArray(new Predicate[]{}));
        query.orderBy(builder.desc(root.get("createdTime")));
        query.select(root);

        TypedQuery<IssueDoc> typesQuery = entityManager.createQuery(query)
                        .setFirstResult(startNo)
                        .setMaxResults(10);

        List<IssueDoc> issueJpos = typesQuery.getResultList();
        return IssueDoc.toDomains(issueJpos);
        */
        return null;
    }

    @Override
    public void update(Issue issue)
    {
        String id = issue.getId();
        Query query = new Query(Criteria.where("id").is(id));
        IssueDoc issueDoc = mongoTemplate.findOne(query, IssueDoc.class);
        if (issueDoc == null) {
            throw new AlreadyExistsException(String.format("Comment documenr[ID:%s  exit", id));
        }

        issueDoc.update(issue);
        mongoTemplate.save(issueDoc);
    }

    @Override
    public void delete(Issue issue)
    {
        mongoTemplate.remove(new IssueDoc(issue));
    }
}