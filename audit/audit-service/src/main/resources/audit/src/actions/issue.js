import {
  ISSUE_GET,
  ISSUE_GET_SUCCESS,
  ISSUE_GET_FAILURE,
  ISSUE_LIST_GET,
  ISSUE_LIST_GET_SUCCESS,
  ISSUE_LIST_GET_FAILURE,
  ISSUE_POST,
  ISSUE_POST_SUCCESS,
  ISSUE_POST_FAILURE
} from './ActionTypes';
import axios from 'axios';


/* getIssue */
export function getIssueRequest(issueId) {
  return (dispatch) => {
    dispatch(getIssue());

    return axios.get('http://localhost:9999/api/p/issues/' + issueId)
      .then((response) => {
        dispatch(getIssueSuccess(response.data));
      }).catch((error) => {
        dispatch(getIssueFailure());
      });
  };
}

export function getIssue() {
  return {
    type: ISSUE_GET
  };
}

export function getIssueSuccess(data) {
  return {
    type: ISSUE_GET_SUCCESS,
    data
  };
}

export function getIssueFailure() {
  console.log('getIssueFailure');
  return {
    type: ISSUE_GET_FAILURE
  };
}

/* postIssue */
export function postIssueRequest(username, password) {
  return (dispatch) => {
    dispatch(postIssue());

    return axios.post('http://localhost:9999/api/p/issues/', {
        username,
        password
      })
      .then((response) => {
        dispatch(postIssueSuccess(response.data));
      }).catch((error) => {
        dispatch(postIssueFailure());
      });
  };
}

export function postIssue() {
  return {
    type: ISSUE_POST
  };
}

export function postIssueSuccess(data) {
  return {
    type: ISSUE_POST_SUCCESS,
    data
  };
}

export function postIssueFailure() {
  console.log('postIssueFailure');
  return {
    type: ISSUE_POST_FAILURE
  };
}


/* getIssueList */
export function getIssueListRequest(projectIds, start) {
  return (dispatch) => {
    console.log('getIssueListRequest: ' + projectIds + ', ' + start);
    dispatch(getIssueList());

    return axios.get('http://localhost:9999/api/p/issues?projectIds=' + projectIds + '&start=' + start)
      .then((response) => {
        dispatch(getIssueListSuccess(response.data));
      }).catch((error) => {
        dispatch(getIssueListFailure());
      });
  };
}

export function getIssueList() {
  console.log('getIssueList');
  return {
    type: ISSUE_LIST_GET
  };
}

export function getIssueListSuccess(data) {
  console.log('getIssueListSuccess');
  return {
    type: ISSUE_LIST_GET_SUCCESS,
    data
  };
}

export function getIssueListFailure() {
  console.log('getIssueListFailure');
  return {
    type: ISSUE_LIST_GET_FAILURE
  };
}
