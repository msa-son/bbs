/* AUTH */
export const AUTH_LOGIN = "AUTH_LOGIN";
export const AUTH_LOGIN_SUCCESS = "AUTH_LOGIN_SUCCESS";
export const AUTH_LOGIN_FAILURE = "AUTH_LOGIN_FAILURE";

export const AUTH_REGISTER = "AUTH_REGISTER";
export const AUTH_REGISTER_SUCCESS = "AUTH_REGISTER_SUCCESS";
export const AUTH_REGISTER_FAILURE = "AUTH_REGISTER_FAILURE";

export const AUTH_GET_STATUS = "AUTH_GET_STATUS";
export const AUTH_GET_STATUS_SUCCESS = "AUTH_GET_STATUS_SUCCESS";
export const AUTH_GET_STATUS_FAILURE = "AUTH_GET_STATUS_FAILURE";

export const AUTH_LOGOUT = "AUTH_LOGOUT";


/* ISSUE */
export const ISSUE_SEARCH = "ISSUE_SEARCH";

export const ISSUE_GET = "ISSUE_GET";
export const ISSUE_GET_SUCCESS = "ISSUE_GET_SUCCESS";
export const ISSUE_GET_FAILURE = "ISSUE_GET_FAILURE";

export const ISSUE_POST = "ISSUE_POST";
export const ISSUE_POST_SUCCESS = "ISSUE_POST_SUCCESS";
export const ISSUE_POST_FAILURE = "ISSUE_POST_FAILURE";

export const ISSUE_LIST_GET = "ISSUE_LIST_GET";
export const ISSUE_LIST_GET_FAILURE = "ISSUE_LIST_GET_FAILURE";
export const ISSUE_LIST_GET_SUCCESS = "ISSUE_LIST_GET_SUCCESS";

/* PROJECT */
export const PROJECT_SELECTED = "PROJECT_SELECTED";

export const PROJECT_GET = "PROJECT_GET";
export const PROJECT_GET_SUCCESS = "PROJECT_GET_SUCCESS";
export const PROJECT_GET_FAILURE = "PROJECT_GET_FAILURE";

export const PROJECT_LIST_GET = "PROJECT_LIST_GET";
export const PROJECT_LIST_GET_SUCCESS = "PROJECT_LIST_GET_SUCCESS";
export const PROJECT_LIST_GET_FAILURE = "PROJECT_LIST_GET_FAILURE";

/* MEMBER */
export const MEMBER_SELECTED = "MEMBER_SELECTED";

export const MEMBER_GET = "MEMBER_GET";
export const MEMBER_GET_SUCCESS = "MEMBER_GET_SUCCESS";
export const MEMBER_GET_FAILURE = "MEMBER_GET_FAILURE";

export const MEMBER_LIST_GET = "MEMBER_LIST_GET";
export const MEMBER_LIST_GET_SUCCESS = "MEMBER_LIST_GET_SUCCESS";
export const MEMBER_LIST_GET_FAILURE = "MEMBER_LIST_GET_FAILURE";

/* STATUS */
export const STATUS_SELECTED = "STATUS_SELECTED";
