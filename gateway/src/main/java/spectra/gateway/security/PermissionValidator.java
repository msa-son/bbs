package spectra.gateway.security;

import org.springframework.stereotype.Component;
import spectra.gateway.config.PermissionProperty;
import spectra.gateway.exception.AccessDeniedException;
import spectra.gateway.exception.InvalidTokenException;
import spectra.gateway.exception.TokenExpiredException;
import spectra.issue.adapter.rest.IssueRestUrl;

import javax.annotation.Resource;
import java.util.HashMap;

@Component
public class PermissionValidator
{
    @Resource
    PermissionProperty permissionProperty;

    public void validate(String uri, String method, String roleId) throws InvalidTokenException, TokenExpiredException
    {
        HashMap<String, String> permissionMap = null;

        if ("admin".equals(roleId))
        {
            permissionMap = permissionProperty.getRole().getAdmin();
        }
        else if ("manager".equals(roleId))
        {
            permissionMap = permissionProperty.getRole().getManager();
        }
        else if ("user".equals(roleId))
        {
            permissionMap = permissionProperty.getRole().getUser();
        }

        boolean accessAllowed = false;
        if (permissionMap != null)
        {
            for (String key : permissionMap.keySet())
            {
                String value = permissionMap.get(key);

                if (uri.startsWith(key) && value.indexOf(method) > -1)
                {
                    accessAllowed = true;
                    break;
                }
            }
        }

        if (!accessAllowed)
        {
            throw new AccessDeniedException(String.format("Access Denied[URI:%s, METHOD:%s, ROLEID:%s]", uri, method, roleId));
        }
    }

}
