package spectra.issue.domain.spec;

import nara.share.domain.NameValueList;
import spectra.issue.domain.entity.Issue;
import spectra.issue.domain.spec.sdo.IssueCdo;

public interface IssueService
{
    //
    String registerIssue(IssueCdo issueCdo);
    Issue findIssue(String issueId);

    void modifyIssue(String issueId, NameValueList nameValueList);
    void removeIssue(String issueId);
}
