package spectra.issue.adapter.rest;

import nara.share.domain.NameValueList;
import nara.share.restclient.NaraRestClient;
import nara.share.restclient.RequestBuilder;
import spectra.issue.domain.entity.Issue;
import spectra.issue.domain.spec.IssueProvider;
import spectra.issue.domain.spec.IssueService;
import spectra.issue.domain.spec.sdo.IssueCdo;

public class IssueServiceRestAdapter implements IssueService
{
    private NaraRestClient naraRestClient;

    public IssueServiceRestAdapter(NaraRestClient naraRestClient)
    {
        this.naraRestClient = naraRestClient;
    }

    @Override
    public String registerIssue(IssueCdo issueCdo)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.ISSUE_S_BUILD)
                            .setRequestBody(issueCdo)
                            .setResponseType(String.class)
        );
    }

    @Override
    public Issue findIssue(String issueId)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.ISSUE_S_FIND)
                            .addPathParam("issueId", issueId)
                            .setResponseType(Issue.class)
        );
    }

    @Override
    public void modifyIssue(String issueId, NameValueList nameValueList)
    {
        naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.ISSUE_S_MODIFY)
                            .addPathParam("issueId", issueId)
                            .setRequestBody(nameValueList)
        );
    }

    @Override
    public void removeIssue(String issueId)
    {
        naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.ISSUE_S_REMOVE)
                            .addPathParam("issueId", issueId)
        );
    }
}
