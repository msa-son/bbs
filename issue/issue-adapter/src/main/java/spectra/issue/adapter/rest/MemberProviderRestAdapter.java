package spectra.issue.adapter.rest;

import nara.share.domain.NameValueList;
import nara.share.restclient.NaraRestClient;
import nara.share.restclient.RequestBuilder;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.spec.MemberProvider;
import spectra.issue.domain.spec.sdo.MemberCdo;

import java.util.List;

public class MemberProviderRestAdapter implements MemberProvider
{
    private NaraRestClient naraRestClient;

    public MemberProviderRestAdapter(NaraRestClient naraRestClient)
    {
        this.naraRestClient = naraRestClient;
    }

    @Override
    public List<Member> listMember()
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.MEMBER_P_LIST)
                                        .setResponseType(List.class)
        );
    }

    @Override
    public String registerMember(MemberCdo memberCdo)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.MEMBER_P_BUILD)
                                        .setRequestBody(memberCdo)
                                        .setResponseType(String.class)
        );
    }

    @Override
    public Member findMember(String memberId)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.MEMBER_P_FIND)
                            .addPathParam("memberId", memberId)
                            .setResponseType(Member.class)
        );
    }

    @Override
    public boolean existsMemberId(String memberId)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.MEMBER_P_EXISTS)
                            .addPathParam("memberId", memberId)
                            .setResponseType(Boolean.class)
        );
    }

    @Override
    public void modifyMember(String memberId, NameValueList nameValueList)
    {
        naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.MEMBER_P_MODIFY)
                            .addPathParam("memberId", memberId)
                            .setRequestBody(nameValueList)
        );
    }

    @Override
    public Member login(String memberId, String password)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.MEMBER_P_LOGIN)
                            .addQueryParam("memberId", memberId)
                            .addQueryParam("password", password)
                            .setResponseType(Member.class)
        );
    }

    @Override
    public void logout(String memberId)
    {

    }
}
