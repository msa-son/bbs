import React from 'react';
import Snackbar from 'material-ui/Snackbar';


class Notify extends React.Component {
    render() {
        return (
            <Snackbar
              open={this.props.open}
              message={this.props.message}
              autoHideDuration={2000}
              />
        );
    }
}

export default Notify;
