package spectra.issue.domain.spec.sdo;

import nara.share.domain.annote.Optional;
import nara.share.util.json.JsonUtil;
import spectra.issue.domain.entity.Issue;
import spectra.issue.domain.entity.Project;

import java.io.Serializable;

public class IssueCdo implements Serializable
{
    private String projectId;
    private String title;
    private String questionContent;
    private String creator;

    @Optional
    private String answerContent;
    @Optional
    private String assignee;
    @Optional
    private String status;

    public IssueCdo() {}

    public IssueCdo(String projectId, String title, String questionContent, String creator)
    {
        this.projectId = projectId;
        this.title = title;
        this.questionContent = questionContent;
        this.creator = creator;
    }

    public static IssueCdo getSample()
    {
        String projectId = Project.getSample().getId();
        Issue issueSample = Issue.getSample();

        IssueCdo sample = new IssueCdo(projectId, issueSample.getTitle(), issueSample.getQuestionContent(), issueSample.getCreator());
        sample.setAssignee(issueSample.getAssignee());
        sample.setStatus(issueSample.getStatus());

        return sample;
    }

    public String toJson()
    {
        return JsonUtil.toJson(this);
    }

    public static IssueCdo fromJson(String json)
    {
        return JsonUtil.fromJson(json, IssueCdo.class);
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getQuestionContent()
    {
        return questionContent;
    }

    public void setQuestionContent(String questionContent)
    {
        this.questionContent = questionContent;
    }

    public String getCreator()
    {
        return creator;
    }

    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    public String getAnswerContent()
    {
        return answerContent;
    }

    public void setAnswerContent(String answerContent)
    {
        this.answerContent = answerContent;
    }

    public String getAssignee()
    {
        return assignee;
    }

    public void setAssignee(String assignee)
    {
        this.assignee = assignee;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public static void main(String[] args)
    {
        //
        System.out.println(getSample());
    }
}
