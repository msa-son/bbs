import React from 'react';
import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';


class Comment extends React.Component
{
  constructor(props){
    super(props);

    this.state={
      comment: ''
    };
  }

  submit(){
    console.log(this.state.comment);
  }

  onKeyPress = (event) => {
    if (event.charCode === 13) { // enter key pressed
      event.preventDefault();
      this.submit();
    }
  }

  onChange = (event, newValue) => {
    this.setState({
      comment: newValue
    });
  }

  handleOK = () => {
    this.submit();
  }

  render(){
    return(
      <div style={{padding:'40px'}}>
        <Table>
          <TableBody displayRowCheckbox={false}>
            <TableRow>
              <TableRowColumn style={{width: '40rem'}}>이런 현상이 전에 K뱅크에서도 있었는데 … 방법을 써서 해결했습니다.</TableRowColumn>
              <TableRowColumn style={{width: '5rem'}}>이승엽</TableRowColumn>
              <TableRowColumn style={{width: '5rem'}}>2017.11.27</TableRowColumn>
            </TableRow>
            <TableRow>
              <TableRowColumn style={{width: '40rem'}}>감사합니다. 도움이 많이 되었습니다.</TableRowColumn>
              <TableRowColumn style={{width: '5rem'}}>엄윤식</TableRowColumn>
              <TableRowColumn style={{width: '5rem'}}>2017.11.27</TableRowColumn>
            </TableRow>
          </TableBody>
        </Table>
        <TextField style={{width: '70%', margin: '5px'}}
        hintText="Comment" onChange={this.onChange} onKeyPress={this.onKeyPress}/>
        <RaisedButton style={{margin: '5px'}} label="OK" onClick={this.handleOK}/>
      </div>
    );
  }
}

export default Comment;
