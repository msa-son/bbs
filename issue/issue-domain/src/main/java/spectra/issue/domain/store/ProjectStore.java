package spectra.issue.domain.store;

import spectra.issue.domain.entity.Project;

import java.util.List;
import java.util.NoSuchElementException;

public interface ProjectStore
{
    String create(Project project);
    Project retrieve(String id) throws NoSuchElementException;
    List<Project> retrieveByName(String name);
    List<Project> retrieveByOwner(String owner);
    List<Project> retrieveList();

    void update(Project project);
    void delete(Project project);
}
