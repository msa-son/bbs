package spectra.issue.da.jpa.springdata;

import org.springframework.data.repository.PagingAndSortingRepository;
import spectra.issue.da.jpa.jpo.MemberJpo;

import java.util.List;

public interface MemberRepository extends PagingAndSortingRepository<MemberJpo, String>
{
    MemberJpo findByMemberId(String memberId);
    List<MemberJpo> findByName(String name);
    List<MemberJpo> findByEmail(String email);
    List<MemberJpo> findByRoleId(String roleId);
    MemberJpo findByMemberIdAndPassword(String memberId, String password);
}
