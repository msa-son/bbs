import * as types from '../actions/ActionTypes';
import update from 'react-addons-update';

const initialState = {
    selected: [],
    list_get: {
        status: 'INIT',
        data: [],
        error: -1
    },
    login: {
        status: 'INIT'
    },
    register: {
        status: 'INIT',
        error: -1
    },
    status: {
        valid: false,
        isLoggedIn: false,
        currentUser: ''
    }
};

export default function member(state, action) {
    if(typeof state === "undefined") {
        state = initialState;
    }

    switch(action.type) {
        /* selected */
        case types.MEMBER_SELECTED:
            return update(state, {
                selected: {
                    $set: action.ids
                }
            });

        /* getList */
        case types.MEMBER_LIST_GET:
            return update(state, {
                list_get: {
                    status: { $set: 'WAITING' },
                    error: { $set: -1 }
                }
            });
        case types.MEMBER_LIST_GET_SUCCESS:
            return update(state, {
                list_get: {
                    status: { $set: 'SUCCESS' },
                    data: { $set: action.data }
                }
            });
        case types.MEMBER_LIST_GET_FAILURE:
            return update(state, {
                list_get: {
                    status: { $set: 'FAILURE' },
                    error: { $set: action.error }
                }
            });

        /* LOGIN */
        case types.AUTH_LOGIN:
            return update(state, {
                login: {
                    status: { $set: 'WAITING '}
                }
            });
        case types.AUTH_LOGIN_SUCCESS:
            return update(state, {
                login: {
                    status: { $set: 'SUCCESS' }
                },
                status: {
                    isLoggedIn: { $set: true },
                    currentUser: { $set: action.data }
                }
            });
        case types.AUTH_LOGIN_FAILURE:
            return update(state, {
                login: {
                    status: { $set: 'FAILURE' }
                }
            });

        /* logout */
        case types.AUTH_LOGOUT:
            return update(state, {
                status: {
                    isLoggedIn: { $set: false },
                    currentUser: { $set: {} }
                }
            });
        default:
            return state;
    }
}
