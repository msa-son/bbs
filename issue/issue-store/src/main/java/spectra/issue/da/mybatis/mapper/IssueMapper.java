package spectra.issue.da.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import spectra.issue.domain.entity.Issue;

import java.util.List;

@Mapper
public interface IssueMapper
{
    void insert(Issue issue);
    boolean exists(String id);
    Issue findOne(String id);
    List<Issue> findByProjectId(@Param("projectId") String projectId);
    List<Issue> findByTitle(@Param("title") String title);
    List<Issue> findByAssignee(@Param("assignee") String assignee);
    List<Issue> findBystatus(@Param("status") String status);
    int update(Issue issue);
    int delete(String id);
}
