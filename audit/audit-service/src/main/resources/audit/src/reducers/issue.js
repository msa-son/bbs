import * as types from '../actions/ActionTypes';
import update from 'react-addons-update';

const initialState = {
    get: {
        status: 'INIT',
        data: {},
        error: -1
    },
    list_get: {
        status: 'INIT',
        data: [],
        error: -1
    },
};

export default function issue(state, action) {
    if(typeof state === "undefined") {
        state = initialState;
    }

    switch(action.type) {
        case types.ISSUE_GET:
            return update(state, {
                get: {
                    status: { $set: 'WAITING' },
                    error: { $set: -1 }
                }
            });
        case types.ISSUE_GET_SUCCESS:
            return update(state, {
                get: {
                    status: { $set: 'SUCCESS' },
                    data: { $set: action.data }
                }
            });
        case types.ISSUE_GET_FAILURE:
            return update(state, {
                get: {
                    status: { $set: 'FAILURE' },
                    error: { $set: action.error }
                }
            });
        case types.ISSUE_LIST_GET:
            return update(state, {
                list_get: {
                    status: { $set: 'WAITING' },
                    error: { $set: -1 }
                }
            });
        case types.ISSUE_LIST_GET_SUCCESS:
            return update(state, {
                list_get: {
                    status: { $set: 'SUCCESS' },
                    data: { $set: action.data }
                }
            });
        case types.ISSUE_LIST_GET_FAILURE:
            return update(state, {
                list_get: {
                    status: { $set: 'FAILURE' },
                    error: { $set: action.error }
                }
            });
        default:
            return state;
    }
}
