package spectra.audit.domain.spec;

import spectra.audit.domain.entity.History;
import spectra.audit.domain.spec.sdo.HistoryCdo;

import java.util.List;

public interface HistoryProvider
{
    //
    String registerHistory(HistoryCdo historyCdo);
    History findHistory(String historyId);
    List<History> listHistory();
}
