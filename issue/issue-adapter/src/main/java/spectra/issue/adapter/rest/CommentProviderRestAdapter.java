package spectra.issue.adapter.rest;

import nara.share.domain.NameValueList;
import nara.share.restclient.NaraRestClient;
import nara.share.restclient.RequestBuilder;
import spectra.issue.domain.entity.Comment;
import spectra.issue.domain.entity.Project;
import spectra.issue.domain.spec.CommentProvider;
import spectra.issue.domain.spec.ProjectProvider;
import spectra.issue.domain.spec.sdo.CommentCdo;
import spectra.issue.domain.spec.sdo.ProjectCdo;

public class CommentProviderRestAdapter implements CommentProvider
{
    private NaraRestClient naraRestClient;

    public CommentProviderRestAdapter(NaraRestClient naraRestClient)
    {
        this.naraRestClient = naraRestClient;
    }

    @Override
    public String registerComment(CommentCdo commentCdo)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.COMMENT_P_BUILD)
                                        .setRequestBody(commentCdo)
                                        .setResponseType(String.class)
        );
    }

    @Override
    public Comment findComment(String commentId)
    {
        return naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.COMMENT_P_FIND)
                                        .addPathParam("commentId", commentId)
                                        .setResponseType(Comment.class)
        );
    }

    @Override
    public void modifyComment(String commentId, NameValueList nameValueList)
    {
        naraRestClient.sendAndRecieve(
                        RequestBuilder.create(IssueRestUrl.COMMENT_P_MODIFY)
                            .addPathParam("commentId", commentId)
                            .setRequestBody(nameValueList)
        );
    }
}
