package spectra.issue.da.mybatis;

import nara.share.exception.store.AlreadyExistsException;
import nara.share.exception.store.NonExistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spectra.issue.da.jpa.jpo.MemberJpo;
import spectra.issue.da.mybatis.mapper.MemberMapper;
import spectra.issue.domain.entity.Member;
import spectra.issue.domain.store.MemberStore;

import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class MemberMybatisStore implements MemberStore
{
    @Autowired
    private MemberMapper memberMapper;

    @Override
    public String create(Member member)
    {
        String memberId = member.getMemberId();
        if (exists(memberId))
        {
            throw new AlreadyExistsException(String.format("member jpo[memberId:%s] already exist", memberId));
        }
        memberMapper.insert(member);
        return memberId;
    }

    @Override
    public Member retrieve(String id) throws NoSuchElementException
    {
        Member member = memberMapper.findOne(id);
        if(member == null)
        {
            throw new NoSuchElementException(String.format("No member jpo[ID:%s] to retrieve", id));
        }
        return member;
    }

    @Override
    public List<Member> retreiveList()
    {
        return null;
    }

    @Override
    public Member retrieveByMemberId(String memberId)
    {
        Member member = memberMapper.findByMemberId(memberId);
        if(member == null)
        {
            throw new NoSuchElementException(String.format("No member jpo[MEMBERID:%s] to retrieve", memberId));
        }
        return member;
    }

    @Override
    public Member retrieveByMemberIdPassword(String memberId, String password)
    {
        Member member = memberMapper.findByMemberIdAndPassword(memberId, password);
        if(member == null)
        {
            throw new NoSuchElementException(String.format("No member jpo[MEMBERID:%s] to retrieveByMemberIdPassword", memberId, password));
        }
        return member;
    }

    public List<Member> retrieveByName(String name)

    {
        return memberMapper.findByName(name);
    }

    @Override
    public List<Member> retrieveByEmail(String email)
    {
        return memberMapper.findByEmail(email);
    }

    @Override
    public List<Member> retrieveByRoleId(String roleId)
    {
        return memberMapper.findByRoleId(roleId);
    }

    @Override
    public void update(Member member)
    {
        String id = member.getId();
        if(!memberMapper.exists(id))
        {
            throw new NonExistenceException(String.format("No member jpo[ID:%s] to update", id));
        }
        memberMapper.update(member);
    }

    @Override
    public void delete(Member member)
    {
        memberMapper.delete(member.getId());
    }

    @Override
    public boolean exists(String memberId)
    {
        return memberMapper.findByMemberId(memberId) != null;
    }
}